const rp = require('request-promise');
const qs = require('querystring');
const crypto = require('crypto');

async function demoPayment(params, uri, amount, address) {
  const body = {
    txn_id: Date.now(),
    fee: 0.01,
    ipn_mode: 'hmac',
    status: 100,
    merchant: params.merchantId,
    address,
    amount
  };

  const hmac = crypto
    .createHmac('sha512', params.ipnSecret)
    .update(qs.stringify(body).replace(/%20/g, '+'))
    .digest('hex');

  return rp({ uri, body, method: 'POST', json: true, headers: { HMAC: hmac } });
}

async function main() {
  const params = {
    merchantId: '2798c4a620e8f0da64a0a5f6ef149f3c',
    ipnSecret: '9mh5tqnVqr6y86VN2YunZTupvKG6Pv3m'
  };
  const endpoint = 'https://quarkchain.tokensale.dreamteam.tech/api/ipn';
  const amount = 0.00005;

  await demoPayment(params, endpoint, amount, '0x3e82fe6d937ccee7bfc349086e7745fa5e33029e');
}

main();
