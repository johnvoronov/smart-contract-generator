module.exports = {
  'extends': [
    'eslint:recommended',
    'airbnb-base'
  ],
  'env': {
    'es6': true,
    'browser': true,
    'mocha': true,
    'node': true
  },
  'plugins': [],
  'parser': 'babel-eslint',
  'parserOptions': {
    'sourceType': 'module',
    'ecmaFeatures': {
      'jsx': false
    }
  },
  'rules': {
    'no-console': 'off',
    'no-debugger': 'off',
    'require-jsdoc': 'off',
    'yoda': 'off',
    'import/prefer-default-export': 'off',
    'lines-around-comment': 'off',
    'newline-after-var': 'off',
    'no-undefined': 'off',
    'no-confusing-arrow': 'off',
    'prefer-destructuring': ['warn', { 'array': false, 'object': true }, { 'enforceForRenamedProperties': false }],
    'brace-style': ['error', '1tbs', { 'allowSingleLine': true }],
    'no-trailing-spaces': ['error', { 'skipBlankLines': true }],
    'linebreak-style': ['warn', 'unix'],
    'no-plusplus': ['error', { 'allowForLoopAfterthoughts': true }],
    'indent': ['warn', 2, {
      // Allow indent in switch/case
      'SwitchCase': 1
    }],
    'import/no-extraneous-dependencies': ['error', { 'devDependencies': true }],
    'class-methods-use-this': ['error', {
      'exceptMethods': [
        'render',
        'getInitialState',
        'getDefaultProps',
        'getChildContext',
        'componentWillMount',
        'componentDidMount',
        'componentWillReceiveProps',
        'shouldComponentUpdate',
        'componentWillUpdate',
        'componentDidUpdate',
        'componentWillUnmount'
      ]
    }],
    'camelcase': ['warn', { 'properties': 'never' }],
    'arrow-body-style': 'off',
    'arrow-parens': ['warn', 'as-needed'],
    'comma-dangle': 'off'
  }
};
