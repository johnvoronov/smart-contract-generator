const express = require('express');
require('express-async-errors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const boom = require('express-boom');
const errorMiddleware = require('./server/middleware/error');
const errorDebugMiddleware = require('./server/middleware/errorDebug');
const cors = require('cors');
const timeout = require('connect-timeout');
const expressWinston = require('express-winston');
const tenant = require('./server/routes/tenant');
const logger = require('./logger');
const events = require('./server/events');
const routes = require('./server/routes');
const i18n = require('./server/service/i18n');
const cron = require('./cron');
const app = express();

const winstonOptions = {
  winstonInstance: logger,
  meta: true,
  dynamicMeta(req, res) {
    return {
      user_id: req.user ? req.user.id : null
    }
  }
};

if (process.env.NODE_ENV !== 'test') {
  app.use(expressWinston.logger(winstonOptions));
}

app.disable('etag');
app.disable('x-powered-by');
app.use('/public', express.static(__dirname + '/public'));
app.use(timeout('5s'));
app.use(cors({
  origin: true,
  optionsSuccessStatus: 200,
  allowedHeaders: ['Content-Type', 'Authorization']
}));
app.use(boom());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(i18n.init);

events(app);

// if (process.env.NODE_ENV === 'development') {
//   app.set('subdomain offset', 0);
// }

app.use(tenant);
app.use('/', routes);

app.use(errorDebugMiddleware);
app.use(errorMiddleware);

app.get('*', (req, res) => res.boom.notFound());

module.exports = app;
