const Token = artifacts.require("./BaseBurnableToken.sol");

const name = 'TEST';
const symbol = 'TST';
const decimals = 18;
const ownerSupply = web3.toWei('999', 'ether');

contract('BaseBurnableToken', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];
    const amount = web3.toWei('9', 'ether');

    let contract;

    beforeEach(async () => {
        contract = await Token.new({
            from: owner
        });
    });

    it('makes sure that the contract was deployed', () => {
        assert.ok(contract.address);
    });

    it('checks params', async () => {
        let instanceName = await contract.name();
        let instanceSymbol = await contract.symbol();
        let instanceDecimals = await contract.decimals();
        let ownerBalance = await contract.balanceOf(owner);

        assert.equal(name, instanceName);
        assert.equal(symbol, instanceSymbol);
        assert.equal(decimals, instanceDecimals);
        assert.equal(ownerSupply, ownerBalance.toNumber());
    });

    it('sends tokens to another account', async () => {
        // First check if the first account would lose his tokens
        const ownerBeforeBalance = await contract.balanceOf(owner);

        assert.equal(
            ownerSupply,
            ownerBeforeBalance.toNumber()
        );

        await contract.transfer(anotherAccount, amount, {from: owner, gas: '1000000'});

        const ownerAfterBalance = await contract.balanceOf(owner);

        assert.equal(
            ownerSupply - amount,
            ownerAfterBalance.toNumber()
        );

        const anotherCurrentBalance = await contract.balanceOf(anotherAccount);

        assert.equal(
            amount,
            anotherCurrentBalance.toNumber()
        );
    });

    it('transfers ownership', async () => {
        const newOwner = accounts[1];

        const instanceOwner = await contract.owner();

        assert.equal(owner, instanceOwner);

        await contract.transferOwnership(newOwner, {from: owner});

        const afterOwner = await contract.owner();

        assert.equal(newOwner, afterOwner);
    });

    describe('Check burn cases', () => {
        it('burns by owner', async () => {
            let ownerCurrentBalance;

            ownerCurrentBalance = await contract.balanceOf(owner);

            assert.equal(
                ownerSupply,
                ownerCurrentBalance.toNumber()
            );

            await contract.burn(amount, {
                from: owner,
                gas: '1000000'
            });

            ownerCurrentBalance = await contract.balanceOf(owner);

            assert.equal(
                ownerSupply - amount,
                ownerCurrentBalance.toNumber()
            );
        });
    });

    describe('Check allowance cases', () => {
        it('approves transfering', async () => {
            let anotherCurrentBalance, ownerCurrentBalance, anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentBalance = await contract.balanceOf(anotherAccount);
            ownerCurrentBalance = await contract.balanceOf(owner);
            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(0, anotherCurrentBalance.toNumber());
            assert.equal(ownerSupply, ownerCurrentBalance.toNumber());
            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.transferFrom(owner, anotherAccount, amount, {
                from: anotherAccount,
                gas: '1000000'
            });

            anotherCurrentBalance = await contract.balanceOf(anotherAccount);
            ownerCurrentBalance = await contract.balanceOf(owner);
            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentBalance.toNumber());
            assert.equal(ownerSupply - amount, ownerCurrentBalance.toNumber());
            assert.equal(0, anotherCurrentAllowance);
        });

        it('decreases approval', async () => {
            let anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.decreaseApproval(anotherAccount, amount / 2, {
                from: owner,
                gas: '1000000'
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount / 2, anotherCurrentAllowance.toNumber());
        });

        it('increases approval', async () => {
            let anotherCurrentAllowance;

            await contract.approve(anotherAccount, amount, {
                from: owner
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount, anotherCurrentAllowance.toNumber());

            await contract.increaseApproval(anotherAccount, amount, {
                from: owner,
                gas: '1000000'
            });

            anotherCurrentAllowance = await contract.allowance(owner, anotherAccount);

            assert.equal(amount * 2, anotherCurrentAllowance.toNumber());
        });
    });
});