const Crowdsale = artifacts.require('./BaseBurnableCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseBurnableToken.sol");

const name = 'TEST';
const symbol = 'TST';
const decimals = 18;
const ownerSupply = 9999;


// 2 tokens for every wei or 2 tokens for every eth
const rate = 2;

contract('BaseBurnableCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(rate, owner, token.address, {
            from: owner
        });

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });

        // Transfer all the tokens
        await token.transfer(crowdsale.address, ownerSupply, {
            from: owner
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks that the withdrawByOwner method works fine', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        // Withdraw tokens
        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });

        const crowdsaleBalance = await token.balanceOf(crowdsale.address);
        const balance = await token.balanceOf(anotherAccount);

        assert.equal(
            weiAmount * rate,
            balance.toNumber()
        );

        assert.equal(
            ownerSupply - (weiAmount * rate),
            crowdsaleBalance.toNumber()
        );
    });

    it('checks that the withdraw method works fine', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        // Withdraw tokens
        await crowdsale.withdraw({
            from: anotherAccount
        });

        const crowdsaleBalance = await token.balanceOf(crowdsale.address);
        const balance = await token.balanceOf(anotherAccount);

        assert.equal(
            weiAmount * rate,
            balance.toNumber()
        );

        assert.equal(
            ownerSupply - (weiAmount * rate),
            crowdsaleBalance.toNumber()
        );
    });
});