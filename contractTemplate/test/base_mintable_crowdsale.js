const Crowdsale = artifacts.require('./BaseMintableCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseMintableToken.sol");

// 2 tokens for every wei or 0.5 tokens for every eth
const rate = 2;

contract('BaseMintableCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(rate, owner, token.address, {
            from: owner
        });

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks that the withdrawByOwner method works fine', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        // Withdraw tokens
        await crowdsale.withdrawByOwner(anotherAccount, anotherAccount, {
            from: owner
        });

        const balance = await token.balanceOf(anotherAccount);

        assert.equal(
            weiAmount * rate,
            balance
        );
    });

    it('checks that the withdraw method works fine', async () => {
        const weiAmount = 1000;

        // To make sure that the method would not fail because of 0 balance
        await crowdsale.addTokens(anotherAccount, weiAmount, {
            from: owner
        });

        // Withdraw tokens
        await crowdsale.withdraw({
            from: anotherAccount
        });

        const balance = await token.balanceOf(anotherAccount);

        assert.equal(
            weiAmount * rate,
            balance
        );
    });
});