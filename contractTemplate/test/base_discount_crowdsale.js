const Crowdsale = artifacts.require('./BaseDiscountCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseMintableToken.sol");

// 2 tokens for every wei or 2 tokens for every eth
const rate = 2;

contract('BaseDiscountCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        crowdsale = await Crowdsale.new(
            rate,
            owner,
            token.address,
            {
                from: owner
            }
        );

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks if the discount system works properly', async () => {
        let currentDiscount = await crowdsale.getDiscount();
        let currentBalance = await crowdsale.balanceOf(anotherAccount);

        // Initial discount
        assert.equal(50, currentDiscount.toNumber());
        assert.equal(0, currentBalance.toNumber());

        // 10 tokens
        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('5', 'ether')
        });

        currentDiscount = await crowdsale.getDiscount();
        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // Next available discount after 10 tokens
        assert.equal(40, currentDiscount.toNumber());

        // We should get 10 tokens + 5 (50% of 10) tokens by discount
        assert.equal(web3.toWei('15', 'ether'), currentBalance.toNumber());


        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('2', 'ether')
        });

        currentDiscount = await crowdsale.getDiscount();
        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // Next available discount after 20 tokens
        assert.equal(30, currentDiscount.toNumber());

        // We should get 15 + 4 + 1.6 (40% of 4) tokens by discount
        assert.equal(web3.toWei('20.6', 'ether'), currentBalance.toNumber());


        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('4', 'ether')
        });

        currentDiscount = await crowdsale.getDiscount();
        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // No more discount after 30 tokens
        assert.equal(0, currentDiscount.toNumber());

        // We should get 20.6 + 8 + 2.4 (30% of 8) tokens by discount
        assert.equal(web3.toWei('31', 'ether'), currentBalance.toNumber());
    });
});