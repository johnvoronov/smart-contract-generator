const Crowdsale = artifacts.require('./BaseTimeDiscountCrowdsaleTestable.sol');
const Token = artifacts.require("./BaseMintableToken.sol");

// 2 tokens for every wei or 2 tokens for every eth
const rate = 2;

const seconds = require('time-funcs/seconds');


const advanceBlock = () => {
    return new Promise((resolve, reject) => {
        web3.currentProvider.sendAsync({
            jsonrpc: '2.0',
            method: 'evm_mine',
            id: Date.now(),
        }, (err, res) => {
            return err ? reject(err) : resolve(res)
        })
    });
};


// To change the time of the latest block
const backToTheFuture = (seconds) => {
    // Increase time
    const id = Date.now();

    return new Promise((resolve, reject) => {
        web3.currentProvider.sendAsync({
            jsonrpc: '2.0',
            method: 'evm_increaseTime',
            params: [seconds],
            id: id,
        }, err1 => {
            if (err1) return reject(err1);

            web3.currentProvider.sendAsync({
                jsonrpc: '2.0',
                method: 'evm_mine',
                id: id + 1,
            }, (err2, res) => {
                return err2 ? reject(err2) : resolve(res)
            })
        })
    });
};

contract('BaseTimeDiscountCrowdsale', (accounts) => {
    const owner = accounts[0];
    const anotherAccount = accounts[1];

    let crowdsale;
    let token;
    let blockTimestamp;

    // Mine a block
    before(async () => {
        await advanceBlock();
    });

    beforeEach(async () => {
        token = await Token.new({
            from: owner
        });

        const lastBlock = await web3.eth.getBlock('latest');

        blockTimestamp = lastBlock.timestamp;

        crowdsale = await Crowdsale.new(
            rate,
            owner,
            token.address,
            {
                from: owner
            }
        );

        // Now Crowdsale owns the token
        await token.transferOwnership(crowdsale.address, {
            from: owner,
            gas: '1000000'
        });
    });

    it('makes sure that the contracts were deployed', () => {
        assert.ok(token.address);
        assert.ok(crowdsale.address);
    });

    it('checks if the discount system works properly', async () => {
        let currentDiscount = await crowdsale.getDiscount();
        let currentBalance = await crowdsale.balanceOf(anotherAccount);

        // Initial discount
        assert.equal(50, currentDiscount.toNumber());
        assert.equal(0, currentBalance.toNumber());

        // 10 tokens
        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('5', 'ether')
        });

        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // We should get 10 tokens + 5 (50% of 10) tokens by discount
        assert.equal(web3.toWei('15', 'ether'), currentBalance.toNumber());

        await backToTheFuture(seconds({days: 1, hours: 1}));

        currentDiscount = await crowdsale.getDiscount();

        // 1 day after
        assert.equal(40, currentDiscount.toNumber());

        // 10 tokens
        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('5', 'ether')
        });

        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // We should get 15 tokens + 10 tokens + 4 (40% of 10) tokens by discount
        assert.equal(web3.toWei('29', 'ether'), currentBalance.toNumber());

        await backToTheFuture(seconds({days: 1, hours: 1}));

        currentDiscount = await crowdsale.getDiscount();

        // 2 days after
        assert.equal(30, currentDiscount.toNumber());


        // 10 tokens
        await crowdsale.sendTransaction({
            from: anotherAccount,
            value: web3.toWei('5', 'ether')
        });

        currentBalance = await crowdsale.balanceOf(anotherAccount);

        // We should get 29 tokens + 10 tokens + 3 (30% of 10) tokens by discount
        assert.equal(web3.toWei('42', 'ether'), currentBalance.toNumber());
    });
});