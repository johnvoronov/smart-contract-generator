const Token = artifacts.require('BaseMintableToken.sol');
const Crowdsale = artifacts.require('BaseWithdrawCrowdsale.sol');

module.exports = async (deployer, network, accounts) => {
    await deployer.deploy(Token);

    const token = await Token.deployed();

    const _token = token.address;

    await deployer.deploy(Crowdsale, 2, _token, _token);
    const crowdsale = await Crowdsale.deployed();

    await token.transferOwnership(crowdsale.address);

    const owner = await crowdsale.owner();

    console.log('******************************************');
    console.log(' -- ПОЖАЛУЙСТА, СКОПИРУЙТЕ ИНФОРМАЦИЮ НИЖЕ');
    console.log('Адрес токена: ' + token.address);
    console.log('Адрес Crowdsale: ' + crowdsale.address);
    console.log('Владелец Crowdsale: ' + owner);
    console.log('************************************');
};