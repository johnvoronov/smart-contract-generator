pragma solidity ^0.4.21;

import './../BaseWithdrawCrowdsale.sol';
import './../BaseDiscountCrowdsale.sol';

contract BaseDiscountCrowdsaleTestable is BaseWithdrawCrowdsale, BaseDiscountCrowdsale {
    function BaseDiscountCrowdsaleTestable(uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token)
    BaseDiscountCrowdsale()
    {
    }

    // Overrided this function to set required discounts
    function setDiscounts() internal {
        discountAvailable = [50, 40, 30];
        discountByTokens[50] = 10 * 1 ether;
        discountByTokens[40] = 20 * 1 ether;
        discountByTokens[30] = 30 * 1 ether;
    }

}
