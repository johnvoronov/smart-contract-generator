pragma solidity ^0.4.0;

import './../BaseWithdrawCrowdsale.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';

contract BaseCappedCrowdsaleTestable is BaseWithdrawCrowdsale {
    using SafeMath for uint256;

    // How many wei we would like to get (min)
    uint256 public softCap;

    // How many wei we would like to get (max)
    uint256 public hardCap;

    function BaseCappedCrowdsaleTestable(uint256 _softCap, uint256 _hardCap, uint256 _rate, address _wallet, ERC20 _token) public
    BaseWithdrawCrowdsale(_rate, _wallet, _token)
    {
        require(_softCap > 0);
        require(_hardCap > 0);
        require(_hardCap >= _softCap);

        softCap = _softCap;
        hardCap = _hardCap;
    }

    // Withdraw is available only if the softCap is reached
    function _validateWithdraw(address _from, address _to, uint256 _tokenAmount) internal {
        require(weiRaised >= softCap);

        super._validateWithdraw(_from, _to, _tokenAmount);
    }

    /**
     * @dev Extend parent behavior requiring purchase to respect the funding cap.
     * @param _beneficiary Token purchaser
     * @param _weiAmount Amount of wei contributed
     */
    function _preValidatePurchase(address _beneficiary, uint256 _weiAmount) internal {
        super._preValidatePurchase(_beneficiary, _weiAmount);

        require(weiRaised.add(_weiAmount) <= hardCap);
    }

    function softLimitReached() public view returns (bool) {
        return weiRaised >= softCap;
    }

    function hardLimitReached() public view returns (bool) {
        return weiRaised >= hardCap;
    }
}
