pragma solidity ^0.4.21;

import './BaseWithdrawCrowdsale.sol';
import './BaseBurnableToken.sol';


contract BaseBurnableCrowdsale is BaseWithdrawCrowdsale {
    // Override this function to send the tokens
    function _withdraw(address _to, uint256 _tokenAmount) internal {
        require(BaseBurnableToken(token).transfer(_to, _tokenAmount));
    }
}
