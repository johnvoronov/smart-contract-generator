pragma solidity ^0.4.21;

import 'openzeppelin-solidity/contracts/crowdsale/Crowdsale.sol';
import './MultiOwnable.sol';
import 'openzeppelin-solidity/contracts/math/SafeMath.sol';


contract BaseWithdrawCrowdsale is Crowdsale, MultiOwnable {
  using SafeMath for uint256;

  // Balances of the beneficiaries
  mapping(address => uint256) balances;

  // Total supply of the tokens during the crowdsale
  uint256 totalSupply_;


  function BaseWithdrawCrowdsale(uint256 _rate, address _wallet, ERC20 _token) public
  Crowdsale(_rate, _wallet, _token)
  MultiOwnable() {
  }

  /**
   * @dev This function should be called by owner if the beneficiary used some other crypto to pay
   * @param _beneficiary Address of the _beneficiary
   * @param weiAmount Amount of wei (converted from other crypto)
   */
  function addTokens(address _beneficiary, uint256 weiAmount) public onlyOwner {
    _preValidatePurchase(_beneficiary, weiAmount);

    // calculate token amount to be created
    uint256 tokens = _getPureTokenAmount(weiAmount);

    // update state
    weiRaised = weiRaised.add(weiAmount);

    _processPurchase(_beneficiary, tokens);

    emit TokenPurchase(
      msg.sender,
      _beneficiary,
      weiAmount,
      tokens
    );

    _updatePurchasingState(_beneficiary, weiAmount);

    _forwardFunds();
    _postValidatePurchase(_beneficiary, weiAmount);
  }


  // Multi version of the addTokens function
  function addTokensMultiple(address[] investors, uint256[] weiAmounts) public onlyOwner {
    for (uint i = 0; i < investors.length; i++) {
      addTokens(investors[i], weiAmounts[i]);
    }
  }

  // Overrided the function to make it store the number of tokens in the balances mapping
  function _processPurchase(address _beneficiary, uint256 _tokenAmount) internal {
    balances[_beneficiary] = balances[_beneficiary].add(_tokenAmount);

    totalSupply_ = totalSupply_.add(_tokenAmount);
  }

  // Withdraw the tokens to the same address
  function withdraw() public {
    uint256 tokenAmount = balances[msg.sender];

    require(tokenAmount > 0);

    _validateWithdraw(msg.sender, msg.sender, tokenAmount);

    balances[msg.sender] = 0;

    _withdraw(msg.sender, tokenAmount);
  }

  // Withdraw the tokens of the _from address to the required account
  function withdrawByOwner(address _from, address _to) public onlyOwner {
    uint256 tokenAmount = balances[_from];

    if (tokenAmount > 0) {
      _validateWithdraw(_from, _to, tokenAmount);

      balances[_from] = 0;

      _withdraw(_to, tokenAmount);
    }
  }

  // Multi version of the withdrawByOwner function
  function withdrawMultiple(address[] from, address[] to) public onlyOwner {
    for (uint i = 0; i < from.length; i++) {
      withdrawByOwner(from[i], to[i]);
    }
  }

  // Override this function to send the tokens
  function _withdraw(address _to, uint256 _tokenAmount) internal {
  }

  // Override this function to add additional validation logic
  function _validateWithdraw(address _from, address _to, uint256 _tokenAmount) internal {
    require(withdrawAvailable());
  }

  // Override this function to add additional checks
  function withdrawAvailable() public view returns (bool) {
    return true;
  }

  /**
  * @dev Gets the balance of the specified address.
  * @param _owner The address to query the the balance of.
  * @return An uint256 representing the amount owned by the passed address.
  */
  function balanceOf(address _owner) public view returns (uint256) {
    return balances[_owner];
  }

  /**
  * @dev total number of tokens in existence
  */
  function totalSupply() public view returns (uint256) {
    return totalSupply_;
  }

  /**
   * To get pure amount of the tokens, without discounts and other stuff
   * @param _weiAmount Value in wei to be converted into tokens
   * @return Number of tokens that can be purchased with the specified _weiAmount
   */
  function _getPureTokenAmount(uint256 _weiAmount) internal view returns (uint256) {
    return _weiAmount.mul(rate);
  }

//  // To update the contract if that would be required in the future
//  function transferTokenOwnership(address _newOwner) public onlyOwner {
//    token.transferOwnership(_newOwner);
//  }
}
