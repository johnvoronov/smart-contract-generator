pragma solidity ^0.4.21;

import 'openzeppelin-solidity/contracts/token/ERC20/BurnableToken.sol';
import 'openzeppelin-solidity/contracts/token/ERC20/StandardToken.sol';
import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';


contract BaseBurnableToken is BurnableToken, StandardToken, Ownable {
    string public name = "TEST";
    string public symbol = "TST";
    uint8 public decimals = 18;
    uint256 public supply = 999 * 1 ether;

    function BaseBurnableToken() public
    Ownable() {
        totalSupply_ = supply;

        balances[msg.sender] = supply;

        emit Transfer(address(0), msg.sender, supply);
    }
}
