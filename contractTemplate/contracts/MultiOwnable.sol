pragma solidity ^0.4.21;

import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';

/**
 * @title MultiOwnable
 * @dev The MultiOwnable contract implements multiple version of the Ownable contract
 */
contract MultiOwnable {
    // The main owner of the contract
    address public owner;

    // Additional owners those may use onlyOwner methods
    mapping(address => bool) public owners;

    /**
     * @dev We should have at least 1 owner to add other owners lately
     */
    function MultiOwnable() public {
        owner = msg.sender;

        owners[msg.sender] = true;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(owners[msg.sender] == true);
        _;
    }
    /**
     * @dev Throws if called by any account other than the main owner.
     */
    modifier onlyMainOwner() {
        require(msg.sender == owner);
        _;
    }

    // Add a new owner to the list
    function addOwner(address newOwner) public onlyMainOwner {
        owners[newOwner] = true;
    }

    // Remove the owner from the list
    function removeOwner(address owner) public onlyMainOwner {
        delete owners[owner];
    }
}
