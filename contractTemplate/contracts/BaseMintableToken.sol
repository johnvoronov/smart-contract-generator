pragma solidity ^0.4.21;

import 'openzeppelin-solidity/contracts/token/ERC20/MintableToken.sol';

contract BaseMintableToken is MintableToken {
    string public name = "TEST";
    string public symbol = "TST";
    uint8 public decimals = 18;
}
