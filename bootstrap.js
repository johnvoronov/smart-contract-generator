const path = require('path');
require('dotenv').config({
  path: path.resolve(__dirname, '.env')
});

// https://github.com/sequelize/sequelize/issues/3781
const pg = require('pg');
delete pg.native;

require('./raven');
