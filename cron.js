const cron = require('node-cron');
const blockchain = require('./server/util/blockchain');
const deployService = require('./server/service/deploy');
const currencyUtil = require('./server/util/currency');

cron.schedule('* * * * *', async () => {
  try {
    await deployService.deployProjects();
  } catch (e) {
    console.error('CRON[deployService.deployProjects]: ', e);
  }
});

cron.schedule('* * * * *', async () => {
  try {
    await blockchain.checkProjectTransactions();
  } catch (e) {
    console.error('CRON[blockchain.checkProjectTransactions]: ', e);
  }
});

cron.schedule('* * * * *', async () => {
  try {
    await blockchain.sendProjectTransactions();
  } catch (e) {
    console.error('CRON[blockchain.sendProjectTransactions]: ', e);
  }
});

// cron.schedule('0 */3 * * *', async () => {
//   try {
//     await currencyUtil.update();
//   } catch (e) {
//     console.error('CRON[currencyUtil.update]: ', e);
//   }
// });
