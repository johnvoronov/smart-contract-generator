const path = require('path');
const {
  format,
  loggers,
  transports
} = require('winston');

loggers.add('app', {
  transports: [
    new transports.File({
      filename: path.resolve(__dirname, 'var/log/app.log'),
      level: 'error'
    }),
    new transports.Console()
  ]
});

loggers.add('eth', {
  transports: [
    new transports.File({
      filename: path.resolve(__dirname, 'var/log/sql.log'),
      level: 'error'
    }),
    new transports.Console()
  ]
});

loggers.add('sql', {
  format: format.simple(),
  transports: [
    new transports.File({
      filename: path.resolve(__dirname, 'var/log/sql.log'),
    }),
    new transports.Console()
  ]
});

module.exports = loggers.get('app');
