# ENV params

```
COINPAYMENTS_PUBLIC_KEY - публичный ключ от coinpayments
COINPAYMENTS_SECRET_KEY - секретный ключ от coinpayments
COINPAYMENTS_MERCHANT_ID - ID продавца coinpayments
COINPAYMENTS_MERCHANT_SECRET - секретный ключ продавца coinpayments

APP_NAME - название проекта / клиента для почтовых шаблонов
THEME_COLOR - цвет темы для почтовых шаблонов (пометить как deprecated и создать отдельный универсальный шаблон)
COMPANY_NAME - название компании

WEB3_HTTP_PROVIDER - ссылка на HTTP gateway ethereum, нужна для подключения к блокчейну через web3.
CONTRACT_ABI_FILENAME - имя ABI файла Crowdsale контракта, нужен для создания полноценного "объекта" на основе ABI.
CONTRACT_ADDRESS - адрес контракта в блокчейне.
OWNER_PRIVATE_KEY - приватный ключ овнера, нужен для вызова onlyOwner методов.
OWNER_ADDRESS - адрес овнера, нужен для получения кол-ва транзакций (юзается при подписи транзакции, параметр nonce)

RDS_USERNAME - имя пользователя бд
RDS_PASSWORD - пароль от бд
RDS_DB_NAME - имя бд
RDS_HOSTNAME - хост для подключения
RDS_PORT - порт подключения

SMTP_HOST - хост smtp для отправки писем
SMTP_PORT - порт smtp для отправки писем
SMTP_USERNAME - имя пользователя
SMTP_PASSWORD - пароль
SMTP_FROM - почтовый адрес от которого происходит отправка писем

SOCIAL_GOOGLE_ID - публичный ключ
SOCIAL_GOOGLE_SECRET - приватный ключ
SOCIAL_FACEBOOK_ID - публичный ключ
SOCIAL_FACEBOOK_SECRET - приватный ключ
SOCIAL_TWITTER_ID - публичный ключ
SOCIAL_TWITTER_SECRET - приватный ключ

JWT_SECRET - приватный ключ для JWT токена (или сертификат)
JWT_EXPIRE - время жизни токена
```
