const Raven = require('raven');
const { sentry } = require('./server/config/app');

if (sentry.dsn) {
  Raven.config(sentry.dsn, {
    release: sentry.release,
    environment: sentry.environment
  }).install();
}
