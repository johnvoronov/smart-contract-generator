require('./bootstrap');
const blockchain = require('./server/util/blockchain');
const deployService = require('./server/service/deploy');

async function main() {
  try {
    await deployService.deployProjects();
    // await blockchain.sendProjectTransactions();
    // await blockchain.checkProjectTransactions();
    // await blockchain.checkProjectBalances();

    process.exit(0);
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

main();
