module.exports = {
  /**
   * Unscience numbers:
   *
   * 1 => '1'
   * 3.125e7 => '31250000'
   * 3e-17 => '0.00000000000000003'
   *
   * @param value
   * @returns {*}
   */
  unscienceNumber(value) {
    if (/\d+\.?\d*e[\+\-]*\d+/i.test(value) === false) {
      return value.toString();
    }

    const zero = '0';
    const parts = String(value).toLowerCase().split('e'); //split into coeff and exponent
    const e = parts.pop();//store the exponential part

    let l = Math.abs(e); //get the number of zeros

    const sign = e / l;
    const coeffArray = parts[0].split('.');

    if (sign === -1) {
      return (zero + '.' + new Array(l).join(zero) + coeffArray.join('')).toString();
    }

    const dec = coeffArray[1];
    if (dec) {
      l = l - dec.length
    }
    return (coeffArray.join('') + new Array(l + 1).join(zero)).toString();
  },
};
