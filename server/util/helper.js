const models = require('../models');
const config = require('../config/blockchain');
const blockchain = require('./blockchain');
const mail = require('./mail');

module.exports = {
  validateBalance(user_id) {
    return new Promise((resolve, reject) => {
      models.Transaction.sum('amount', {
        where: { user_id }
      }).then(amount => {
        const hash = blockchain.signUser(user_id);

        blockchain.getBalance(hash).then(balance => {
          resolve(parseFloat(amount || 0), parseFloat(balance));
        }).catch(reject);
      }).catch(reject);
    })
  },

  validateBalances() {
    return new Promise((resolve, reject) => {
      models.User.findAll({
        where: { is_active: true }
      }).then(rows => {
        for (let i = 0; i < rows.length; i++) {
          const row = rows[i];

          this.validateBalance(row.id).then(({ local, remote }) => {
            if (local !== remote) {
              mail.send(config.blockchain_developer, 'balance_not_equal', {
                balance: {
                  local,
                  remote
                },
                user: {
                  id: user.id,
                  hash: blockchain.signUser(user.id)
                }
              });
            }
          }).catch(reject);
        }
      }).catch(reject);
    })
  }
};
