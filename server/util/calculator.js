const discountService = require('../service/discount');
const BigNumber = require('bignumber.js');
const BN = BigNumber.clone({
  DECIMAL_PLACES: 17
});

module.exports = {
  /**
   * Расчет количества бонусных токенов
   *
   * @param project проект
   * @param amount количество купленных токенов
   * @param discount процент скидки (бонуса)
   * @returns {Promise<*>}
   */
  async getBonusFor(project, amount, discount = null) {
    discount = discount || discountService.getDiscount(project);

    if (discount === 0) {
      return amount;
    }

    return amount / 100 * discount;
  },

  /**
   * amount * rate / token_price
   *
   * @param token_price
   * @param amount
   * @param rate
   * @returns {number | *}
   */
  calcTokenAmount(token_price, amount, rate) {
    return (new BN(amount))
      .multipliedBy(rate)
      .div(token_price)
      .toNumber();
  },

  /**
   * price + (price / 100 * discount)
   *
   * @param priceRaw
   * @param discount
   * @returns {number}
   */
  calcTokenBonus(priceRaw, discount = 0) {
    return (new BN(priceRaw))
      .div(100)
      .multipliedBy(discount)
      .toNumber();
  }
};
