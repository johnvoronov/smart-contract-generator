const { Op } = require('sequelize');
const { ethStatus } = require('../consts');
const models = require('../models');

module.exports = {
  /**
   * Fetch nonce value from eth network
   *
   * @returns {Promise<number>}
   */
  getNonceFromNetwork(provider, address) {
    return provider.getNonce(address);
  },

  /**
   * Возвращаем значение < 1, чтобы в методе getNonce
   * вернулось максимальное значение из eth сети.
   * NaN || 0 возвращается в случае, если записи в бд
   * отсутствуют
   *
   * @returns {Promise<number>}
   */
  async getNonceFromDatabase(project, address) {
    const nonce = await models.Eth.max('nonce', {
      where: {
        address,
        project_id: project.id,
        status: {
          [Op.not]: ethStatus.ETH_ERROR
        }
      }
    });

    return nonce ? nonce + 1 : 0;
  },

  /**
   * Находим максимальное число с nonce.
   *
   * Кейс 1: nonce в eth сети больше, чем nonce в локальной бд
   * Причина: выполнены действия напрямую со смарт контрактом
   * Результат: возврат nonce из eth сети
   *
   * Кейс 2: nonce в локальной бд больше, чем в eth сети
   * Причина: отправлено N транзакций разом, но они еще не
   * были замайнены
   * Результат: вернется nonce из локальной бд
   *
   * Кейс 3: в локальной бд были отправлены N транзакций, но
   * первая из них провалилась, следовательно все остальные за этой // TODO
   * нужно пометить как проваленые в локальной бд
   * Причина: множество разных причин. Недостатого "газа", причина из
   * кейсов 1 и 2 и так далее.
   * Результат: вернется nonce из eth сети больший, чем nonce в
   * локальной бд.
   *
   * Кейс 4: // TODO
   * Ты отправил N транзакций, спустя 5 минут отправляешь еще N транзакций.
   * Какой nonce актуальный? В eth сети допустим он будет 10, в бд он будет
   * допустим 10 + 10 + 10 (по 10 транзакций за раз), итого мы ожидаем что
   * в следующий раз мы ставим nonce 31, но вот тут начинается радость.
   * 20-ая (и выше соответственно) транзакция пошла по мзде.
   * В бд записано 20 как актуальный нонс после провала 30-ой, но первая
   * пачка еще не замайнилась. Вопрос к знатокам: какой nonce?
   * eth будет возвращать тебе 10 nonce, в бд у нас 20 актуальный. Кто прав?
   *
   * @returns {Promise<int>}
   */
  async getNonce(project, provider) {
    const ownerAddress = project.credentials.address;

    const network = await this.getNonceFromNetwork(provider, ownerAddress);
    const database = await this.getNonceFromDatabase(project, ownerAddress);

    return Math.max(network, database);
  }
};
