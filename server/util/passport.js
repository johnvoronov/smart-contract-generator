const passport = require('passport');
const TwitterTokenStrategy = require('passport-twitter-token');
const FacebookTokenStrategy = require('passport-facebook-token');
const GoogleTokenStrategy = require('passport-google-token').Strategy;
const socialConfig = require('../config/social');
const models = require('../models');

function socialCallback(provider) {
  return async (token, secret, parameters, done) => {
    const social = await models.Social.findOne({
      where: { provider, owner_id: parameters.id }
    });

    if (social) {
      const user = await models.User.findById(social.user_id);
      if (user) {
        done(null, user);
      } else {
        done(new Error('User not found'), null);
      }
    } else {
      const email = parameters.emails.length > 0
        ? parameters.emails.pop().value
        : null;

      let user = await models.User.findOne({
        where: { email }
      });

      if (!user) {
        user = await models.User.create({ email, is_active: true });
      } else {
        await user.update({ is_active: true });
      }

      await models.Social.create({
        provider,
        owner_id: parameters.id,
        parameters: JSON.stringify(parameters),
        user_id: user.id
      });

      done(null, user);
    }
  }
}

module.exports = {
  facebookProvider() {
    const params = {
      clientID: socialConfig.facebook.id,
      clientSecret: socialConfig.facebook.secret
    };

    return new FacebookTokenStrategy(params, socialCallback('facebook'));
  },

  twitterProvider() {
    const params = {
      consumerKey: socialConfig.twitter.id,
      consumerSecret: socialConfig.twitter.secret,
      includeEmail: true
    };

    return new TwitterTokenStrategy(params, socialCallback('twitter'));
  },

  googleProvider() {
    const params = {
      clientID: socialConfig.google.id,
      clientSecret: socialConfig.google.secret
    };

    return new GoogleTokenStrategy(params, socialCallback('google'));
  },

  getPassport() {
    passport.use(this.twitterProvider());
    passport.use(this.googleProvider());
    passport.use(this.facebookProvider());

    return passport;
  }
};
