const rp = require('request-promise');
const models = require('../models');

module.exports = {
  fetch() {
    return rp({
      uri: 'https://api.coinmarketcap.com/v1/ticker/?convert=ETH&limit=5000',
      json: true
    });
  },

  formatCurrencies(items = [], codes = []) {
    let result = {};

    for (let i = 0; i < items.length; i++) {
      if (codes.indexOf(items[i].symbol) > -1) {
        const {
          symbol,
          price_eth,
          price_usd
        } = items[i];

        if (!price_eth || !price_usd) {
          continue;
        }

        result[symbol] = {
          to_eth: price_eth,
          to_usd: price_usd
        };
      }
    }

    return result;
  },

  async update() {
    const resp = await this.fetch();

    const rows = await models.Currency.findAll();

    const currencies = this.formatCurrencies(resp, rows.map(row => row.code));
    for (let i = 0; i < rows.length; i++) {
      if (typeof currencies[rows[i].code] === 'undefined') {
        continue;
      }

      await rows[i].update(currencies[rows[i].code], {
        fields: ['to_eth', 'to_usd']
      });
    }

    const eth = await models.Currency.findOne({
      where: { code: 'ETH' }
    });

    if (eth !== null) {
      let usd = await models.Currency.findOne({
        where: { code: 'USD' }
      });
      if (null === usd) {
        usd = await models.Currency.create({
          code: 'USD',
          name: 'Dollar',
          to_eth: 1 / eth.to_usd,
          to_usd: 1
        });
      } else {
        await usd.update({
          to_eth: 1 / eth.to_usd,
          to_usd: 1
        });
      }
    }
  }
};
