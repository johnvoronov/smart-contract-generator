const ethers = require('ethers');
const config = require('../config/blockchain');
const models = require('../models');
const deployerFactory = require('../service/deployer');
const Wallet = require('../service/deployer/wallet');
const mail = require('./mail');
const Raven = require('raven');
const numberUtil = require('./number');
const keccak256Util = require('./keccak256');
const nonceUtil = require('./nonce');
const { projectStatus, ethStatus, transactionStatus } = require('../consts');
const { Op } = require('sequelize');
const BigNumber = require('bignumber.js');
const Big = BigNumber.clone({ DECIMAL_PLACES: 17 });

module.exports = {
  /**
   * @returns {*}
   */
  getProvider(project) {
    const {
      code
    } = project.blockchain;

    return deployerFactory.createProvider(code, this.getWallet(project), 'rinkeby');
  },

  /**
   * TODO интегрировать с sentry / jira + sla / gitlab
   *
   * В обязательном порядке уведомляем ответственного
   * о существующей проблеме
   *
   * @param eth
   * @returns {*}
   */
  sendSystemNotification(eth) {
    return mail.send(config.blockchain_developer, 'transaction_error', {
      eth
    });
  },

  /**
   * Отправка уведомления пользователю о успешном зачислении
   * баланса на СК
   *
   * @param eth
   * @returns {*}
   */
  async sendUserNotification(eth) {
    const transactions = await models.Transaction.findAll({
      where: { eth_id: eth.id },
      include: [
        { model: models.User, as: 'user' },
        { model: models.Payment, as: 'payment' }
      ]
    });

    for (let i = 0; i < transactions.length; i++) {
      const transaction = transactions[i];

      mail.send(transaction.user.email, 'transaction_success', {
        eth,
        transaction,
        payment: transaction.payment,
        user: transaction.user
      });
    }
  },

  /**
   * @param project
   * @param nonce
   * @param address
   * @param ids
   * @returns {Promise<any>}
   */
  async createTransaction(project, nonce, address, ids) {
    const eth = await models.Eth.create({
      project_id: project.id,
      status: ethStatus.ETH_PENDING,
      address,
      nonce
    });

    await models.Transaction.update({
      eth_id: eth.id,
      in_sync: true
    }, {
      where: {
        id: { [Op.in]: ids }
      }
    });

    return eth;
  },

  async _sendTransaction(project, nonce, address, transactions, data) {
    const ids = transactions.map(tx => tx.id);

    // Создаем транзакцию в локальной бд
    const eth = await this.createTransaction(project, nonce, address, ids);

    // Выполняем валидацию будущей транзакции. При estimateGas
    // виртуальная машина Ethereum прогоняет транзакцию, делает revert
    // и возвращает примерно затраченный gas по транзакции в целом.
    // Благодаря этому, мы можем предварительно с помощью estimateGas
    // убедиться в валидности транзакции.
    try {
      const estimateGas = await this.getEstimateGas(project, {
        data,
        from: project.credentials.address,
        to: project.contract.ico.address
      });

      await eth.update({ gas: estimateGas.toString() });

      try {
        // Подписываем и отправляем транзакцию
        const txHash = await this.sendSignedTransaction(project, nonce, data, estimateGas);

        // Если TX пришла от eth сети, то записываем
        // hash в родительскую транзакцию
        await eth.update({
          hash: txHash
        });

        return txHash;

      } catch (e) {
        Raven.captureException(e, {
          extra: {
            project_id: project.id,
            eth_id: eth.id,
            transaction_ids: ids.join(', ')
          }
        });

        await this._failWaitTransaction(e, eth);
      }
    } catch (e) {
      Raven.captureException(e, {
        extra: {
          eth_id: eth.id,
          transaction_ids: ids.join(', ')
        }
      });

      await this._failGetEstimateGas(e, eth, ids);
    }
  },

  _successWaitTransaction(eth) {
    this.sendUserNotification(eth);

    return eth.update({ status: ethStatus.ETH_COMPLETE });
  },

  getAmountFromTransaction(transaction) {
    const tokens = new Big(transaction.amount.toString());
    const rate = new Big(transaction.payment.to_eth.toString());
    const price = new Big(transaction.payment.token_price.toString());

    return numberUtil.unscienceNumber(tokens.div(rate).multipliedBy(price).toString());
  },

  /**
   * 100 или меньше операций проводим в eth сети
   * одной транзакцией
   *
   * @param project
   * @param nonce
   * @param address
   * @param transactions
   * @returns {Promise<any>}
   */
  sendTokens(project, nonce, address, transactions = []) {
    const identifiers = [];
    const amounts = [];

    for (let x = 0; x < transactions.length; x++) {
      const transaction = transactions[x];
      identifiers.push(keccak256Util.signUser(transaction.user_id));
      amounts.push(this.toWei(this.getAmountFromTransaction(transaction)));
    }

    return this._sendTransaction(
      project,
      nonce,
      address,
      transactions,
      this._getAddTokensMultipleData(project, identifiers, amounts)
    );
  },

  /**
   * 100 или меньше операций проводим в eth сети
   * одной транзакцией
   *
   * @param project
   * @param nonce
   * @param address
   * @param transactions
   * @returns {Promise<any>}
   */
  sendWithdraw(project, nonce, address, transactions = []) {
    let identifiers = [],
      wallets = [];

    for (let x = 0; x < transactions.length; x++) {
      const transaction = transactions[x];
      identifiers.push(keccak256Util.signUser(transaction.user_id));
      wallets.push(transaction.user.eth_address);
    }

    return this._sendTransaction(
      project,
      nonce,
      address,
      transactions,
      this._getWithdrawMultipleData(
        project,
        identifiers,
        wallets
      )
    );
  },

  /**
   * В обоих случаях делаем reject, чтобы
   * задача в очереди провалилась и ушло уведомление
   * ответственному лицу
   *
   * @param error
   * @param eth
   * @param ids
   * @returns {Promise<T>}
   * @private
   */
  async _failGetEstimateGas(error, eth, ids) {
    // В случае, если estimateGas не прошла и вернула ошибку, значит в
    // данных транзакции есть ошибка. Мы должны отметить транзакции которые к нам
    // пришли как in_sync = false, чтобы в следующую итерацию крона они повторно
    // отправились в очередь
    await eth.update({
      status: ethStatus.ETH_ERROR,
      error: error.toString()
    });

    await models.Transaction.update({ in_sync: false }, {
      where: {
        id: { [Op.in]: ids }
      }
    });
  },

  /**
   * Отмечаем транзакцию проваленой и в обоих случаях
   * после обновления бд нужно делать reject.
   *
   * @param error
   * @param eth
   * @returns {Promise}
   * @private
   */
  async _failWaitTransaction(error, eth) {
    // Отправляем системное уведомление
    // ответственному лицу
    this.sendSystemNotification(eth);

    // Отмечаем транзакцию проваленой и в обоих случаях
    // после обновления бд нужно делать reject.
    await eth.update({
      status: ethStatus.ETH_ERROR,
      error: error.toString()
    });

    await models.Transaction.update({ in_sync: false }, {
      where: { eth_id: eth.id }
    });
  },

  /**
   * Конвертим сумму в wei формат (сдвиг вправо на 18 цифр)
   *
   * @param value
   * @returns {*}
   */
  toWei(value) {
    return ethers.utils.parseEther(
      numberUtil.unscienceNumber(value.toString())
    ).toString();

    // return this.getClient().utils.toWei(
    //     numberUtil.unscienceNumber(value.toString())
    // );
  },

  getWallet(project) {
    const {
      address,
      privateKey
    } = project.credentials;

    return new Wallet(address, privateKey);
  },

  getEstimateGas(project, parameters) {
    return this.getProvider(project).getEstimateGas(parameters);
  },

  /**
   * Добавление estimateGas к данным транзакции
   *
   * @param project
   * @param transaction
   * @param estimateGas
   * @returns {Promise<*>}
   * @private
   */
  async _wrapEstimateGas(project, transaction, estimateGas = null) {
    if (estimateGas) {
      return estimateGas;
    }

    let params = { ...transaction };
    delete params.nonce;
    return await this.getEstimateGas(project, params);
  },

  /**
   * Добавляем +20% к оценке для безопасности транзакции
   *
   * @param estimateGas
   * @returns {*}
   */
  getEstimateGasWithBonus(estimateGas) {
    return estimateGas.add(estimateGas.div('5'));
  },

  /**
   * Генерируем и отправляем подписанную транзакцию
   *
   * @param project
   * @param nonce
   * @param data
   * @param estimateGas
   * @returns {Promise<any>}
   */
  async sendSignedTransaction(project, nonce, data, estimateGas = null) {
    const transaction = {
      data,
      nonce,
      to: project.contract.ico.address
    };

    const gas = await this._wrapEstimateGas(project, transaction, estimateGas);

    return await this.getProvider(project).sendTransaction({
      ...transaction,
      gasLimit: this.getEstimateGasWithBonus(gas)
    });
  },

  /**
   * Получение баланса пользователя
   *
   * @param project
   * @param identifier
   * @returns {Promise<any>}
   */
  getBalance(project, identifier) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).getBalance(abi, address, identifier);
  },

  /**
   * @param project
   * @returns {Promise<any>}
   */
  getRate(project) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).callContractMethod(abi, address, 'rate');
  },

  /**
   * Получение текущей скидки / бонуса из смарт контракта
   *
   * @param project
   * @returns {Promise<any>}
   */
  getDiscount(project) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).callContractMethod(abi, address, 'getDiscount');
  },

  /**
   * Проверка доступности withdraw из смарт контракта
   *
   * @param project
   * @returns {Promise<boolean>}
   */
  withdrawAvailable(project) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).callContractMethod(abi, address, 'withdrawAvailable'); // TODO
  },

  /**
   * Мультиверсия метода withdraw
   *
   * @param project проект
   * @param nonce актуальный nonce
   * @param identifiers массив идентификаторов
   * @param wallets массив Ethereum кошельков
   *
   * @returns Promise
   */
  withdrawMultiple(project, nonce, identifiers, wallets) {
    return this.sendSignedTransaction(
      project,
      nonce,
      this._getWithdrawMultipleData(project, identifiers, wallets)
    );
  },

  /**
   * Мультиверсия метода withdraw
   *
   * @param project проект
   * @param identifiers массив идентификаторов
   * @param wallets массив Ethereum кошельков
   *
   * @returns {*}
   */
  _getWithdrawMultipleData(project, identifiers, wallets) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).encodeContractMethod(abi, address, 'withdrawMultiple', [
      identifiers,
      wallets
    ]);
  },

  /**
   * Мультиверсия метода withdraw
   *
   * @param project проект
   * @param identifiers массив идентификаторов
   * @param weiAmounts массив Ethereum кошельков
   *
   * @returns {*}
   */
  _getAddTokensMultipleData(project, identifiers, weiAmounts) {
    const {
      abi,
      address
    } = project.contract.ico;

    return this.getProvider(project).encodeContractMethod(abi, address, 'addTokensMultiple', [
      identifiers,
      weiAmounts
    ]);
  },

  /**
   * Мультиверсия метода addTokens
   *
   * @param project проект
   * @param nonce актуальный nonce
   * @param identifiers массив идентификаторов
   * @param weiAmounts массив wei
   *
   * @returns Promise
   */
  addTokensMultiple(project, nonce, identifiers, weiAmounts) {
    return this.sendSignedTransaction(
      project,
      nonce,
      this._getAddTokensMultipleData(project, identifiers, weiAmounts)
    );
  },

  async isQueueBroken(project) {
    const where = {
      project_id: project.id,
      status: ethStatus.ETH_ERROR
    };

    const count = await models.Eth.count({ where }) || 0;

    return count > 0;
  },

  async checkProjectTransactions() {
    const projects = await models.Project.findAll({
      where: { status: projectStatus.PROJECT_READY },
      include: [
        {
          model: models.Blockchain,
          as: 'blockchain'
        }
      ]
    });
    for (let i = 0; i < projects.length; i++) {
      await this.checkTransactions(projects[i]);
    }
  },

  async checkTransactions(project) {
    const rows = await models.Eth.findAll({
      where: {
        project_id: project.id,
        status: ethStatus.ETH_PENDING,
        hash: { [Op.not]: null }
      },
      order: [
        ['nonce', 'ASC']
      ]
    });

    for (let i = 0; i < rows.length; i++) {
      await this.checkTransaction(project, rows[i]);
    }
  },

  async checkTransaction(project, transaction) {
    const status = await this.getProvider(project).getTransactionStatus(transaction.hash);
    if (null === status) {
      // Транзакция где то в пути и еще не попала в блок / не замайнена
      return;
    }

    await transaction.update({
      status: status ? ethStatus.ETH_COMPLETE : ethStatus.ETH_ERROR
    });
  },

  async _countAvailableSlots(project, infuraLimit = 32) {
    const count = await models.Eth.count({
      where: {
        project_id: project.id,
        status: ethStatus.ETH_PENDING
      }
    });

    const slots = infuraLimit - (count || 0);

    return slots <= 0 ? 0 : slots;
  },

  async sendProjectTransactions(groupBy = 100) {
    const projects = await models.Project.findAll({
      where: { status: projectStatus.PROJECT_READY },
      include: [
        {
          model: models.Blockchain,
          as: 'blockchain',
          where: {
            code: {
              [Op.not]: 'local'
            }
          }
        }
      ]
    });

    for (let i = 0; i < projects.length; i++) {
      await this.sendTransactions(projects[i], groupBy);
    }
  },

  async sendTransactions(project, groupBy = 100) {
    const address = project.credentials.address;

    const hasBrokenTransactions = await this.isQueueBroken(project);
    if (hasBrokenTransactions) {
      return;
    }

    const nonce = await nonceUtil.getNonce(project, this.getProvider(project));
    let incrementNonce = nonce - 1;

    const slots = await this._countAvailableSlots(project);
    if (slots === 0) {
      // Если свободных слотов в pending пуле инфуры нет, то
      // просто пропускаем эту итерацию
      return;
    }

    const rows = await models.Transaction.findAll({
      where: {
        project_id: project.id,
        // Ищем все не обработанные транзакции
        eth_id: { [Op.is]: null },
        in_sync: false
      },
      include: [
        { model: models.Project, as: 'project' },
        { model: models.Payment, as: 'payment' },
        { model: models.User, as: 'user' }
      ],
      order: [
        ['id', 'ASC']
      ],
      limit: slots
    });

    const create = [];
    const withdraw = [];

    ///// TODO
    ///// TODO await or not?
    ///// TODO

    // Сортируем данные по типу операции
    for (let i = 0; i < rows.length; i++) {
      if (rows[i].status === transactionStatus.TRANSACTION_WITHDRAW) {
        withdraw.push(rows[i]);
      } else {
        create.push(rows[i]);
      }
    }

    for (let i = 0, j = create.length; i < j; i += groupBy) {
      incrementNonce += 1;
      await this.sendTokens(
        project,
        incrementNonce,
        address,
        create.slice(i, i + groupBy)
      );
    }

    for (let i = 0, j = withdraw.length; i < j; i += groupBy) {
      incrementNonce += 1;
      await this.sendWithdraw(
        project,
        incrementNonce,
        address,
        withdraw.slice(i, i + groupBy)
      );
    }
  },

  async checkProjectBalances() {
    const projects = await models.Project.findAll({
      include: [
        {
          model: models.Blockchain,
          as: 'blockchain',
          where: {
            code: {
              [Op.not]: 'local'
            }
          }
        }
      ]
    });
    for (let i = 0; i < projects.length; i++) {
      const project = projects[i];

      const users = await models.User.findAll({
        where: { project_id: project.id }
      });

      for (let z = 0; z < users.length; z++) {
        const user = users[z];

        const hash = keccak256Util.signUser(user.id);
        const balance = await this.getBalance(
          project,
          hash
        );

        console.log({ project: project.subdomain, user: user.email, balance });
      }
    }
  }
};
