const bcrypt = require('bcrypt');
const rounds = 13;

module.exports = {
  /**
   * Создание хеша пароля
   *
   * @param password
   * @returns {string}
   */
  hash(password) {
    return bcrypt.hash(password, rounds);
  },

  /**
   * Сравнение пароля с хешем
   *
   * @param password
   * @param hash
   * @returns {bool}
   */
  compare(password, hash) {
    return bcrypt.compare(password, hash);
  },

  /**
   * Генерация случайной последовательности
   * для user.token и аналогов
   *
   * @returns {string}
   */
  token() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  }
};
