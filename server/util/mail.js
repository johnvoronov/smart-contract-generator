const path = require('path');
const nunjucks = require('../service/template');
const nodemailer = require('nodemailer');
const config = require('../config/mail');
const appConfig = require('../config/app');

module.exports = {
  render(template, data) {
    const subject = String(nunjucks.render(path.join(template, 'subject.txt'), data)).trim();
    const html = String(nunjucks.render(path.join(template, 'body.html'), { ...data, subject })).trim();
    const text = String(nunjucks.render(path.join(template, 'body.txt'), { ...data, subject })).trim();

    return {
      subject,
      html,
      text
    };
  },

  async send(to, template, data, transport) {
    transport = transport || nodemailer.createTransport(config.transport);

    const domain = appConfig.frontend_url;
    const { subject, html, text } = this.render(template, {
      ...data,
      frontend_url: domain.charAt(domain.length - 1) === '/'
        ? domain.slice(0, domain.length - 1)
        : domain
    });

    return transport.sendMail({
      to, text, html, subject, from: config.from
    });
  }
};
