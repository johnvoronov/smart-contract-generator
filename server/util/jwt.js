const jwt = require('jsonwebtoken');
const config = require('../config/app');

module.exports = {
  /**
   * @returns {string[]}
   */
  getPublicAttributes() {
    return [
      'id',
      'email',
      'is_active',
      'is_admin',
      'refresh_token',
      'referral_id',
      'owner_id',
      'parents',
      'created_at',
      'eth_address'
    ];
  },

  /**
   * @param user
   * @returns {{user: *|{permissions}, jwt: *}}
   */
  response(user) {
    return {
      user: this.getPublicFields(user),
      jwt: this.signUser(user)
    };
  },

  /**
   * Возвращаем в виде объекта только доступные для публичного просмотра поля из модели User
   *
   * @param user
   * @returns object
   */
  getPublicFields(user) {
    const attributes = this.getPublicAttributes();

    let params = {};

    for (let i = 0; i < attributes.length; i++) {
      const attribute = attributes[i];
      params[attribute] = user[attribute];
    }

    let permissions = [];
    if (user.is_admin || user.is_superuser) {
      permissions.push('admin');
    }

    return { ...params, permissions };
  },

  verify(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.jwt_secret, (err, decodedToken) => {
        if (err || !decodedToken) {
          return reject(err)
        }

        resolve(decodedToken)
      })
    })
  },

  signUser(user) {
    return this.sign(this.getPublicFields(user));
  },

  sign(data = {}) {
    const options = {
      expiresIn: Math.floor(Date.now() / 1000) + (60 * 60) // 1 hour
    };

    return jwt.sign(data, config.jwt_secret, options);
  }
};
