const models = require('../models');
const discount = require('../service/discount');
const calculator = require('./calculator');

module.exports = {
  /**
   * @param status
   * @param data
   * @returns {Promise<any>}
   */
  async callback(status, data) {
    const wallet = await models.Wallet.findOne({
      where: { address: data.address }
    });

    if (null === wallet) {
      throw new Error('Wallet not found');
    }

    const project = await wallet.getProject();

    const payment = await models.Payment.findOne({
      where: {
        project_id: wallet.project_id,
        transaction_id: String(data.txn_id),
        user_id: wallet.user_id
      }
    });

    if (payment) {
      await payment.update({ status });

      return { wallet, payment };
    } else {
      const currency = await wallet.getCurrency();
      const tokenPrice = 1 / project.parameters.rate;
      const tokens_amount = calculator.calcTokenAmount(
        tokenPrice,
        data.amount,
        currency.to_eth
      );
      const payment = await models.Payment.create({
        project_id: wallet.project_id,
        wallet_id: wallet.id,
        user_id: wallet.user_id,

        to_eth: currency.to_eth,
        to_usd: currency.to_usd,
        currency_id: currency.id,

        transaction_id: data.txn_id,
        fee: data.fee,
        amount: data.amount,
        tokens_amount,
        token_price: tokenPrice,
        discount: await discount.getDiscount(project),
        status
      });

      return { wallet, payment };
    }
  }
};
