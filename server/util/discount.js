module.exports = {
  _toDate(str) {
    return str instanceof Date ? str : new Date(str);
  },

  /**
   * Возвращаем актуальное кол-во бонусных процентов (скидки)
   * на текущий момент времени
   *
   * @param currentDate
   * @param intervals
   * @returns {*}
   */
  getDiscount(currentDate, intervals = []) {
    if (intervals.length === 0) {
      return 0;
    }

    const first = this._toDate(intervals[0].date);
    const last = this._toDate(intervals[intervals.length - 1].date);
    const date = this._toDate(currentDate);

    if (
      first.getTime() <= date.getTime()
      && date.getTime() >= last.getTime()
    ) {
      return 0;
    }

    const interval = this.getCurrentInterval(currentDate, intervals);

    return interval ? interval.discount || 0 : 0;
  },

  getNextInterval(currentDate, intervals = []) {
    const date = this._toDate(currentDate);

    for (let i in intervals) {
      const interval = intervals[i];
      const next = this._toDate(interval.date);
      if (next.getTime() >= date.getTime()) {
        return interval;
      }
    }

    return null;
  },

  _isInInterval(current, prev, next) {
    if (!next) {
      return current.getTime() >= prev.getTime();
    }

    return current.getTime() >= prev.getTime() && current.getTime() <= next.getTime();
  },

  getCurrentInterval(currentDate, intervals = []) {
    const date = this._toDate(currentDate);

    let last = null;
    for (let i = 0; i < intervals.length; i++) {
      const interval = intervals[i];
      const current = this._toDate(intervals[i].date);
      const next = typeof intervals[i + 1] !== 'undefined' ?
        this._toDate(intervals[i + 1].date) :
        null;

      if (this._isInInterval(date, current, next)) {
        last = interval;
      }
    }

    return last;
  },

  /**
   * Возвращаем следующую существующую дату интервала для
   * обратного счетчика на frontend
   *
   * @param currentDate
   * @param intervals
   * @returns {*}
   */
  getNextDate(currentDate, intervals = []) {
    const next = this.getNextInterval(currentDate, intervals);

    return next ? this._toDate(next.date) : null;
  }
};
