const ethers = require('ethers');

module.exports = {
  /**
   * Hash user id into keccak256
   *
   * Python equal app:
   * #!/usr/bin/python3
   *
   * import sha3
   * keccak = sha3.keccak_256()
   * keccak.update("12345")
   * print(keccak.hexdigest()[24:])
   *
   * @param value {number}
   * @returns {String}
   */
  signUser(value) {
    const id = ethers.utils.id(value.toString()).substring(26);
    return `0x${id}`;
  },
};
