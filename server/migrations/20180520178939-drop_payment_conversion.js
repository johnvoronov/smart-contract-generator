module.exports = {
  up: async (queryInterface, Sequelize) => {
    try {
      await queryInterface.removeColumn('payment', 'conversion');
    } catch (e) {

    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'conversion', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
  }
};
