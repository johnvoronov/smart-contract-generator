module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable('project', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false
        },
        subdomain: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true,
        },
        parameters: {
          type: Sequelize.JSON,
          allowNull: true
        },
        token: {
          type: Sequelize.JSON,
          allowNull: true
        },
        coinpayments: {
          type: Sequelize.JSON,
          allowNull: true
        },
        contract: {
          type: Sequelize.JSON,
          allowNull: true
        },
        status: {
          type: Sequelize.INTEGER,
          defaultValue: 0,
          allowNull: false,
        },
        is_paid: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        blockchain_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          onDelete: 'set null',
          references: {
            model: 'blockchain',
            key: 'id'
          }
        },
        user_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          onDelete: 'set null',
          references: {
            model: 'user',
            key: 'id'
          }
        },
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('project');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
