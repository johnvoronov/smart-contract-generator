module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'is_referrer');
    await queryInterface.removeColumn('transaction', 'is_bonus');
    await queryInterface.removeColumn('transaction', 'is_withdraw');
    await queryInterface.addColumn('transaction', 'status', {
      type: Sequelize.INTEGER,
      allowNull: false
    });
  },

  down: async (queryInterface, Sequelize) => {

  }
};
