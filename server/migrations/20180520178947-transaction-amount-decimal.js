module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('transaction', 'amount', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('transaction', 'amount', {
      type: Sequelize.DOUBLE,
      allowNull: false
    });
  }
};
