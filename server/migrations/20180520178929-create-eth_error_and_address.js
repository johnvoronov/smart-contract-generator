module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('eth', 'address', {
        type: Sequelize.STRING,
        allowNull: true
      }),
      queryInterface.addColumn('eth', 'error', {
        type: Sequelize.TEXT,
        allowNull: true
      })
    ]);
  },
  down: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('eth', 'address'),
      queryInterface.removeColumn('eth', 'error')
    ]);
  }
};
