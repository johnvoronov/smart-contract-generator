module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user', 'referral_path');
    await queryInterface.addColumn('user', 'parents', {
      type: Sequelize.ARRAY(Sequelize.INTEGER),
      allowNull: true
    });
  },

  down: async (queryInterface, Sequelize) => {
  }
};
