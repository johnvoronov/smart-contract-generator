module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('project', 'customer_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'set null',
        references: {
          model: 'customer',
          key: 'id'
        }
      }, { transaction });
      await queryInterface.removeColumn('project', 'user_id', { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('project', 'customer_id', { transaction });
      await queryInterface.addColumn('project', 'user_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'set null',
        references: {
          model: 'user',
          key: 'id'
        }
      }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
