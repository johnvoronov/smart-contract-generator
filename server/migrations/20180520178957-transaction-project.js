module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('transaction', 'project_id', {
        type: Sequelize.INTEGER,
        allowNull: true,
        onDelete: 'set null',
        references: {
          model: 'project',
          key: 'id'
        }
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('transaction', 'project_id');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
