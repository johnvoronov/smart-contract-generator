module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('eth', 'nonce', {
      type: Sequelize.INTEGER,
      allowNull: true
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('eth', 'nonce');
  }
};
