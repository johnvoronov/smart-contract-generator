module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('customer', 'refresh_token', {
          type: Sequelize.STRING,
          allowNull: true
        }, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('customer', 'refresh_token');
  }
};
