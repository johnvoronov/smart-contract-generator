module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('referral', 'amount', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('referral', 'amount');
  }
};
