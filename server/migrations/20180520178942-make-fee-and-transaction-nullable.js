module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('payment', 'fee', {
      type: Sequelize.DOUBLE,
      allowNull: true
    });
    await queryInterface.changeColumn('payment', 'transaction_id', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },

  down: async (queryInterface, Sequelize) => {
  }
};
