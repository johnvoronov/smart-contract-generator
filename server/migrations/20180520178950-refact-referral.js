module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('referral', 'level');
    await queryInterface.removeColumn('referral', 'percent');
    await queryInterface.removeColumn('referral', 'user_id');
    await queryInterface.addColumn('referral', 'name', {
      type: Sequelize.STRING,
      allowNull: false
    });
    await queryInterface.addColumn('referral', 'user', {
      type: Sequelize.DOUBLE,
      allowNull: false
    });
    await queryInterface.addColumn('referral', 'team', {
      type: Sequelize.DOUBLE,
      allowNull: false
    });
    await queryInterface.addColumn('referral', 'levels', {
      type: Sequelize.ARRAY(Sequelize.DOUBLE),
      allowNull: true
    });
  },

  down: async (queryInterface, Sequelize) => {
  }
};
