module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('user', 'owner_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'user',
        key: 'id',
        as: 'owner_id'
      }
    });
    await queryInterface.addColumn('user', 'referral_path', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('user', 'owner_id');
    await queryInterface.removeColumn('user', 'referral_path');
  }
};
