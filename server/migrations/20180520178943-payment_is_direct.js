module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'is_direct', {
      type: Sequelize.BOOLEAN,
      allowNull: false,
      defaultValue: false
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('payment', 'is_direct');
  }
};
