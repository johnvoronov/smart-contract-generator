module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('eth', 'hash', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('eth', 'hash', {
      type: Sequelize.STRING,
      allowNull: false
    });
  }
};
