module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable('project_currency', {
        project_id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: 'project',
            key: 'id'
          }
        },
        currency_id: {
          primaryKey: true,
          type: Sequelize.INTEGER,
          onDelete: 'cascade',
          references: {
            model: 'currency',
            key: 'id'
          }
        }
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('project_currency');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
