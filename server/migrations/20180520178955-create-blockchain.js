module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable('blockchain', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        code: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        price: {
          type: Sequelize.DECIMAL(27, 18),
          allowNull: false
        },
        currency_id: {
          type: Sequelize.INTEGER,
          allowNull: true,
          onDelete: 'set null',
          references: {
            model: 'currency',
            key: 'id'
          }
        },
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('blockchain');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
