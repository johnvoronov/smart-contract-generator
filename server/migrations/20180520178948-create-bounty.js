module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('bounty', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: false
      },
      amount: {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false
      },
      is_enabled: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('bounty');
  }
};
