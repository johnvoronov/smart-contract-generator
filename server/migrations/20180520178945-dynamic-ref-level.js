module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('referral', 'is_done');
    await queryInterface.removeColumn('referral', 'payment_id');
    await queryInterface.removeColumn('referral', 'amount');
    await queryInterface.removeColumn('referral', 'exchange_rate');
    await queryInterface.removeColumn('referral', 'created_at');
    await queryInterface.removeColumn('referral', 'updated_at');
  },

  down: async (queryInterface, Sequelize) => {

  }
};
