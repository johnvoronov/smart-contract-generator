module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('payment', 'status', {
      type: Sequelize.INTEGER
    });
    await queryInterface.sequelize.query('UPDATE payment SET status = 1 WHERE is_complete = true', {
      type: queryInterface.sequelize.QueryTypes.TYPE_UPDATE
    });
    await queryInterface.sequelize.query('UPDATE payment SET status = 2 WHERE is_cancelled = true', {
      type: queryInterface.sequelize.QueryTypes.TYPE_UPDATE
    });
    await queryInterface.sequelize.query('UPDATE payment SET status = 0 WHERE is_cancelled = false AND is_complete = false', {
      type: queryInterface.sequelize.QueryTypes.TYPE_UPDATE
    });

    await queryInterface.removeColumn('payment', 'is_complete');
    await queryInterface.removeColumn('payment', 'is_cancelled');
    await queryInterface.removeColumn('payment', 'is_pending');
  },

  down: async (queryInterface, Sequelize) => {
  }
};
