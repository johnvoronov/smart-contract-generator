module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('referral', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      tokens_amount: {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false
      },
      exchange_rate: {
        type: Sequelize.DECIMAL(27, 18),
        allowNull: false
      },
      level: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: false
      },
      percent: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0
      },
      is_done: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      user_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'user',
          key: 'id',
          as: 'user_id'
        }
      },
      payment_id: {
        type: Sequelize.INTEGER,
        onDelete: 'CASCADE',
        references: {
          model: 'payment',
          key: 'id',
          as: 'payment_id'
        }
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('referral');
  }
};
