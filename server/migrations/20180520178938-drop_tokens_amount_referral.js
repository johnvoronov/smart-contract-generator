module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('referral', 'tokens_amount');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('referral', 'tokens_amount', {
      type: Sequelize.DECIMAL(27, 18),
      allowNull: false
    });
  }
};
