module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('referral', 'user_count', {
      type: Sequelize.INTEGER,
      allowNull: false,
      defaultValue: 0
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('referral', 'user_count');
  }
};
