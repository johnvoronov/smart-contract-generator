module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('transaction', 'bounty_id', {
      type: Sequelize.INTEGER,
      allowNull: true,
      references: {
        model: 'bounty',
        key: 'id',
        as: 'bounty_id'
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('transaction', 'bounty_id');
  }
};
