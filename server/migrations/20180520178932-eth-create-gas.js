module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('eth', 'gas', {
      allowNull: true,
      type: Sequelize.INTEGER
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('eth', 'gas');
  }
};
