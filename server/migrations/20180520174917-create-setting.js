module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('setting', {
      path: {
        type: Sequelize.STRING,
        primaryKey: true
      },
      value: {
        type: Sequelize.STRING
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('setting');
  }
};
