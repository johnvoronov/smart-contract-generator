module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.addColumn('project', 'settings', {
        type: Sequelize.JSON,
        allowNull: true,
      });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.removeColumn('project', 'settings');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
