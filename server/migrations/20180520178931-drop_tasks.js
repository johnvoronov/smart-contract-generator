module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('tasks');
  },
  down: (queryInterface, Sequelize) => {
    return new Error('Migration can not be reversed');
  }
};
