module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('user', 'eth_address', {
      type: Sequelize.STRING,
      allowNull: true
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('user', 'eth_address');
  }
};
