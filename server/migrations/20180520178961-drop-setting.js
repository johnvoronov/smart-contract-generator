module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.dropTable('setting');

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {

  }
};
