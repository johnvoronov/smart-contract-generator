const fs = require('fs');
const path = require('path');
const basename = path.basename(module.filename);
const languages = {};

const ext = '.json';

const validate = file => {
  if (file.charAt(0) === '.' || file === basename) {
    return false;
  }

  return file.slice(-ext.length) === ext
};

fs.readdirSync(__dirname)
  .filter(file => validate(file))
  .forEach(file => {
    languages[path.basename(file, ext)] = require(path.resolve(__dirname, file));
  });

module.exports = languages;
