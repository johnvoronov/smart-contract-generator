const models = require('../models');
const { projectStatus } = require('../consts');

module.exports = async (req, res, next) => {
  if (req.subdomains <= 1) {
    return res.boom.notFound('bad subdomain request');
  }

  const project = await models.Project.findOne({
    where: {
      status: projectStatus.PROJECT_READY,
      subdomain: req.subdomains.pop().toLowerCase()
    },
    include: [
      { model: models.Blockchain, as: 'blockchain' }
    ]
  });

  if (null === project) {
    return res.boom.notFound('project not found');
  }

  req.project = project;
  return next();
};
