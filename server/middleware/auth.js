const jwt = require('../util/jwt');
const models = require('../models');

module.exports = async (req, res, next) => {
  const queryToken = (req.query || {}).token || '';
  const auth = (req.headers || {}).authorization || '';
  if (
    queryToken.length === 0
    && auth.length === 0
  ) {
    return res.boom.unauthorized('no authorization');
  }

  // Поддерживаем два вида авторизации: Bearer и вариант в лоб
  const headerToken = auth.split(' ')[0] === 'Bearer' ? auth.split(' ')[1] : auth;
  const token = headerToken || queryToken;

  try {
    const jwtUser = await jwt.verify(token);
    // Если JWT токен корректный, находим пользователя в базе данных
    // и обновляем информацию о нем
    const where = {
      id: jwtUser.id,
      is_active: true
    };

    const count = await models.User.count({ where });
    if (count === 0) {
      return res.boom.forbidden();
    }

    req.user = jwtUser;
    return next();

  } catch (err) {
    return res.boom.badData('jwt malformed');
  }
};
