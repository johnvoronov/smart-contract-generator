const UnauthorizedError = require('express-jwt-permissions/error');
const joiUtils = require('../util/joiUtils');

module.exports = (error, req, res, next) => {
  if (error.isJoi) {
    return res.boom.badRequest(null, {
      errors: joiUtils.formatJoi(error, req.t)
    });
  }

  if (error instanceof UnauthorizedError) {
    return res.boom.forbidden(null, {
      error: error.toString(),
      statusCode: 403
    });
  }

  if (error instanceof Error) {
    if (process.env.NODE_ENV !== 'test') {
      console.log(error);
    }

    return res.boom.badImplementation(null, {
      error: error.toString(),
      statusCode: 500
    });
  }

  return next(error);
};
