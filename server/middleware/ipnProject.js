module.exports = async (req, res, next) => {
  req.coinpayments = req.project.coinpayments;
  return next();
};
