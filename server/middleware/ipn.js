const coinpaymentsIpn = require('coinpayments-ipn');

module.exports = async (req, res, next) => {
  const {
    merchant_id,
    ipn_secret
  } = req.coinpayments;

  if (!req.get('HMAC')) {
    return next(new Error('HMAC cannot be empty'));
  }

  if (!req.body) {
    return next(new Error('request body cannot be empty'));
  }

  if (
    !req.body.ipn_mode
    || req.body.ipn_mode !== 'hmac'
    || merchant_id !== req.body.merchant
  ) {
    return next(new Error('invalid ipn parameters'));
  }

  try {
    if (!coinpaymentsIpn.verify(req.get('HMAC'), ipn_secret, req.body)) {
      return next(new Error('Hmac calculation does not match'));
    }

    return next();
  } catch (e) {
    return next(e);
  }
};
