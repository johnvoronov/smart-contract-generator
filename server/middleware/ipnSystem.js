const config = require('../config/systemCoinpayments');

module.exports = async (req, res, next) => {
  req.coinpayments = config;
  return next();
};
