module.exports = (sequelize, DataTypes) => {
  const Discount = sequelize.define('Discount', {
    from: {
      type: DataTypes.DATE,
      allowNull: false
    },
    to: {
      type: DataTypes.DATE,
      allowNull: false
    },
    is_fixed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    value: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'discount',
    version: false
  });

  Discount.associate = function (models) {

  };

  return Discount;
};
