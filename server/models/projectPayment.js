const { paymentStatus } = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const ProjectPayment = sequelize.define('ProjectPayment', {
    amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [paymentStatus]
      }
    },
    transaction_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fee: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: true
    },
    amount_btc: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    amount_eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    rate: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'project_payment'
  });

  ProjectPayment.associate = models => {
    ProjectPayment.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    ProjectPayment.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });

    ProjectPayment.belongsTo(models.Wallet, {
      foreignKey: 'wallet_id',
      onDelete: 'CASCADE',
      as: 'wallet'
    });
  };

  return ProjectPayment;
};
