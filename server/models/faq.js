module.exports = (sequelize, DataTypes) => {
  const Faq = sequelize.define('Faq', {
    question: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    answer: {
      type: DataTypes.TEXT,
      allowNull: false
    }
  }, {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'faq',
    version: false
  });

  Faq.associate = models => {
    Faq.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });
  };

  return Faq;
};
