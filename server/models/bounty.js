module.exports = (sequelize, DataTypes) => {
  const Bounty = sequelize.define('Bounty', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    is_enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: false,
    tableName: 'bounty'
  });

  Bounty.associate = models => {

  };

  return Bounty;
};
