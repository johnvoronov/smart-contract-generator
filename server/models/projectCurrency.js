module.exports = (sequelize, DataTypes) => {
  const ProjectCurrency = sequelize.define('ProjectCurrency', {
  }, {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'project_currency',
    version: false
  });

  ProjectCurrency.associate = models => {
    ProjectCurrency.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    ProjectCurrency.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });
  };

  return ProjectCurrency;
};
