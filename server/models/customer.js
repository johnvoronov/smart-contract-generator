const speakeasy = require('speakeasy');
const config = require('../config/app');
const passwordUtil = require('../util/password');

module.exports = (sequelize, DataTypes) => {
  const Customer = sequelize.define('Customer', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    refresh_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_two_factor: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    two_factor_secret: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'customer',
    hooks: {
      beforeCreate: (user, options) => {
        user.two_factor_secret = speakeasy.generateSecret({
          issuer: config.name,
          name: `${config.name} (${ user.email })`,
          length: 64
        }).base32;
        user.token = passwordUtil.token();
        user.refresh_token = passwordUtil.token();
      }
    }
  });

  Customer.associate = models => {

  };

  return Customer;
};
