const { paymentStatus } = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Payment = sequelize.define('Payment', {
    amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    is_direct: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [
          Object.values(paymentStatus)
        ]
      }
    },
    transaction_id: {
      type: DataTypes.STRING,
      allowNull: true
    },
    fee: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: true
    },
    token_price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    discount: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
    tokens_amount: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    to_usd: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    },
    to_eth: {
      type: DataTypes.DECIMAL(27, 18),
      allowNull: false
    }
  }, {
    timestamps: true,
    paranoid: true,
    underscored: true,
    freezeTableName: true,
    tableName: 'payment',
    version: false
  });

  Payment.associate = models => {
    Payment.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    Payment.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Payment.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });

    Payment.belongsTo(models.Wallet, {
      foreignKey: 'wallet_id',
      onDelete: 'CASCADE',
      as: 'wallet'
    });
  };

  return Payment;
};
