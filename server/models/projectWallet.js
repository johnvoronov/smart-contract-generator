module.exports = (sequelize, DataTypes) => {
  const ProjectWallet = sequelize.define('ProjectWallet', {
    address: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    timestamps: false,
    tableName: 'project_wallet'
  });

  ProjectWallet.associate = models => {
    ProjectWallet.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    ProjectWallet.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });
  };

  return ProjectWallet;
};
