module.exports = (sequelize, DataTypes) => {
  const Social = sequelize.define('Social', {
    owner_id: DataTypes.STRING,
    provider: DataTypes.STRING,
    parameters: DataTypes.JSON
  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'social',
    version: false
  });

  Social.associate = function (models) {
    Social.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'social',
      allowNull: true
    });
  };

  return Social;
};
