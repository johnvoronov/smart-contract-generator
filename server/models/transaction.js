const { transactionStatus } = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    amount: DataTypes.DECIMAL(27, 18),
    comment: {
      type: DataTypes.STRING,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        isIn: [
          Object.values(transactionStatus)
        ]
      }
    },
    in_sync: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'transaction',
    version: false
  });

  Transaction.associate = models => {
    Transaction.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Transaction.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    Transaction.belongsTo(models.Payment, {
      foreignKey: 'payment_id',
      onDelete: 'CASCADE',
      as: 'payment',
      allowNull: true // TODO add migration for make it nullable
    });

    Transaction.belongsTo(models.Bounty, {
      foreignKey: 'bounty_id',
      onDelete: 'CASCADE',
      as: 'bounty',
      allowNull: true
    });

    Transaction.belongsTo(models.Eth, {
      foreignKey: 'eth_id',
      onDelete: 'CASCADE',
      as: 'eth',
      allowNull: true
    });
  };

  return Transaction;
};
