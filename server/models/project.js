const { projectStatus } = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    subdomain: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    credentials: {
      type: DataTypes.JSON,
      allowNull: true
    },
    parameters: {
      type: DataTypes.JSON,
      allowNull: true
    },
    contract: {
      // eth
      // contract_address
      // owner_private_key
      // owner_address
      type: DataTypes.JSON,
      allowNull: true
    },
    coinpayments: {
      type: DataTypes.JSON,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [
          Object.values(projectStatus)
        ]
      },
      defaultValue: projectStatus.PROJECT_DRAFT,
      allowNull: false
    },
    is_paid: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
  }, {
    timestamps: false,
    tableName: 'project'
  });

  Project.associate = models => {
    Project.belongsToMany(models.Currency, {
      through: 'ProjectCurrency',
      as: 'currencies'
    });

    Project.belongsTo(models.Customer, {
      foreignKey: 'customer_id',
      onDelete: 'CASCADE',
      as: 'customer'
    });

    Project.belongsTo(models.Blockchain, {
      foreignKey: 'blockchain_id',
      onDelete: 'CASCADE',
      as: 'blockchain'
    });
  };

  return Project;
};
