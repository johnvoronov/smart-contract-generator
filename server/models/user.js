const speakeasy = require('speakeasy');
const config = require('../config/app');
const passwordUtil = require('../util/password');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: {
      type: DataTypes.STRING,
      unique: true,
      validate: {
        isEmail: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: true
    },
    token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    eth_address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    refresh_token: {
      type: DataTypes.STRING,
      allowNull: true
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_superuser: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    is_two_factor: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    two_factor_secret: {
      type: DataTypes.STRING,
      allowNull: true
    },
    parents: {
      type: DataTypes.ARRAY(DataTypes.INTEGER),
      allowNull: true
    }
  }, {
    timestamps: true,
    paranoid: true,
    tableName: 'user',
    hooks: {
      beforeCreate: (user, options) => {
        user.two_factor_secret = speakeasy.generateSecret({
          issuer: config.name,
          name: `${config.name} (${ user.email })`,
          length: 64
        }).base32;
        user.token = passwordUtil.token();
        user.refresh_token = passwordUtil.token();
      }
    }
  });

  User.associate = models => {
    User.hasMany(models.Wallet, {
      foreignKey: 'user_id',
      as: 'wallets'
    });

    User.belongsTo(models.Project, {
      foreignKey: 'project_id',
      as: 'project'
    });

    User.belongsTo(models.User, {
      foreignKey: 'referral_id',
      as: 'referral',
      allowNull: true
    });

    User.belongsTo(models.User, {
      foreignKey: 'owner_id',
      as: 'owner',
      allowNull: true
    });
  };

  return User;
};
