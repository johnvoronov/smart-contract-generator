module.exports = (sequelize, DataTypes) => {
  const Currency = sequelize.define('Currency', {
    code: {
      type: DataTypes.STRING,
      unique: true
    },
    name: DataTypes.STRING,
    is_enabled: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    to_usd: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    to_eth: {
      type: DataTypes.DOUBLE,
      allowNull: false
    }
  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'currency',
    version: false
  });

  Currency.associate = models => {
    Currency.belongsToMany(models.Project, {
      through: 'ProjectCurrency',
      as: 'projects'
    });
  };

  return Currency;
};
