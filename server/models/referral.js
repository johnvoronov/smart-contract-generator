module.exports = (sequelize, DataTypes) => {
  const Referral = sequelize.define('Referral', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    team: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    user: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    user_count: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    levels: {
      type: DataTypes.ARRAY(DataTypes.DOUBLE),
      allowNull: false
    }
  }, {
    timestamps: false,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'referral',
    version: false
  });

  Referral.associate = models => {

  };

  return Referral;
};
