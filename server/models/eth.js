const { ethStatus } = require('../consts');

module.exports = (sequelize, DataTypes) => {
  const Eth = sequelize.define('Eth', {
    hash: {
      type: DataTypes.STRING,
      allowNull: true
    },
    nonce: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    },
    error: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    gas: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      defaultValue: ethStatus.ETH_PENDING,
      validate: {
        isIn: [
          Object.values(ethStatus)
        ]
      }
    }
  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'eth',
    version: false
  });

  Eth.associate = models => {
    Eth.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    Eth.hasMany(models.Transaction, {
      foreignKey: 'eth_id',
      as: 'transactions'
    });
  };

  return Eth;
};
