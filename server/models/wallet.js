module.exports = (sequelize, DataTypes) => {
  const Wallet = sequelize.define('Wallet', {
    address: {
      type: DataTypes.STRING,
      allowNull: false
    },
    merchant_id: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    timestamps: true,
    paranoid: false,
    underscored: true,
    freezeTableName: true,
    tableName: 'wallet',
    version: false
  });

  Wallet.associate = models => {
    Wallet.belongsTo(models.User, {
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
      as: 'user'
    });

    Wallet.belongsTo(models.Project, {
      foreignKey: 'project_id',
      onDelete: 'CASCADE',
      as: 'project'
    });

    Wallet.belongsTo(models.Currency, {
      foreignKey: 'currency_id',
      onDelete: 'CASCADE',
      as: 'currency'
    });
  };

  return Wallet;
};
