const password = require('../util/password');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('user', null, { transaction });
      await queryInterface.bulkInsert('user', [
        {
          email: 'user@demo.com',
          password: await password.hash('user@demo.com'),
          is_active: true,
          is_admin: true,
          is_superuser: false,
          created_at: new Date,
          updated_at: new Date
        }
      ], { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('user', null, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
