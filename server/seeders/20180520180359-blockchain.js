module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('blockchain', null, {});
      await queryInterface.bulkInsert('blockchain', [
        {
          name: 'Local',
          code: 'local',
          price: 1
        },
        {
          name: 'Ethereum',
          code: 'ethereum',
          price: 1
        },
        {
          name: 'Quarkchain',
          code: 'quark',
          price: 1
        },
        {
          name: 'Fantom',
          code: 'fantom',
          price: 1
        },
        {
          name: 'everiToken',
          code: 'everitoken',
          price: 1
        }
      ], {});

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('blockchain', null, {});

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
