module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('referral', null, {});
    await queryInterface.bulkInsert('referral', [
      {
        name: 'vip',
        team: 50000,
        user: 5000,
        levels: [10, 5, 3, 1, 0.5, 0.3, 0.3]
      },
      {
        name: 'profi',
        team: 10000,
        user: 1000,
        levels: [9, 4, 1, 0.5, 0.5]
      },
      {
        name: 'partner',
        team: 5000,
        user: 500,
        levels: [7, 2, 1]
      },
      {
        name: 'newbie',
        team: 0,
        user: 100,
        levels: [5]
      },
      {
        name: 'no status',
        team: 0,
        user: 0,
        levels: null
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('referral', null, {});
  }
};
