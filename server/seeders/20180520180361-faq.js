const models = require('../models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('faq', null, {
        transaction
      });

      const projects = await models.Project.findAll();
      for (let i = 0; i < projects.length; i++) {
        const project = projects[i];

        await queryInterface.bulkInsert('faq', [
          {
            project_id: project.id,
            question: 'Is it completely anonymous?',
            answer: 'Yes, as far as you take enough measures to anonymize your activity in the internet. We recommend you to use VPN not only while accessing an investor\'s account. We store your email data only for accounting the referral program and distributing tokens when the ICO will end. We will purge all the email data after the last token will be sent to an investor. Obviously we take care a lot of our own anonymity. And more obviously there\'s no KYC alike procedure here.'
          },
          {
            project_id: project.id,
            question: 'How do I benefit from acquiring ACCP tokens?',
            answer: 'ACCP tokens will be tradable in a few weeks after the ICO will end. We\'re considering to get ACCP token to be listed on few decentralized exchanges. We expect ACCP tokens to grow in price as the result of two main factors. First, all unallocated tokens will be burnt after the ICO ends and, second, ACCP tokens are literally shares of the ACCasino Platform and will be bringing dividends in ETH.'
          },
          {
            project_id: project.id,
            question: 'What exactly is the ACCP token? How does it work?',
            answer: 'The ACCP token is the Ethereum based token (ERC-20). After the project will be launched, all the ACCasino Platform\'s income from rakes will be periodically distributed among token holders. Notice that only amounts higher than 50 000 ACCP will be granted to get dividends, and all these tokens needs to be stored in the ACCasino Client (software).'
          },
          {
            project_id: project.id,
            question: 'Is it legal?',
            answer: 'In your country :D ? We don\'t know. It is not illegal in general like blockchain, e.g. bitcoin or ethereum. That does also mean that ACCasino will never comply for SEC or any other regulating requirements, as long as we advocate true anonymity and decentralization.'
          },
          {
            project_id: project.id,
            question: 'It can be blocked by internet or hosting providers?',
            answer: 'In your country :D ? We don\'t know. We highly recommend you to use VPN while interacting with ACCasino Account and further ACCasino Client. The platform itself will operate as a distributed ledger and game-engine servers on nodes. An example is BitShares DEX project.'
          },
          {
            project_id: project.id,
            question: 'I only wanted to play casino games. Where do I play them?',
            answer: 'We\'re apologize, we don\'t have any yet. We\'ll release our first MVP very shortly, check for the updates in our social media. We rush to develop ACCP first slot machine to play out some bounty tokens.'
          },
          {
            project_id: project.id,
            question: 'I cannot find here any matching question, where can I ask some?',
            answer: 'You can always ask for help in the Support section of the Investor\'s Account.'
          }
        ], {
          transaction
        });
      }

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('faq', null, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
