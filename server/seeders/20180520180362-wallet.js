const models = require('../models');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('wallet', null, { transaction });
      await queryInterface.bulkInsert('wallet', [
        {
          project_id: 1,
          user_id: 1,
          currency_id: 4,
          address: 'test',
          merchant_id: 'test',
          created_at: new Date,
          updated_at: new Date
        }
      ], { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('wallet', null, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
