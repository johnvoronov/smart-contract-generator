module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('bounty', null, {});
    await queryInterface.bulkInsert('bounty', [
      {
        name: 'user.registration',
        amount: 1000,
        is_enabled: true
      },
      {
        name: 'user.registration.mobile',
        amount: 1000,
        is_enabled: true
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('bounty', null, {});
  }
};
