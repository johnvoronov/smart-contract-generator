module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('currency', null, {});
    await queryInterface.bulkInsert('currency', [
      {
        code: 'BCH',
        name: 'Bitcoin Cash',
        is_enabled: true,
        to_usd: 764.099,
        to_eth: 1.6303978594,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'DASH',
        name: 'Dash',
        is_enabled: true,
        to_usd: 241.7,
        to_eth: 0.5157278869,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'ETC',
        name: 'Ethereum Classic',
        is_enabled: true,
        to_usd: 16.4731,
        to_eth: 0.035149512,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'ETH',
        name: 'Ethereum',
        is_enabled: true,
        to_usd: 468.658,
        to_eth: 1,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'ZEC',
        name: 'Zcash',
        is_enabled: true,
        to_usd: 181.426,
        to_eth: 0.3871181117,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'BTC',
        name: 'Bitcoin',
        is_enabled: true,
        to_usd: 6559.48,
        to_eth: 13.9963043413,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'LTC',
        name: 'Litecoin',
        is_enabled: true,
        to_usd: 85.5734,
        to_eth: 0.1825924235,
        created_at: new Date,
        updated_at: new Date
      },
      {
        code: 'XRP',
        name: 'Ripple',
        is_enabled: true,
        to_usd: 0.527169,
        to_eth: 0.001052639,
        created_at: new Date,
        updated_at: new Date
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('currency', null, {});
  }
};
