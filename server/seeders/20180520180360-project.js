const models = require('../models');
const { projectStatus } = require('../consts');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    const customer = await models.Customer.findOne();

    const quark = await models.Blockchain.findOne({
      where: { code: 'quark' }
    });
    const fantom = await models.Blockchain.findOne({
      where: { code: 'fantom' }
    });
    const ethereum = await models.Blockchain.findOne({
      where: { code: 'ethereum' }
    });

    try {
      await queryInterface.bulkDelete('project', null, {});
      await queryInterface.bulkInsert('project', [
        // {
        //   name: 'Eth test',
        //   subdomain: 'eth-test',
        //   credentials: JSON.stringify({
        //     address: '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d',
        //     privateKey: '0xA56543242C369428635518037FFA238461B6DC9E8AF046F0DE11ACEBED2B58CD'
        //   }),
        //   parameters: JSON.stringify({
        //     type: 'mintable',
        //     supply: '',
        //     name: 'example-coin',
        //     symbol: 'exc',
        //     decimals: 18,
        //     rate: 200,
        //     wallet: '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d',
        //     parents: {}
        //   }),
        //   contract: null,
        //   blockchain_id: ethereum.id,
        //   customer_id: customer.id,
        //   coinpayments: JSON.stringify({
        //     public_key: 'ee3f4f8c7b8c128d67aca6018c6d2100831bc55fc4f69f2de7b0a2e143075ee4',
        //     secret_key: 'f47C0E70a8192b9d3768c3671777F7a8183308bcbD1E9a17baD997473E9A51f7',
        //     merchant_id: '30206312cd9818e0f85981b6b332c7f1',
        //     ipn_secret: 'wD3qDPSQNTG4OyaaMTKf'
        //   }),
        //   status: projectStatus.PROJECT_DONE,
        //   is_paid: false
        // },
        {
          name: 'Fantom test',
          subdomain: 'fantom-test',
          credentials: JSON.stringify({
            address: '0xb362e02bffe84e26c5dfd8637c2ed537967ce565',
            privateKey: '0x18b04286f808e267dbce580ea73217cfcb0f3a9b05a551f7e4c2859de24a35fc'
          }),
          coinpayments: JSON.stringify({
            public_key: 'ee3f4f8c7b8c128d67aca6018c6d2100831bc55fc4f69f2de7b0a2e143075ee4',
            secret_key: 'f47C0E70a8192b9d3768c3671777F7a8183308bcbD1E9a17baD997473E9A51f7',
            merchant_id: '30206312cd9818e0f85981b6b332c7f1',
            ipn_secret: 'wD3qDPSQNTG4OyaaMTKf'
          }),
          parameters: JSON.stringify({
            type: 'mintable',
            supply: '',
            name: 'example-coin',
            symbol: 'exc',
            decimals: 18,
            rate: 200,
            wallet: '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d',
            parents: {}
          }),
          contract: null,
          blockchain_id: fantom.id,
          customer_id: customer.id,
          status: projectStatus.PROJECT_DONE,
          is_paid: false
        },
        // {
        //   name: 'Quark test',
        //   subdomain: 'quark-test',
        //   credentials: JSON.stringify({
        //     address: '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183dFBCdfCea',
        //     privateKey: '0xA56543242C369428635518037FFA238461B6DC9E8AF046F0DE11ACEBED2B58CD'
        //   }),
        //   parameters: JSON.stringify({
        //     type: 'mintable',
        //     supply: '',
        //     name: 'example-coin',
        //     symbol: 'exc',
        //     decimals: 18,
        //     rate: 200,
        //     wallet: '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183dFBCdfCea',
        //     parents: {}
        //   }),
        //   contract: null,
        //   blockchain_id: quark.id,
        //   customer_id: customer.id,
        //   coinpayments: JSON.stringify({
        //     public_key: 'ee3f4f8c7b8c128d67aca6018c6d2100831bc55fc4f69f2de7b0a2e143075ee4',
        //     secret_key: 'f47C0E70a8192b9d3768c3671777F7a8183308bcbD1E9a17baD997473E9A51f7',
        //     merchant_id: '30206312cd9818e0f85981b6b332c7f1',
        //     ipn_secret: 'wD3qDPSQNTG4OyaaMTKf'
        //   }),
        //   status: projectStatus.PROJECT_DONE,
        //   is_paid: false
        // }
      ], {});

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },

  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('project', null, {});

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
