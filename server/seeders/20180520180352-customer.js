const password = require('../util/password');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('customer', null, { transaction });
      await queryInterface.bulkInsert('customer', [
        {
          email: 'user@demo.com',
          password: await password.hash('user@demo.com'),
          is_active: true,
          created_at: new Date,
          updated_at: new Date
        }
      ], { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  },
  down: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.bulkDelete('customer', null, { transaction });

      transaction.commit();
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
};
