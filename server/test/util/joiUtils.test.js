const joiUtils = require('../../util/joiUtils');

describe('joi', () => {
  test('formatJoi', () => {
    expect(joiUtils.formatJoi()).toEqual({});

    const err = {
      details: [
        {
          path: ['"foo"', '"bar"'],
          context: {
            label: 'hello',
          },
          message: 'foo.bar hello world'
        },
        {
          path: ['"foo"', '"bar"'],
          context: {
            label: 'hello',
          },
          message: 'foo.bar yo niga'
        }
      ]
    };

    expect(formatJoi(err)).toEqual({
      'foo.bar': [
        'hello hello world',
        'hello yo niga'
      ]
    });

    expect(formatJoi(err, id => 'Привет')).toEqual({
      'foo.bar': [
        'Привет hello world',
        'Привет yo niga'
      ]
    });
  });
});
