const calc = require('../../util/calculator');

describe('util/calculator', () => {
  it('calcTokenBonus', () => {
    expect(calc.calcTokenBonus(100, 10)).toEqual(10);
    expect(calc.calcTokenBonus(100, 0)).toEqual(0);
  });

  it('calculateTokenAmount', () => {
    expect(calc.calcTokenAmount(1, 1, 100)).toEqual(100);
    expect(calc.calcTokenAmount(1, 1, 14)).toEqual(14);
    expect(calc.calcTokenAmount(1, 1, 0.008)).toEqual(0.008);
    expect(calc.calcTokenAmount(0.1, 1, 0.08)).toEqual(0.8);
    expect(calc.calcTokenAmount(1, 2, 10)).toEqual(20);
  });
});
