const mail = require('../../util/mail');

describe('util/mail', () => {
  it('send', async () => {
    const result = await mail.send('foo@bar.com', '_test');
    const msg = JSON.parse(result.message);

    expect(msg.from).toEqual({
      address: expect.any(String),
      name: ''
    });
    expect(msg.to).toEqual([
      { address: 'foo@bar.com', name: '' }
    ]);
    expect(msg.messageId).toBeTruthy();
  });
});
