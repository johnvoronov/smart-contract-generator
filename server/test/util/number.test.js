const numberUtil = require('../../util/number');

describe('util/number', () => {
  test('unscienceNumber', () => {
    expect(numberUtil.unscienceNumber(1)).toEqual('1');
    expect(numberUtil.unscienceNumber(3.125e7)).toEqual('31250000');
    expect(numberUtil.unscienceNumber(3e-17)).toEqual('0.00000000000000003');
  });
});
