const { callback } = require('../../util/coinpayment_events');
const { paymentStatus } = require('../../consts');
const models = require('../../models');

describe('util/coinpayment_events', () => {
  let project;
  let user;
  let currency;
  let wallet;
  let blockchain;

  async function prepare(data = {}) {
    user = await models.User.create({
      email: 'oleg@gazmanov.com'
    });

    currency = await models.Currency.create({
      code: 'test',
      ...data
    });

    blockchain = await models.Blockchain.create({
      name: 'Ethereum',
      price: 1,
      currency_id: currency.id
    });

    project = await models.Project.create({
      name: 'test',
      blockchain_id: blockchain.id,
      user_id: user.id,
      token: {
        price: 0.02
      },
      coinpayments: {
        publicKey: '123',
        secretKey: '123',
        ipnSecret: '123',
        merchantId: '123'
      }
    });

    wallet = await models.Wallet.create({
      address: 'test',
      user_id: user.id,
      project_id: project.id,
      currency_id: currency.id
    });
  }

  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  const defaultPayment = {
    txn_id: '1',
    fee: 0,
    to_eth: 0,
    to_usd: 0,
    conversion: 'USD',
    address: 'test'
  };

  test('pending', async () => {
    await prepare({ to_usd: 123, to_eth: 14 });

    const { payment } = await callback(paymentStatus.PAYMENT_PENDING, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(paymentStatus.PAYMENT_PENDING);
  });

  test('complete', async () => {
    await prepare({ to_usd: 123, to_eth: 14 });

    const { payment } = await callback(paymentStatus.PAYMENT_COMPLETE, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(paymentStatus.PAYMENT_COMPLETE);
  });

  test('fail', async () => {
    await prepare({ to_usd: 123, to_eth: 14 });

    const { payment } = await callback(paymentStatus.PAYMENT_ERROR, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(paymentStatus.PAYMENT_ERROR);
  });

  test('real#1', async () => {
    await prepare({ to_usd: 123, to_eth: 14 });
    expect(project.token.price).toEqual(0.02);

    const { payment } = await callback(paymentStatus.PAYMENT_PENDING, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });
    expect(payment.status).toEqual(paymentStatus.PAYMENT_PENDING);
    expect(parseFloat(payment.amount)).toEqual(1);
    expect(parseFloat(payment.tokens_amount)).toEqual(700);
    expect(parseFloat(payment.token_price)).toEqual(0.02);
  });

  test('real#2', async () => {
    await prepare({ to_usd: 466, to_eth: 1 });
    expect(project.token.price).toEqual(0.02);

    const { payment } = await callback(paymentStatus.PAYMENT_PENDING, {
      amount: 1,
      currency: 'test',
      ...defaultPayment
    });

    expect(payment.status).toEqual(paymentStatus.PAYMENT_PENDING);
    expect(parseFloat(payment.amount)).toEqual(1);
    expect(parseFloat(payment.tokens_amount)).toEqual(50);
    expect(parseFloat(payment.token_price)).toEqual(0.02);
  });
});
