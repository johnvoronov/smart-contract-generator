const discount = require('../../util/discount');

/**
 * Example:
 *
 * '2010-10-10': 10, (до этой даты - 0, после этой даты - 10)
 * '2012-10-10': 20, (после этой даты - 20)
 *
 * Важно! Если хотите обнулить бонус в интервал времени - обнуляйте
 * его явным образом:
 *
 * '2013-10-10': 0, (после этой даты - 0)
 */
const exampleDiscount = [
  // https://blockchaincorp.atlassian.net/browse/ICC-29
  // +30%/+20%/+10%/0%
  // с 13авг/ с 13сент/ с 13 окт/ с 13 ноября 90 дней
  { date: '2018-08-13', discount: 30 },
  { date: '2018-09-13', discount: 20 },
  { date: '2018-10-13', discount: 10 },
  { date: '2018-11-13', discount: 0 }
];

const intervals = [
  { date: '2018-01-01', discount: 10 },
  { date: '2018-06-13', discount: 20 },
  { date: '2018-08-21', discount: 30 },
  { date: '2019-12-13', discount: 40 },
  { date: '2020-01-01', discount: 0 }
];

describe('service/discount', () => {
  it('getDiscount (per project)', () => {
    expect(discount.getDiscount('2019-08-12', exampleDiscount)).toEqual(0);
    expect(discount.getDiscount('2018-08-12', exampleDiscount)).toEqual(0);
    expect(discount.getDiscount('2018-08-13', exampleDiscount)).toEqual(30);
    expect(discount.getDiscount('2018-09-13', exampleDiscount)).toEqual(20);
    expect(discount.getDiscount('2018-10-13', exampleDiscount)).toEqual(10);
    expect(discount.getDiscount('2018-11-13', exampleDiscount)).toEqual(0);
  });

  it('getDiscount', () => {
    expect(discount.getDiscount('2009-10-10', intervals)).toEqual(0);
    expect(discount.getDiscount('2018-07-19', intervals)).toEqual(20);
    expect(discount.getDiscount('2018-12-31', intervals)).toEqual(30);
    expect(discount.getDiscount('2019-12-31', intervals)).toEqual(40);
    expect(discount.getDiscount('2020-12-31', intervals)).toEqual(0);
  });

  it('getNextDate', () => {
    expect(discount.getNextDate('2009-10-10', intervals).toISOString()).toEqual(new Date('2018-01-01').toISOString());
    expect(discount.getNextDate('2018-08-20', intervals).toISOString()).toEqual(new Date('2018-08-21').toISOString());
    expect(discount.getNextDate('2018-08-21', intervals).toISOString()).toEqual(new Date('2018-08-21').toISOString());
    expect(discount.getNextDate('2018-08-22', intervals).toISOString()).toEqual(new Date('2019-12-13').toISOString());
    expect(discount.getNextDate('2020-12-16', intervals)).toBeFalsy();
  });
});
