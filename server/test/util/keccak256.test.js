const keccak256Utils = require('../../util/keccak256');

describe('util/keccak256', () => {
  test('signUser', () => {
    expect(keccak256Utils.signUser('12345')).toEqual('737b39763d6bd40f569a3ec6859d3305b72310e6');
  });
});
