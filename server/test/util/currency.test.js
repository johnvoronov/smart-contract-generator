const currency = require('../../util/currency');
const models = require('../../models');

describe('util/currency', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('update - create usd if not exists', async () => {
    expect(await models.Currency.count()).toEqual(0);

    await models.Currency.create({
      name: 'Ethereum',
      code: 'ETH',
      to_usd: 0,
      to_eth: 1,
      is_enabled: true
    });
    await currency.update();
    expect(await models.Currency.count()).toEqual(2);

    const eth = await models.Currency.findOne({
      where: { code: 'ETH' }
    });
    expect(eth.to_eth).toEqual(1);

    const usd = await models.Currency.findOne({
      where: { code: 'USD' }
    });
    expect(usd.to_usd).toEqual(1);
    expect(await models.Currency.count()).toEqual(2);
  });

  it('update - update usd if exists', async () => {
    expect(await models.Currency.count()).toEqual(0);

    await models.Currency.create({
      name: 'Ethereum',
      code: 'ETH',
      to_usd: 0,
      to_eth: 0,
      is_enabled: true
    });
    await models.Currency.create({
      name: 'Dollar',
      code: 'USD',
      to_usd: 0,
      to_eth: 0,
      is_enabled: true
    });
    await currency.update();
    expect(await models.Currency.count()).toEqual(2);

    const eth = await models.Currency.findOne({
      where: { code: 'ETH' }
    });
    expect(eth.to_eth).toEqual(1);

    const usd = await models.Currency.findOne({
      where: { code: 'USD' }
    });
    expect(usd.to_usd).toEqual(1);
    expect(await models.Currency.count()).toEqual(2);
  });
});
