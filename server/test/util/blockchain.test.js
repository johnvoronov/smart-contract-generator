const models = require('../../models');
const blockchain = require('../../util/blockchain');

describe('util/blockchain', () => {
  beforeEach(() => models.sequelize.sync({
    force: true
  }));

  test('500 getRate', done => {
    const wait = 500;
    const promises = new Array(wait).map(blockchain.getRate);
    Promise.all(promises).then(values => {
      expect(values.length).toEqual(wait);
      done();
    });
  });

  test('toWei', () => {
    expect(blockchain.toWei(1)).toEqual('1000000000000000000');
    expect(blockchain.toWei(3)).toEqual('3000000000000000000');
    expect(blockchain.toWei(0.003)).toEqual('3000000000000000');
  });

  test('calculateWeiFromTransaction', () => {
    const ltc = {
      'id': 2,
      'code': 'LTC',
      'name': 'Litecoin',
      'is_enabled': 1,
      'to_usd': 85.2334,
      'to_eth': 0.1835320818,
      'created_at': '2018-07-16 10:10:24',
      'updated_at': '2018-07-20 10:00:00'
    };
    const eth = {
      'id': 67,
      'code': 'ETH',
      'name': 'Ethereum',
      'is_enabled': 1,
      'to_usd': 464.406,
      'to_eth': 1,
      'created_at': '2018-07-16 10:10:24',
      'updated_at': '2018-07-20 10:00:00'
    };
    const payment1 = {
      'currency': ltc,
      'amount': 0.0005,
      'discount': 0,
      'tokens_amount': 3.967595,
      'token_price': 0.1,
      'to_usd': 793.519,
      'to_eth': 14
    };
    const payment2 = {
      'currency': eth,
      'amount': 0.003,
      'discount': 10,
      'token_price': 0.0002,
      'tokens_amount': 15,
      'to_usd': 490.441,
      'to_eth': 1
    };
    const payment3 = {
      'currency': eth,
      'amount': 0.003,
      'discount': 10,
      'wallet_id': 2,
      'token_price': 0.0002,
      'tokens_amount': 15,
      'to_usd': 490.441,
      'to_eth': 1
    };
    const payment4 = {
      'currency': eth,
      'amount': 0.003,
      'discount': 20,
      'token_price': 0.0002,
      'tokens_amount': 15,
      'to_usd': 479.528,
      'to_eth': 1
    };
    const payment5 = {
      'currency': eth,
      'amount': 0.003,
      'discount': 20,
      'token_price': 0.0002,
      'tokens_amount': 15,
      'to_usd': 479.528,
      'to_eth': 1
    };

    expect(blockchain.getAmountFromTransaction({
      'amount': 35.16,
      'payment': payment1
    })).toEqual('0.251142857142857143');
    expect(blockchain.toWei('0.251142857142857143')).toEqual('251142857142857143');

    expect(blockchain.getAmountFromTransaction({
      'amount': 13.32,
      'payment': payment1
    })).toEqual('0.095142857142857143');

    expect(blockchain.getAmountFromTransaction({
      'amount': 15,
      'payment': payment2
    })).toEqual('0.003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 1.5,
      'payment': payment2
    })).toEqual('0.0003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 15,
      'payment': payment3
    })).toEqual('0.003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 1.5,
      'payment': payment3
    })).toEqual('0.0003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 15,
      'payment': payment4
    })).toEqual('0.003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 3,
      'payment': payment4
    })).toEqual('0.0006');

    expect(blockchain.getAmountFromTransaction({
      'amount': 15,
      'payment': payment5
    })).toEqual('0.003');

    expect(blockchain.getAmountFromTransaction({
      'amount': 3,
      'payment': payment5
    })).toEqual('0.0006');

    let v = blockchain.getAmountFromTransaction({
      'amount': 0.0011,
      'payment': {
        'currency': eth,
        'amount': 0.0000011,
        'discount': 20,
        'token_price': 0.0002,
        'tokens_amount': 0.0055,
        'to_usd': 463.673,
        'to_eth': 1
      }
    });
    expect(v).toEqual('0.00000022');
    expect(blockchain.toWei(v)).toEqual('220000000000');

    expect(blockchain.getAmountFromTransaction({
      'amount': 0.03317,
      'payment': {
        'currency': eth,
        'amount': 0.00003317,
        'discount': 20,
        'token_price': 0.0002,
        'tokens_amount': 0.16585,
        'to_usd': 463.673,
        'to_eth': 1
      }
    })).toEqual('0.000006634');

    expect(blockchain.getAmountFromTransaction({
      'amount': 0.16585,
      'payment': {
        'currency': eth,
        'amount': 0.00003317,
        'discount': 20,
        'token_price': 0.0002,
        'tokens_amount': 0.16585,
        'to_usd': 463.673,
        'to_eth': 1
      }
    })).toEqual('0.00003317');
  })
});
