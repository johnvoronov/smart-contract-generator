const models = require('../../models');
const nonceUtil = require('../../util/nonce');

const fixtures = [
  {
    'id': 23,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:09:01',
    'updated_at': '2018-07-14 23:09:01',
    'nonce': 2
  },
  {
    'id': 24,
    'hash': null,
    'address': 'test',
    'status': 0,
    'created_at': '2018-07-14 23:13:09',
    'updated_at': '2018-07-14 23:13:09',
    'nonce': 3
  },
  {
    'id': 25,
    'hash': null,
    'address': 'test',
    'status': 0,
    'created_at': '2018-07-14 23:14:03',
    'updated_at': '2018-07-14 23:14:03',
    'nonce': 4
  },
  {
    'id': 26,
    'hash': null,
    'address': 'test',
    'status': 0,
    'created_at': '2018-07-14 23:14:23',
    'updated_at': '2018-07-14 23:14:23',
    'nonce': 5
  },
  {
    'id': 27,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:16:04',
    'updated_at': '2018-07-14 23:16:04',
    'nonce': 6
  },
  {
    'id': 28,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:17:40',
    'updated_at': '2018-07-14 23:17:40',
    'nonce': 7
  },
  {
    'id': 29,
    'hash': null,
    'address': 'test',
    'status': 0,
    'created_at': '2018-07-14 23:19:01',
    'updated_at': '2018-07-14 23:19:01',
    'nonce': 8
  },
  {
    'id': 30,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:19:40',
    'updated_at': '2018-07-14 23:19:40',
    'nonce': 9
  },
  {
    'id': 31,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:20:00',
    'updated_at': '2018-07-14 23:20:00',
    'nonce': 10
  },
  {
    'id': 32,
    'hash': null,
    'address': 'test',
    'status': -1,
    'created_at': '2018-07-14 23:20:11',
    'updated_at': '2018-07-14 23:20:11',
    'nonce': 11
  }
];

describe('util/blockchain', () => {
  beforeEach(async () => {
    await models.sequelize.sync({
      force: true
    });
  });

  test('getNonce from network', async () => {
    const project = await models.Project.create({
      name: 'Test',
      subdomain: 'test',
      code: 'TST',
      contract: {
        contract_address: 'test',
      }
    });
    const provider = {
      getTransactionCount: jest.fn(() => Promise.resolve(1))
    };
    const nonce = await nonceUtil.getNonceFromNetwork(project, provider);
    expect(nonce).toBeTruthy();
  });

  test('getNonce from empty database', async () => {
    const project = await models.Project.create({
      name: 'Test',
      subdomain: 'test',
      code: 'TST',
      contract: {
        contract_address: 'test',
      }
    });
    const nonce = await nonceUtil.getNonceFromDatabase(project);
    expect(nonce).toEqual(0);
  });

  test('getNonce mixed nonce', async () => {
    const project = await models.Project.create({
      name: 'Test',
      subdomain: 'test',
      code: 'TST',
      contract: {
        contract_address: 'test',
      }
    });

    for (let i = 0; i < fixtures.length; i++) {
      await models.Eth.create({
        ...fixtures[i],
        project_id: project.id
      });
    }

    const provider = {
      getTransactionCount: jest.fn(() => Promise.resolve(0))
    };

    const nonce = await nonceUtil.getNonce(project, provider);
    expect(nonce).toBeGreaterThanOrEqual(8);
  });
});
