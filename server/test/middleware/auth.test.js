const auth = require('../../middleware/auth');
const jwt = require('../../util/jwt');

describe('Test Auth Middleware', () => {
  let req;
  let resp;
  let next;

  beforeEach(() => {
    req = {
      query: {},
      body: {},
      headers: {}
    };
    resp = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
      boom: {
        badData: jest.fn(),
        unauthorized: jest.fn()
      }
    };
    next = jest.fn();
  });

  test('next should not be called if no token provided', async () => {
    await auth(req, resp, next);
    expect(next).not.toBeCalled();
  });

  test('should return 401 status code if no token provided', async () => {
    await auth(req, resp, next);
    expect(resp.boom.unauthorized).toBeCalled();
  });

  test('next should not be called if bad token was provided', async () => {
    req.headers.authorization = 'some authorization header';
    auth(req, resp, next);
    expect(next).not.toBeCalled();
  });

  test('req should contain user info if good token was provided', async () => {
    req.headers.authorization = jwt.sign({ id: 1 });
    await auth(req, resp, () => {
      expect(req).to.have.property('user');
      expect(req.user).to.have.property('id');
      expect(req.user.id).toEqual(1);
    });
  });
});
