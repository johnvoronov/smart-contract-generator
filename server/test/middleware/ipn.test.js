const coinpaymentsIpn = require('coinpayments-ipn');
jest.mock('coinpayments-ipn', () => ({
  verify: jest.fn()
}));
const models = require('../../models');
const ipn = require('../../middleware/ipn');

describe('middlware/ipn', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  test('ipn: HMAC cannot be empty', done => {
    ipn({
      get: name => null,
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'hmac',
        merchant: '1234'
      }
    }, null, result => {
      expect(result.toString()).toEqual('Error: HMAC cannot be empty');
      done();
    });
  });

  test('ipn: Hmac calculation does not match', done => {
    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'hmac',
        merchant: '1234'
      }
    }, null, result => {
      expect(result.toString()).toEqual('Error: Hmac calculation does not match');
      done();
    });
  });

  test('ipn: request body cannot be empty', done => {
    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
    }, null, result => {
      expect(result.toString()).toEqual('Error: request body cannot be empty');
      done();
    });
  });

  test('ipn: invalid ipn parameters', done => {
    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'foobar',
        merchant: '1234'
      }
    }, null, result => {
      expect(result.toString()).toEqual('Error: invalid ipn parameters');
      done();
    });
  });

  test('ipn: Hmac calculation does not match', done => {
    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'hmac',
        merchant: '1234'
      }
    }, null, result => {
      expect(result.toString()).toEqual('Error: Hmac calculation does not match');
      done();
    });
  });

  test('ipn: valid', done => {
    coinpaymentsIpn.verify.mockReturnValue(true);
    expect(coinpaymentsIpn.verify()).toEqual(true);

    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'hmac',
        merchant: '1234'
      }
    }, null, result => {
      expect(result).toBeUndefined();
      done();
    });
  });

  test('ipn: bad implementation error', done => {
    coinpaymentsIpn.verify.mockImplementation(() => {
      throw new Error('test');
    });

    ipn({
      get: () => '1234',
      coinpayments: {
        publicKey: '1234',
        secretKey: '1234',
        ipnSecret: '1234',
        merchantId: '1234'
      },
      body: {
        ipn_mode: 'hmac',
        merchant: '1234'
      }
    }, null, result => {
      expect(result.toString()).toEqual('Error: test');
      done();
    });
  });
});
