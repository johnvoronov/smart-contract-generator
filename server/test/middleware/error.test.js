const middleware = require('../../middleware/error');

describe('joi', () => {
  test('middleware', done => {
    const error = {
      isJoi: true,
      details: [
        {
          path: ['"foo"', '"bar"'],
          context: {
            label: 'hello'
          },
          message: 'foo.bar hello world'
        },
        {
          path: ['"foo"', '"bar"'],
          context: {
            label: 'hello'
          },
          message: 'foo.bar yo niga'
        }
      ]
    };

    const res = {
      boom: {
        badRequest: (message, data) => {
          return data;
        }
      }
    };
    const data = middleware(error, {}, res, () => null);
    expect(data).toEqual({
      errors: {
        'foo.bar': [
          'hello hello world',
          'hello yo niga'
        ]
      }
    });

    middleware({ foo: 'bar' }, {}, res, err => {
      expect(err).toEqual({ foo: 'bar' });
      done();
    });
  });
});
