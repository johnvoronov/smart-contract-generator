const Contract = require('../../../service/deployer/contract');
const mock = require('./contract_mock');
const result = require('./contract_result');

describe('service/deployer/contract', () => {
  test('encodeABI', async () => {
    const contract = new Contract(mock.abi, mock.bytecode);
    const res = contract.encodeABI([1, '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d', '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d']);
    expect(res).toEqual(result.bytecode);
  });
});
