const models = require('../../models');
const coinpayments = require('coinpayments');
const projectWalletService = require('../../service/projectWallet');
jest.mock('coinpayments');

describe('service/projectWallet', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => ({
        address: 'test'
      }))
    }));
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  test('createNewWallet', async () => {
    expect(await projectWalletService.createNewWallet({ code: 'test' })).toEqual({
      address: 'test'
    });
  });

  test('getWallet', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const currency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'TST',
      name: 'Test'
    });
    const blockchain = await models.Blockchain.create({
      name: 'Ethereum',
      price: 1,
      currency_id: currency.id
    });
    const project = await models.Project.create({
      name: 'test',
      blockchain_id: blockchain.id,
      user_id: user.id,
    });
    const coinpaymentsWallet = await projectWalletService.getWallet(project, currency);
    expect(coinpaymentsWallet.address).toEqual('test');
    expect(await models.ProjectWallet.count()).toEqual(1);

    const newCurrency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'Foo',
      name: 'Bar'
    });
    const coinpaymentsNewWallet = await projectWalletService.getWallet(project, newCurrency);
    expect(coinpaymentsNewWallet.address).toEqual('test');
    expect(await models.ProjectWallet.count()).toEqual(2);
  });
});
