const models = require('../../models');
const coinpayments = require('coinpayments');
const walletService = require('../../service/wallet');
jest.mock('coinpayments');

describe('service/wallet', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });

    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => ({
        address: 'test'
      }))
    }));
  });

  afterEach(async () => {
    jest.resetAllMocks();
  });

  test('getWallet', async () => {
    const user = await models.User.create({
      email: 'user@user.com'
    });
    const currency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'TST',
      name: 'Test'
    });
    const blockchain = await models.Blockchain.create({
      name: 'Ethereum',
      price: 1,
      currency_id: currency.id
    });
    const project = await models.Project.create({
      name: 'test',
      blockchain_id: blockchain.id,
      user_id: user.id,
      coinpayments: {
        publicKey: '123',
        secretKey: '123',
        ipnSecret: '123',
        merchantId: '123'
      },
    });
    const coinpaymentsWallet = await walletService.getWallet(user, project, currency);
    expect(coinpaymentsWallet.address).toEqual('test');
    expect(await models.Wallet.count()).toEqual(1);

    const newCurrency = await models.Currency.create({
      to_eth: 1,
      to_usd: 1,
      code: 'Foo',
      name: 'Bar'
    });
    const coinpaymentsNewWallet = await walletService.getWallet(user, project, newCurrency);
    expect(coinpaymentsNewWallet.address).toEqual('test');
    expect(await models.Wallet.count()).toEqual(2);
  });
});
