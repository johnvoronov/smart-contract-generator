const GeneratorService = require('../../service/generator');

describe('generator', () => {
  let data;
  beforeEach(async () => {
    data = {
      token: {
        type: 'mintable',
        name: 'Bitcoin',
        symbol: 'BTC',
        decimals: 18
      },
      rate: 200,
      wallet: '0x1fe157F5592a7696c9708c145Dc75B3598577901',
      parents: {
        discountByTokens: {
          discount: 20,
          tokens: 2000
        },
        restrictByCap: {
          softCap: 10000,
          hardCap: 20000
        },
        restrictByTime: {
          openingTime: '10/29/2018 11:07 PM',
          closingTime: '12/29/2018 11:07 PM'
        }
      }
    };
  });

  it('generate-token-burnable', async () => {
    expect(GeneratorService.generateTokenBurnable(data.token))
      .toEqual(expect.any(String))
  });

  it('generate-token-mintable', async () => {
    expect(GeneratorService.generateTokenMintable(data.token))
      .toEqual(expect.any(String))
  });

  it('generate-token-template', async () => {
    expect(GeneratorService.generateTokenTemplate(data))
      .toEqual(expect.any(String))
  });

  it('crowdsale-parents-data', async () => {
    expect(GeneratorService.crowdsaleParentsData())
      .toEqual(expect.any(Object))
  });

  it('generate-crowdsale-template', async () => {
    expect(GeneratorService.generateCrowdsaleTemplate(data))
      .toEqual(expect.any(String))
  });

  it('generate-deployer-template', async () => {
    expect(GeneratorService.generateDeployerTemplate(data))
      .toEqual(expect.any(String))
  });

  it('generate', async () => {
    expect(GeneratorService.generate(data))
      .toEqual(expect.any(Object))
  });

  it('upload', async () => {
    const result = GeneratorService.generate(data)
    expect(await GeneratorService.upload(result))
      .toEqual(true)
  });

  it('zip', async () => {
    const result = GeneratorService.generate(data)
    expect(GeneratorService.zip(result))
      .toEqual(expect.any(Object))
  });

  it('generate-token-burnable', async () => {
    expect(GeneratorService.generateTokenBurnable(data.token))
      .toEqual(expect.any(String))
  });

  it('generate-token-mintable', async () => {
    expect(GeneratorService.generateTokenMintable(data.token))
      .toEqual(expect.any(String))
  });

  it('generate-token-template', async () => {
    expect(GeneratorService.generateTokenTemplate(data))
      .toEqual(expect.any(String))
  });

  it('crowdsale-parents-data', async () => {
    expect(GeneratorService.crowdsaleParentsData())
      .toEqual(expect.any(Object))
  });

  it('generate-crowdsale-template', async () => {
    expect(GeneratorService.generateCrowdsaleTemplate(data))
      .toEqual(expect.any(String))
  });

  it('generate-deployer-template', async () => {
    expect(GeneratorService.generateDeployerTemplate(data))
      .toEqual(expect.any(String))
  });

  it('generate', async () => {
    expect(GeneratorService.generate(data))
      .toEqual(expect.any(Object))
  });

  it('upload', async () => {
    const result = GeneratorService.generate(data)
    expect(await GeneratorService.upload(result))
      .toEqual(true)
  });

  test('zip', async () => {
    const result = GeneratorService.generate(data)
    expect(GeneratorService.zip(result))
      .toEqual(expect.any(Object))
  });
});
