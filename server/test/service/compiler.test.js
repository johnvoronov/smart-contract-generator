const path = require('path');
const compiler = require('../../service/compiler');

describe('service/compiler', () => {
  test('send', async () => {
    const contractPath = path.join(__dirname, 'compiler.fixture.sol');
    const res = compiler.compileFromPath(contractPath);
    expect(res).toEqual(expect.objectContaining({
      bin: expect.any(String),
      abi: expect.any(String),
    }));
  });
});
