const blockchain = require('../../util/blockchain');
const blockchainConfig = require('../../config/blockchain');
const ganache = require('ganache-cli');
const fs = require('fs');
const cache = require('../../service/cache');

// Сначала деплоим токен
const tokenInterface = JSON.parse(fs.readFileSync(blockchainConfig.token_abi_path, 'utf8'));
const tokenBytecode = fs.readFileSync(blockchainConfig.token_byte_path);

// Теперь деплоим crowdsale
const contractInterface = JSON.parse(fs.readFileSync(blockchainConfig.contract_abi_path, 'utf8'));
const contractBytecode = fs.readFileSync(blockchainConfig.contract_byte_path, 'utf8');

describe('service/blockchain', () => {
    let accounts;

    const provider = ganache.provider({
        accounts: [
            {
                balance: 1000 * 10e18, // 100 ether
                secretKey: blockchainConfig.owner_private_key // Будем юзать тот же аккаунт, что и в rinkeby
            }
        ]
    });

    beforeEach(async () => {
        // Придется пересоздавать блокчейн каждый раз, иначе nonce зависит от предыдущих тестов
        // В любом случае, мы должны получать наш локальный провайдер
        blockchain._createHttpProvider = jest.fn(() => provider);
        blockchain.getOwnerPrivateKey = jest.fn(() => blockchainConfig.owner_private_key);

        // Вытаскиваем из blockchain, заодно проверим, что стабы работают
        const web3 = blockchain.getClient();

        // Вытаскиваем аккаунты из сети
        accounts = await web3.eth.getAccounts();

        const token = await new web3.eth.Contract(tokenInterface['abi'])
            .deploy({ data: tokenBytecode, arguments: [] })
            .send({ 'from': accounts[0], 'gas': '3000000' });

        // Rate - 1 к 1 за эфирчик, прямые переводы уйдут на аккаунт владельца
        const contract = await new web3.eth.Contract(contractInterface['abi'])
            .deploy({
                data: contractBytecode,
                arguments: ['1', accounts[0], token.options.address]
            })
            .send({ 'from': accounts[0], 'gas': '3000000' });

        // Заглушки на все данные
        blockchain.getContract = jest.fn(() => contract);
        blockchain.getContractAddress = jest.fn(() => contract.options.address);
        blockchain.getOwnerAddress = jest.fn(() => accounts[0]);

        cache.remove(blockchain.getContractAddress());
    });

    // Сбрасываем stub-ы, чтобы реюзануть их в следующий раз, иначе exception
    afterEach(() => {
        jest.resetAllMocks();
    });

    it('contract was deployed', async () => {
        expect(blockchain.getContract().options.address).toBeTruthy();
        expect(await blockchain.getRate()).toEqual('1');
        expect(await blockchain.getBalance(accounts[0])).toEqual('0');
    });

    /*
    it('test nonce', done => {
        // Адрес владельца, т.к. все транзакции идут от него
        const ownerAddress = accounts[0];
        const web3 = blockchain.getClient();
        blockchain.getContract().getPastEvents('allEvents', {
            fromBlock: 0,
            toBlock: 'latest'
        }, (e, l) => {
            console.log(e, l);
            done();
        });
        const sub = web3.eth.subscribe('logs', {
            address: ownerAddress,
            from: 1,
            to: 'latest'
        });

        const hash = blockchain.signUser(1);
        const wei = blockchain.toWei(1);

        sub.on('data', result => {
            console.log(result);
        });

        blockchain.addTokens(hash, wei).then(() => {
            setTimeout(() => {
                done();
            }, 4000);
        });

        // Владелец уже задеплоил 2 контракта - Token и Crowdsale
        // blockchain.getClient().eth.getTransactionCount(ownerAddress).then(nonce => {
        //     expect(nonce).toEqual(2);
        //
        //     const hash = blockchain.signUser(1);
        //     const wei = blockchain.toWei(1);
        //
        //     const promises = [
        //         blockchain.addTokens(hash, wei),
        //         blockchain.addTokens(hash, wei),
        //         blockchain.addTokens(hash, wei)
        //     ];
        //
        //     Promise.all(promises).then(() => {
        //         blockchain.getClient().eth.getTransactionCount(ownerAddress).then(nonce => {
        //             // Деплой 2х контрактов + 3 транзакции на добавление токенов
        //             expect(nonce).toEqual(5);
        //             done();
        //         });
        //     });
        // });
    });
    */
});
