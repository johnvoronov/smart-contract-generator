const moment = require('moment');
const chart = require('../../../service/dashboard/chart');
const models = require('../../../models');
const {
  PAYMENT_COMPLETE,
  TRANSACTION_BONUS,
  TRANSACTION_PAYMENT
} = require('../../../consts');

describe('service/dashboard/chart', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('getStats', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    const currency = await models.Currency.create({
      code: 'TST',
      name: 'test',
      to_usd: 0,
      to_eth: 0
    });

    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });

    const payment = await models.Payment.create({
      amount: 0.0005,
      transaction_id: '0xeac693d64d94152631f7d81fcef4c089bc1452c7c556900812874e9f6635e49c',
      fee: 0,
      token_price: 0.1,
      status: PAYMENT_COMPLETE,
      tokens_amount: 3.967595,
      to_usd: 793.519,
      to_eth: 14,
      created_at: new Date,
      updated_at: new Date,
      wallet_id: wallet.id,
      user_id: user.id,
      currency_id: currency.id
    });

    await models.Transaction.create({
      amount: 35.16,
      user_id: user.id,
      payment_id: payment.id,
      status: TRANSACTION_PAYMENT,
      created_at: moment().subtract(1, 'day').toDate()
    });
    await models.Transaction.create({
      id: 2,
      amount: 35.16,
      user_id: user.id,
      status: TRANSACTION_BONUS,
      payment_id: payment.id,
      created_at: moment().startOf('day').toDate()
    });
    const data = await chart.getStats('year');
    expect(data).toEqual({
      bonus: [
        { amount: '0', date: '2017' },
        { amount: '35.160000000000000000', date: '2018' }
      ],
      token: [
        { amount: '0', date: '2017' },
        { amount: '35.160000000000000000', date: '2018' }
      ],
      withdraw: [
        { amount: '0', date: '2017' },
        { amount: '0', date: '2018' }
      ]
    });
  });
});
