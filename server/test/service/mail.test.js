const models = require('../../models');
const mail = require('../../service/mail');
const settings = require('../../util/settings');

describe('util/mail', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  it('send', async () => {
    const result = await mail.send('foo@bar.com', '_test');
    const msg = JSON.parse(result.message);

    expect(msg.from).toEqual({
      address: expect.any(String),
      name: ''
    });
    expect(msg.to).toEqual([
      { address: 'foo@bar.com', name: '' }
    ]);
    expect(msg.messageId).toBeTruthy();
  });

  it('send from settings transport', async () => {
    const spy = jest.spyOn(settings, 'fetch');
    const result = await mail.send('foo@bar.com', '_test');
    expect(spy).toHaveBeenCalled();

    const msg = JSON.parse(result.message);

    expect(msg.from).toEqual({
      address: expect.any(String),
      name: ''
    });
    expect(msg.to).toEqual([
      { address: 'foo@bar.com', name: '' }
    ]);
    expect(msg.messageId).toBeTruthy();
  });
});
