const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/currency', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    await models.Currency.create({
      id: 1,
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 0,
      to_eth: 0,
      is_enabled: true
    });

    await request(app)
      .get('/currency')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.length).toEqual(1);
        expect(response.body[0].id).toEqual(1);
      });
  });
});
