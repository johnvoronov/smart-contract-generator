const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/settings', () => {
  beforeEach(() => {
    return models.sequelize.sync({
      force: true
    });
  });

  afterAll(() => models.sequelize.close());

  it('list all public settings', async () => {
    await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });

    const headers = { 'Accept': 'application/json', 'Authorization': jwt.sign({ id: 1 }) };

    await models.Setting.create({
      path: 'foo',
      value: 'bar'
    });
    await models.Setting.create({
      path: 'bar',
      value: 'foo'
    });

    await request(app)
      .get('/settings')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(Object.keys(response.body)).toEqual([
          'foo',
          'bar'
        ]);
      });
  });
});
