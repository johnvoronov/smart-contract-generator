const request = require('supertest');
const coinpayments = require('coinpayments');
const app = require('../../../app');
const models = require('../../models');
const passwordUtil = require('../../util/password');
const jwt = require('../../util/jwt');
jest.mock('coinpayments');

describe('routes/wallet', () => {
  beforeEach(async () => {
    await models.sequelize.sync({
      force: true
    });

    coinpayments.mockImplementation(() => ({
      getCallbackAddress: jest.fn(() => ({
        address: 'test'
      }))
    }));
  });

  test('get / create', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });

    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 0,
      to_eth: 0,
      is_enabled: true
    });

    const blockchain = await models.Blockchain.create({
      name: 'Ethereum',
      price: 1,
      currency_id: currency.id
    });
    const project = await models.Project.create({
      name: 'test',
      blockchain_id: blockchain.id,
      user_id: user.id,
      coinpayments: {
        publicKey: '123',
        secretKey: '123',
        ipnSecret: '123',
        merchantId: '123'
      },
    });

    await request(app)
      .post('/wallet')
      .send({ currency_id: 10 })
      .set('Authorization', jwt.signUser(user))
      .expect(400);
    await request(app)
      .post('/wallet')
      .send({ project_id: 10 })
      .set('Authorization', jwt.signUser(user))
      .expect(400);
    await request(app)
      .post('/wallet')
      .send({ project_id: 10, currency_id: 10 })
      .set('Authorization', jwt.signUser(user))
      .expect(404);

    const resp = await request(app)
      .post('/wallet')
      .send({
        project_id: project.id,
        currency_id: currency.id
      })
      .set('Authorization', jwt.signUser(user))
      .expect('Content-Type', /json/)
      .expect(200);
    expect(resp.body.address).toEqual('test');
  });
});
