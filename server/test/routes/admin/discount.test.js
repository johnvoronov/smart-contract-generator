const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/discount', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    request(app)
      .get('/admin/discount/list')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  it('create', async () => {
    const user = await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .post('/admin/discount')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .send({
        intervals: [
          {
            from: (new Date('10-10-2010')).toISOString(),
            to: (new Date('10-30-2010')).toISOString(),
            is_fixed: true,
            value: 10
          },
          {
            from: (new Date('1-11-2010')).toISOString(),
            to: (new Date('5-11-2010')).toISOString(),
            is_fixed: false,
            value: 10
          }
        ]
      })
      .then(async resp => {
        expect(await models.Discount.count()).toEqual(2);
      });
  });
});
