const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/user', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/user/list')
      .set('Authorization', jwt.signUser(user))
      .expect('Content-Type', /json/)
      .expect(200)
      .then(resp => {
        expect(resp.body.objects[0].stats).toEqual({
          tokens_total: 0,
          tokens_amount: 0,
          tokens_bonus: 0
        })
      });
  });

  it('user/view', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await request(app)
      .get('/admin/user/' + user.id)
      .set('Authorization', jwt.signUser(user))
      .expect('Content-Type', /json/)
      .expect(200)
      .then(resp => {
        expect(resp.body.stats).toEqual({
          tokens_total: 0,
          tokens_amount: 0,
          tokens_bonus: 0
        })
      });
  });
});
