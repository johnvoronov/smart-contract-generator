const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin - auth-permissions', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: false,
      is_admin: false
    });

    await request(app)
      .get('/admin/user/list')
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(403);

    await user.update({
      is_admin: true
    });

    await request(app)
      .get('/admin/user/list')
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);

    await user.update({
      is_admin: false,
      is_superuser: true
    });

    await request(app)
      .get('/admin/user/list')
      .set({ Accept: 'application/json', Authorization: jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200);
  });
});
