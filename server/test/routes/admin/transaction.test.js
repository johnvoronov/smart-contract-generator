const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');
const {
  PAYMENT_COMPLETE,
  TRANSACTION_BONUS
} = require('../../../consts');

describe('routes/admin/transaction', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  test('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    request(app)
      .get('/admin/transaction/list')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  test('view', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    const currency = await models.Currency.create({
      code: 'TST',
      name: 'test',
      to_usd: 0,
      to_eth: 0
    });

    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });

    const payment = await models.Payment.create({
      amount: 0.0005,
      transaction_id: '0xeac693d64d94152631f7d81fcef4c089bc1452c7c556900812874e9f6635e49c',
      fee: 0,
      token_price: 0.1,
      status: PAYMENT_COMPLETE,
      tokens_amount: 3.967595,
      to_usd: 793.519,
      to_eth: 14,
      created_at: new Date,
      updated_at: new Date,
      wallet_id: wallet.id,
      user_id: user.id,
      currency_id: currency.id
    });

    await models.Transaction.create({
      amount: 35.16,
      comment: 'Bonus',
      status: TRANSACTION_BONUS,
      user_id: user.id,
      payment_id: payment.id,
      created_at: new Date,
      updated_at: new Date
    });

    await request(app)
      .get('/admin/transaction/1')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });
});
