const request = require('supertest');
const app = require('../../../../app');
const models = require('../../../models');
const jwt = require('../../../util/jwt');

describe('routes/admin/payment', () => {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true })
  });

  it('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    request(app)
      .get('/admin/payment/list')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });

  it('view', async () => {
    const user = await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      is_active: true,
      is_superuser: true,
      is_admin: true
    });

    await models.Currency.create({
      id: 1,
      code: 'TST',
      name: 'test',
      to_usd: 0,
      to_eth: 0
    });

    await models.Wallet.create({
      id: 1,
      user_id: 1,
      currency_id: 1,
      address: 'test'
    });

    await models.Payment.create({
      id: 1,
      amount: 0.0005,
      transaction_id: '0xeac693d64d94152631f7d81fcef4c089bc1452c7c556900812874e9f6635e49c',
      fee: 0,
      token_price: 0.1,
      is_complete: true,
      tokens_amount: 3.967595,
      conversion: 'USD',
      to_usd: 793.519,
      to_eth: 14,
      is_cancelled: false,
      is_pending: false,
      created_at: new Date,
      updated_at: new Date,
      wallet_id: 1,
      user_id: 1,
      currency_id: 1
    });

    await request(app)
      .get('/admin/payment/1')
      .set({
        'Accept': 'application/json',
        'Authorization': jwt.signUser(user)
      })
      .expect('Content-Type', /json/)
      .expect(200);
  });
});
