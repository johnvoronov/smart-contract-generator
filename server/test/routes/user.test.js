const request = require('supertest');
const speakeasy = require('speakeasy');
const app = require('../../../app');
const models = require('../../models');
const settings = require('../../util/settings');
const passwordUtil = require('../../util/password');
const jwt = require('../../util/jwt');
jest.mock('speakeasy', () => ({
  generateSecret: () => '123456',
  totp: {
    verify: jest.fn()
  }
}));

describe('routes/user', function () {
  beforeEach(async () => {
    await models.sequelize.sync({
      force: true
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('login', async () => {
    await request(app)
      .post('/user/login')
      .send({ name: 'john' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(response.body.errors).toEqual({
          email: ['Email is required'],
          password: ['Password is required']
        });
      });

    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: false
    });

    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    await user.update({
      is_active: true
    });

    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123123' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.user.id).toEqual(user.id);
        expect(response.body.jwt).toBeTruthy();
      });

    await user.update({
      is_two_factor: true
    });

    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(406);

    speakeasy.totp.verify.mockReturnValue(false);
    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456', token: '1234' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400);

    speakeasy.totp.verify.mockReturnValue(true);
    await request(app)
      .post('/user/login')
      .send({ email: 'user@example.com', password: '123456', token: '123456' })
      .set('Accept', 'application/json')
      .expect(200)
      .then(response => {
        expect(response.body.user.id).toEqual(user.id);
        expect(response.body.jwt).toBeTruthy();
      });
  });

  test('registration eth_address', async () => {
    const valid = {
      is_agree: true,
      is_confirm: true,
      email: 'foo@bar.com',
      password: 'qwe!@#eq@w!@#',
      password_confirm: 'qwe!@#eq@w!@#'
    };

    await request(app)
      .post('/user/registration')
      .send({
        ...valid,
        eth_address: 'foobar'
      })
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({
        ...valid,
        eth_address: '123'
      })
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({
        ...valid,
        eth_address: '0x32be343b94f860124dc4fee278fdcbd38c102d88'
      })
      .expect('Content-Type', /json/)
      .expect(201);
  });

  test('registration', async () => {
    await request(app)
      .post('/user/registration')
      .send({
        is_agree: true,
        is_confirm: true,
        email: 'foo@bar.com',
        password: 'qwe!@#eq@w!@#',
        password_confirm: 'qwe!@#eq@w!@#'
      })
      .expect('Content-Type', /json/)
      .expect(201)
      .then(async response => {
        expect(response.body.id).toEqual(1);
      });

    await request(app)
      .post('/user/registration')
      .send({ email: 'user@example.com', password: '123456' })
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({ is_agree: true, is_confirm: true })
      .expect('Content-Type', /json/)
      .expect(400);

    await request(app)
      .post('/user/registration')
      .send({
        is_agree: true,
        is_confirm: true,
        email: 'user@user.com',
        password: '123456',
        password_confirm: '123456'
      })
      .set('Cookie', ['lang=ru'])
      .expect('Content-Type', /json/)
      .expect(400)
      .then(resp => {
        expect(resp.body).toEqual({
          error: 'Bad Request',
          errors: {
            password: ['Cлишком простой пароль']
          },
          statusCode: 400
        })
      });
  });

  test('restore', async () => {
    await models.User.create({
      email: 'user@example.com'
    });

    await request(app)
      .post('/user/restore')
      .send({ email: 'user@example.com' })
      .expect(204);
  });

  test('registration/resend', async () => {
    const user = await models.User.create({
      email: 'user@example.com'
    });

    await request(app)
      .post('/user/registration/resend')
      .send({ email: 'user@example.com' })
      .set('Authorization', jwt.signUser(user))
      .expect(204);
  });

  test('registration/confirm', async () => {
    const user = await models.User.create({
      email: 'user@example.com'
    });

    await request(app)
      .post('/user/registration/confirm')
      .send({ token: user.token })
      .expect(200)
      .then(response => {
        expect(response.body.user.id).toEqual(user.id);
        expect(response.body.jwt).toBeTruthy();
      });
  });

  test('restore/confirm', async () => {
    const user = await models.User.create({
      email: 'user@example.com'
    });

    await request(app)
      .post('/user/restore/confirm')
      .send({
        token: user.token,
        password: '654321',
        password_confirm: '654321' })
      .expect(200)
      .then(response => {
        expect(response.body.user.id).toEqual(user.id);
        expect(response.body.jwt).toBeTruthy();
      });
  });

  test('change_password', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: await passwordUtil.hash('123456'),
      is_active: true
    });

    await request(app)
      .post('/user/profile/change_password')
      .send({
        password_current: '123456',
        password: '654321',
        password_confirm: '654321'
      })
      .set('Authorization', jwt.signUser(user))
      .expect(204);

    await request(app)
      .post('/user/profile/change_password')
      .send({
        password_current: '111',
        password: '654321',
        password_confirm: '654321'
      })
      .set('Authorization', jwt.signUser(user))
      .expect(400);

    await request(app)
      .post('/user/profile/change_password')
      .send({
        password_current: '654321',
        password: '555666',
        password_confirm: '55577'
      })
      .set('Authorization', jwt.signUser(user))
      .expect(400);
  });

  test('2fa: qr', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    await request(app)
      .get('/user/2fa')
      .set('Authorization', jwt.signUser(user))
      .expect(200)
      .then(response => {
        expect(response.body.qr).toBeTruthy();
      });
  });

  test('2fa: enable', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    await request(app)
      .post('/user/2fa')
      .send({ token: '123' })
      .set('Authorization', jwt.signUser(user))
      .expect(204);

    await user.reload();
    expect(user.is_two_factor).toBeTruthy();
  });

  test('2fa: disable', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      is_two_factor: true,
      is_active: true
    });
    await request(app)
      .delete('/user/2fa')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(204);

    await user.reload();
    expect(user.is_two_factor).toBeFalsy();
  });
});
