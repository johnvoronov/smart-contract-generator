const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const balanceService = require('../../service/balance');
const jwt = require('../../util/jwt');

describe('routes/bounty', function () {
  beforeEach(async () => {
    await models.sequelize.sync({ force: true });
  });

  test('accept', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    await models.Bounty.create({
      name: 'user.registration.mobile',
      amount: 1000,
      is_enabled: true
    });

    await request(app)
      .post('/bounty/accept')
      .send({
        name: 'user.registration.mobile'
      })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(200);

    expect(await balanceService.getBalance(user.id)).toEqual(1000);

    await request(app)
      .post('/bounty/accept')
      .send({
        name: 'user.registration.mobile'
      })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(400);

    expect(await balanceService.getBalance(user.id)).toEqual(1000);
  });

  test('list', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    await models.Bounty.create({
      name: 'user.registration.mobile',
      amount: 1000,
      is_enabled: true
    });

    await request(app)
      .get('/bounty')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(200)
      .then(resp => {
        expect(resp.body.length).toEqual(1);
      });
  });

  test('view', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true
    });

    const bounty = await models.Bounty.create({
      name: 'user.registration.mobile',
      amount: 1000,
      is_enabled: true
    });

    await request(app)
      .get(`/bounty/${bounty.id}`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(200)
      .then(resp => {
        expect(resp.body.name).toEqual(bounty.name);
      });
  });

  test('update', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
      is_admin: true
    });

    const bounty = await models.Bounty.create({
      name: 'user.registration.mobile',
      amount: 1000,
      is_enabled: true
    });

    await request(app)
      .post(`/bounty/${bounty.id}`)
      .send({ amount: 100 })
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(204);

    await bounty.reload();
    expect(parseFloat(bounty.amount)).toEqual(100);
  });

  test('enable / disable', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      is_active: true,
    });

    const bounty = await models.Bounty.create({
      name: 'user.registration.mobile',
      amount: 1000
    });
    expect(bounty.is_enabled).toEqual(false);

    await request(app)
      .post(`/bounty/enable/${bounty.id}`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(403);
    await request(app)
      .post(`/bounty/disable/${bounty.id}`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(403);

    await user.update({
      is_admin: true
    });

    await request(app)
      .post(`/bounty/enable/${bounty.id}`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(204);

    await bounty.reload();
    expect(bounty.is_enabled).toEqual(true);

    await request(app)
      .post(`/bounty/disable/${bounty.id}`)
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect(204);

    await bounty.reload();
    expect(bounty.is_enabled).toEqual(false);
  });
});
