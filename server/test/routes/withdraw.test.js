const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const withdrawService = require('../../service/withdraw');
const jwt = require('../../util/jwt');
const {
  TRANSACTION_PAYMENT,
  TRANSACTION_RETURN,
  TRANSACTION_WITHDRAW
} = require('../../consts');

describe('routes/withdraw', () => {
  beforeEach(async () => {
    withdrawService.isAvailable = jest.fn();
    await models.sequelize.sync({ force: true });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  test('available / unavailable', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      eth_address: '123456',
      is_active: true
    });

    withdrawService.isAvailable.mockReturnValue(Promise.resolve(false));
    await request(app)
      .get('/withdraw/available')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.is_available).toEqual(false);
      });

    withdrawService.isAvailable.mockReturnValue(Promise.resolve(true));
    await request(app)
      .get('/withdraw/available')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.is_available).toEqual(true);
      });
  });

  test('eth_address in user account', async () => {
    await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      is_active: true
    });

    await request(app)
      .post('/withdraw')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.sign({ id: 1 }) })
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(Object.keys(response.body.errors)).toEqual(['eth_address']);
        expect(response.body.errors.eth_address.pop()).toEqual('Incorrect ethereum wallet address');
      });
  });

  test('pending withdraw transaction', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 1,
      to_eth: 1,
      is_enabled: true
    });
    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });
    const payment = await models.Payment.create({
      wallet_id: wallet.id,
      currency_id: currency.id,
      amount: 10,
      to_usd: 1,
      to_eth: 1,
      user_id: user.id,
      transaction_id: '0x123',
      token_price: 1,
      tokens_amount: 10,
      fee: 0
    });
    await models.Transaction.create({
      amount: 10,
      status: TRANSACTION_PAYMENT,
      payment_id: payment.id,
      user_id: user.id
    });

    const headers = {
      'Accept': 'application/json',
      'Authorization': jwt.signUser(user)
    };

    // Проверяем текущий баланс пользователя, должен быть равен 10
    await request(app)
      .get('/balance')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.balance).toEqual(10);
      });

    // Создаем запрос на вывод средств, id транзакции 2
    await request(app)
      .post('/withdraw')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(201)
      .then(response => {
        expect(response.body.id).toEqual(2);
      });

    // Проверяем баланс пользователя, должен быть 0
    await request(app)
      .get('/balance')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.balance).toEqual(0);
      });

    // Запрашиваем вывод средств повторно, должны получить ошибку
    // недостаточно средств вне зависимости от того, замайнилась ли транзакция
    await request(app)
      .post('/withdraw')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(Object.keys(response.body.errors)).toEqual(['eth_address']);
        expect(response.body.errors.eth_address.pop()).toEqual('Insufficient funds');
      });

    await models.Transaction.create({
      amount: 10,
      payment_id: payment.id,
      user_id: user.id,
      status: TRANSACTION_RETURN,
      comment: 'Restore balance after broken eth transaction'
    });

    await request(app)
      .post('/withdraw')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(201)
      .then(response => {
        expect(response.body.id).toEqual(4);
      });
  });

  test('insufficient funds', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });

    await request(app)
      .post('/withdraw')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(Object.keys(response.body.errors)).toEqual(['eth_address']);
        expect(response.body.errors.eth_address.pop()).toEqual('Insufficient funds');
      });
  });

  test('insufficient funds with minimal_withdraw', async () => {
    const user = await models.User.create({
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });
    const currency = await models.Currency.create({
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 1,
      to_eth: 1,
      is_enabled: true
    });
    const wallet = await models.Wallet.create({
      user_id: user.id,
      currency_id: currency.id,
      address: 'test'
    });
    const payment = await models.Payment.create({
      wallet_id: wallet.id,
      currency_id: currency.id,
      amount: 10,
      to_usd: 1,
      to_eth: 1,
      user_id: user.id,
      transaction_id: '0x123',
      token_price: 1,
      tokens_amount: 10,
      fee: 0
    });
    await models.Transaction.create({
      amount: 10,
      payment_id: payment.id,
      user_id: user.id,
      status: TRANSACTION_PAYMENT
    });

    await request(app)
      .post('/withdraw')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(201);

    await models.Transaction.create({
      amount: 5,
      payment_id: payment.id,
      user_id: user.id,
      status: TRANSACTION_PAYMENT
    });

    expect(await models.Transaction.sum('amount')).toEqual(5);

    await models.Setting.create({
      path: 'minimal_withdraw',
      value: '50'
    });

    await request(app)
      .post('/withdraw')
      .set({ 'Accept': 'application/json', 'Authorization': jwt.signUser(user) })
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({
          error: 'Bad Request',
          errors: {
            eth_address: [
              'Insufficient funds'
            ]
          },
          statusCode: 400
        });
      });
  });
});
