const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');

describe('routes/transaction', () => {
  beforeEach(() => {
    return models.sequelize.sync({
      force: true
    });
  });

  afterAll(() => models.sequelize.close());

  it('list', async () => {
    await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });

    const headers = { 'Accept': 'application/json', 'Authorization': jwt.sign({ id: 1 }) };

    await request(app)
      .get('/transaction/list')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body).toEqual(
          expect.objectContaining({
            objects: expect.any(Array),
            meta: expect.any(Object)
          })
        );
      });
  });
});
