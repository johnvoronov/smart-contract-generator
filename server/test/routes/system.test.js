const request = require('supertest');
const app = require('../../../app');
const i18n = require('../../service/i18n');
const template = require('../../service/template');

describe('routes/system', () => {
  test('async-error', async () => {
    await request(app)
      .get('/system/async-error')
      .then(resp => {
        expect(resp.body).toEqual({
          error: 'Error: test',
          message: 'An internal server error occurred',
          statusCode: 500
        });
      });
  });

  test('locale default', async () => {
    await request(app)
      .get('/system/locale')
      .expect(200)
      .then(resp => {
        expect(resp.body).toEqual({
          locale: 'en',
          text: 'You\'ve been successfully signed in'
        });
      });
  });

  (i18n.getLocales().length > 1 ? test : test.skip)('locale ru', async () => { // https://blockchaincorp.atlassian.net/browse/ICC-129
    await request(app)
      .get('/system/locale')
      .set('Cookie', ['lang=ru'])
      .expect(200)
      .then(resp => {
        expect(resp.body).toEqual({
          locale: 'ru',
          text: 'В ваш аккаунт выполнен вход'
        });
      });
  });

  (i18n.getLocales().length > 1 ? test : test.skip)('template locale', () => { // https://blockchaincorp.atlassian.net/browse/ICC-129
    i18n.setLocale('en');
    expect(template.renderString('{{ locale() }}')).toEqual('en');
    i18n.setLocale('ru');
    expect(template.renderString('{{ locale() }}')).toEqual('ru');

    i18n.setLocale('en');
    expect(template.renderString('{{ "mail.login.subject"|trans }}')).toEqual('You\'ve been successfully signed in');
    i18n.setLocale('ru');
    expect(template.renderString('{{ "mail.login.subject"|trans }}')).toEqual('В ваш аккаунт выполнен вход');
  });

  (i18n.getLocales().length > 1 ? test : test.skip)('validation middleware locale', async () => { // https://blockchaincorp.atlassian.net/browse/ICC-129
    await request(app)
      .get('/system/joi-locale')
      .set('Cookie', ['lang=ru'])
      .expect('Content-Type', /json/)
      .expect(400)
      .then(response => {
        expect(response.body).toEqual({
          error: 'Bad Request',
          errors: {
            name: ['name является обязательным']
          },
          statusCode: 400
        });
      });
  });
});
