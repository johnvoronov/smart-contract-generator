const request = require('supertest');
const app = require('../../../app');
const models = require('../../models');
const jwt = require('../../util/jwt');
const {
  TRANSACTION_PAYMENT,
  PAYMENT_COMPLETE
} = require('../../consts');

describe('routes/balance', () => {
  beforeEach(() => {
    return models.sequelize.sync({
      force: true
    });
  });

  afterAll(() => models.sequelize.close());

  it('balance', async () => {
    await models.User.create({
      id: 1,
      email: 'user@example.com',
      password: '',
      eth_address: '123456',
      is_active: true
    });
    await models.Currency.create({
      id: 1,
      code: 'ETH',
      name: 'Ethereum',
      to_usd: 1,
      to_eth: 1,
      is_enabled: true
    });
    await models.Wallet.create({
      id: 1,
      user_id: 1,
      currency_id: 1,
      address: 'test'
    });
    await models.Payment.create({
      id: 1,
      wallet_id: 1,
      currency_id: 1,
      amount: 10,
      to_usd: 1,
      status: PAYMENT_COMPLETE,
      to_eth: 1,
      user_id: 1,
      transaction_id: '0x123',
      token_price: 1,
      tokens_amount: 10,
      fee: 0
    });

    const headers = { 'Accept': 'application/json', 'Authorization': jwt.sign({ id: 1 }) };

    await request(app)
      .get('/balance')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.balance).toEqual(0);
      });

    await models.Transaction.create({
      id: 1,
      amount: 10,
      payment_id: 1,
      user_id: 1,
      status: TRANSACTION_PAYMENT
    });

    await request(app)
      .get('/balance')
      .set(headers)
      .expect('Content-Type', /json/)
      .expect(200)
      .then(response => {
        expect(response.body.balance).toEqual(10);
      });
  });
});
