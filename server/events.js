module.exports = app => {
  /**
   * Событие после успешной регистрации
   */
  app.on('user.registration', async (req, res, user) => {});

  /**
   * Событие после успешной повторной отправки письма с кодом
   * регистрации
   */
  app.on('user.registration.resend', async (req, res, user) => {});

  /**
   * Событие восстановления пароля
   */
  app.on('user.restore', async (req, res, user) => {});

  /**
   * Событие после успешной повторной отправки письма с кодом
   * восстановления пароля
   */
  app.on('user.restore.resend', async (req, res, user) => {});

  /**
   * После подтверждения восстановления пароля и после
   * установки нового пароля
   */
  app.on('user.restore.confirm', async (req, res, user) => {});

  /**
   * Событие после отключения 2fa авторизации
   */
  app.on('user.two_factor.disable', async (req, res, user) => {});

  /**
   * Событие после включения 2fa авторизации
   */
  app.on('user.two_factor.enable', async (req, res, user) => {});

  /**
   * Событие после успешной авторизации
   */
  app.on('user.login', async (req, res, user) => {});
};
