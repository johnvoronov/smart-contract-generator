module.exports = {
  'openapi': '3.0.1',
  'info': {
    'version': '1.0.0',
    'title': 'ic'
  },
  'host': 'localhost:8000',
  'basePath': '/',
  'servers': [
    {
      'url': 'https://google.com',
      'description': 'production'
    },
    {
      'url': 'http://localhost:8000',
      'description': 'localhost'
    }
  ],
  'externalDocs': {
    'url': 'https://polyworld.docs.apiary.io/',
    'description': 'Дополнительная документация по игровому API'
  },
  'schemes': [
    'http',
    'https'
  ],
  'consumes': [
    'application/json'
  ],
  'produces': [
    'application/json'
  ],
  'components': {
    'securitySchemes': {
      'BearerAuth': {
        'type': 'http',
        'scheme': 'bearer'
      },
      'ApiKeyAuth': {
        'type': 'apiKey',
        'in': 'header',
        'name': 'Authorization'
      }
    },
    'responses': {
      'Meta': require('./components/responses/meta'),
      'Jwt': require('./components/responses/jwt'),
      'NotFound': require('./components/responses/not_found'),
      'NoContent': require('./components/responses/no_content'),
      'Created': require('./components/responses/created'),
      'ValidationErrors': require('./components/responses/validation_errors'),
      'Faqs': require('./components/responses/faqs'),
      'FaqsPager': require('./components/responses/faqs_pager'),
      'TransactionsPager': require('./components/responses/transactions_pager'),
      'Transactions': require('./components/responses/transactions'),
      'PaymentsPager': require('./components/responses/payments_pager'),
      'Payments': require('./components/responses/payments'),
      'DiscountsPager': require('./components/responses/discounts_pager'),
      'Discounts': require('./components/responses/discounts'),
      'CurrenciesPager': require('./components/responses/currencies_pager'),
      'Currencies': require('./components/responses/currencies'),
      'UsersPager': require('./components/responses/users_pager'),
      'Users': require('./components/responses/users'),
      'Bounties': require('./components/responses/bounties')
    },
    'schemas': {
      'Faq': require('./components/schemas/faq'),
      'Discount': require('./components/schemas/discount'),
      'Referral': require('./components/schemas/referral'),
      'Currency': require('./components/schemas/currency'),
      'Transaction': require('./components/schemas/transaction'),
      'Payment': require('./components/schemas/payment'),
      'Wallet': require('./components/schemas/wallet'),
      'Settings': require('./components/schemas/settings'),
      'User': require('./components/schemas/user'),
      'Bounty': require('./components/schemas/bounty')
    }
  },
  'paths': {
    ...require('./routes/user'),
    ...require('./routes/bounty'),
    ...require('./routes/settings'),
    ...require('./routes/payment'),
    ...require('./routes/balance'),
    ...require('./routes/currency'),
    ...require('./routes/discount'),
    ...require('./routes/transaction'),
    ...require('./routes/referral'),
    ...require('./routes/wallet'),
    ...require('./routes/withdraw'),
    ...require('./routes/faq')
    // ...require('./routes/admin/currency'),
    // ...require('./routes/admin/payment'),
    // ...require('./routes/admin/user'),
    // ...require('./routes/admin/transaction'),
    // ...require('./routes/admin/discount'),
    // ...require('./routes/admin/referral_withdraw'),
    // ...require('./routes/admin/settings')
  }
};
