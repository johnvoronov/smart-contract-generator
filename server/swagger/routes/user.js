module.exports = {
  '/user/login': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Авторизация',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'format': 'email',
          'description': 'Электронная почта',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Пароль',
          'required': true
        },
        {
          'name': 'token',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': false,
          'description': 'Код входа 2fa. Требуется передавать только в случае получения 406 статуса.'
        }
      ],
      'responses': {
        '200': {
          '$ref': '#/components/responses/Jwt'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        },
        '406': {
          'description': 'Требуется 2fa код'
        }
      }
    }
  },
  '/user/registration': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Регистрация в системе',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'format': 'email',
          'description': 'Эл. почта',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Пароль',
          'required': true
        },
        {
          'name': 'password_confirm',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Повтор пароля',
          'required': true
        },
        {
          'name': 'referral_id',
          'in': 'body',
          'schema': {
            'type': 'integer'
          },
          'description': 'ID пригласившего пользователя',
          'required': false
        },
        {
          'name': 'eth_address',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Адрес кошелька Ethereum',
          'required': false
        },
        {
          'name': 'is_agree',
          'in': 'body',
          'schema': {
            'type': 'boolean'
          },
          'description': 'Согласен с правилами',
          'required': true
        },
        {
          'name': 'is_confirm',
          'in': 'body',
          'schema': {
            'type': 'boolean'
          },
          'description': 'Соглашаюсь с политикой конфиденциальности',
          'required': true
        }
      ],
      'responses': {
        '201': {
          '$ref': '#/components/responses/Created'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/registration/confirm': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Активация учетной записи',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'description': 'Код подтверждения с эл. почты',
          'schema': {
            'type': 'string'
          },
          'required': true
        }
      ],
      'responses': {
        '200': {
          'description': 'Успешная авторизация. В ответе от сервера помимо JWT возвращаются public поля модели User. В момент получения токена происходит дополнительная проверка.',
          'content': {
            'application/json': {
              'schema': {
                'properties': {
                  'user': {
                    '$ref': '#/components/schemas/User'
                  },
                  'jwt': {
                    'type': 'string'
                  },
                  'bounty': {
                    'type': 'array',
                    'items': {
                      'type': 'object',
                      'properties': {
                        'name': {
                          'type': 'string'
                        },
                        'amount': {
                          'type': 'integer',
                          'format': 'double'
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/registration/resend': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Повторная отправка письма после регистрации пользователя',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'format': 'email',
          'description': 'Эл. почта',
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/restore': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Восстановление пароля пользователя',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'format': 'email',
          'schema': {
            'type': 'string'
          },
          'description': 'Эл. почта',
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/restore/confirm': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Подтверждение электронной почты и изменение пароля',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'format': 'string',
          'description': 'Код подтверждения с эл. почты',
          'required': true
        },
        {
          'name': 'password',
          'in': 'body',
          'format': 'string',
          'description': 'Пароль',
          'required': true
        },
        {
          'name': 'password_confirm',
          'in': 'body',
          'format': 'string',
          'description': 'Подтверждение пароля',
          'required': true
        }
      ],
      'responses': {
        '200': {
          '$ref': '#/components/responses/Jwt'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/restore/resend': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Повторная отправка письма после восстановления пароля',
      'parameters': [
        {
          'name': 'email',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'format': 'email',
          'description': 'Эл. почта',
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/restore/check': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Проверка токена полученного пользователем по эл. почте перед установкой пароля. Пользователь заказывает восстановление пароля -> Вводит токен с эл. почты -> Проверяем токен на корректность -> Если верный, позволяем задать новый пароль с тем же токеном.',
      'parameters': [
        {
          'name': 'token',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Код подтверждения',
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/refresh_token': {
    'post': {
      'tags': [
        'Users',
        'Authentication'
      ],
      'description': 'Получение нового JWT токена',
      'parameters': [
        {
          'name': 'refresh_token',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Токен обновления',
          'required': true
        }
      ],
      'responses': {
        '200': {
          '$ref': '#/components/responses/Jwt'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/eth_address': {
    'post': {
      'tags': [
        'Users',
        'Profile'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Сохранение адреса Ethereum кошелька пользователя',
      'parameters': [
        {
          'name': 'eth_address',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Адрес кошелька Ethereum',
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/change_password': {
    'post': {
      'tags': [
        'Users',
        'Profile'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Изменение пароля',
      'parameters': [
        {
          'name': 'password_current',
          'description': 'Текущий пароль',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true
        },
        {
          'name': 'password',
          'description': 'Новый пароль',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true
        },
        {
          'name': 'password_confirm',
          'description': 'Подтверждение пароля',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/user/2fa': {
    'get': {
      'tags': [
        'Users',
        'Two factor',
        'Profile'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Отображение изображения с QR кодом 2fa авторизации',
      'responses': {
        '200': {
          'description': 'Успешно',
          'schema': {
            'type': 'object',
            'properties': {
              'image': {
                'type': 'string'
              }
            }
          }
        }
      }
    },
    'post': {
      'tags': [
        'Users',
        'Two factor',
        'Profile'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Включение 2fa авторизации для текущего пользователя',
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        }
      }
    },
    'delete': {
      'tags': [
        'Users',
        'Two factor',
        'Profile'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Отключение 2fa авторизации для текущего пользователя',
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        }
      }
    }
  }
};
