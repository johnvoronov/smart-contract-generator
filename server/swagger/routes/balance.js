module.exports = {
  '/balance': {
    'get': {
      'tags': [
        'Balance'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение баланса текущего пользователя',
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'balance': {
                    'type': 'number',
                    'format': 'double',
                    'example': 12.5813
                  },
                  'bonus': {
                    'type': 'number',
                    'format': 'double',
                    'example': 12.5813
                  },
                  'withdraw': {
                    'type': 'number',
                    'format': 'double',
                    'example': 12.5813
                  },
                  'bounty': {
                    'type': 'number',
                    'format': 'double',
                    'example': 12.5813
                  },
                  'referral': {
                    'type': 'number',
                    'format': 'double',
                    'example': 12.5813
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
