module.exports = {
  '/settings': {
    'get': {
      'tags': [
        'Settings'
      ],
      'description': 'Вывод настроек',
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'responses': {
        '200': {
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/schemas/Settings'
              }
            }
          }
        }
      }
    }
  }
};
