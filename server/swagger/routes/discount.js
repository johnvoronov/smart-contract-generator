module.exports = {
  '/discount': {
    'get': {
      'tags': [
        'Discount'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение текущей скидки (бонуса)',
      'responses': {
        '200': {
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'discount': {
                    'type': 'number',
                    'format': 'integer',
                    'example': 7
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
