module.exports = {
  '/currency': {
    'get': {
      'tags': [
        'Currency'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'parameters': [
        {
          'name': 'is_enabled',
          'in': 'query',
          'schema': {
            'type': 'bool'
          },
          'required': false
        }
      ],
      'description': 'Получение списка включенных крипто-валют',
      'responses': {
        '200': {
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/responses/Currencies'
              }
            }
          }
        }
      }
    }
  }
};
