module.exports = {
  '/payment/list': {
    'get': {
      'tags': [
        'Payment'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'parameters': require('../parameters/pagination'),
      'description': 'Получение списка платежей текущего пользователя',
      'responses': {
        '200': {
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/responses/PaymentsPager'
              }
            }
          }
        }
      }
    }
  },
  '/payment/withdraw': {
    'post': {
      'tags': [
        'Payment',
        'Withdraw'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Вывод средств',
      'responses': {
        '201': {
          '$ref': '#/components/responses/Created'
        }
      }
    }
  }
};
