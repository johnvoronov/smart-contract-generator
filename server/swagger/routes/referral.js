module.exports = {
  '/referral/status': {
    'get': {
      'tags': [
        'Referral'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение операций по балансу текущего пользователя',
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                'properties': {
                  'raised': {
                    'type': 'object',
                    'properties': {
                      'user': {
                        'type': 'integer',
                        'format': 'double'
                      },
                      'team': {
                        'type': 'integer',
                        'format': 'double'
                      }
                    }
                  },
                  'current': {
                    'type': 'object',
                    'properties': {
                      'name': {
                        'type': 'string'
                      },
                      'levels': {
                        'type': 'array',
                        'items': {
                          'type': 'integer'
                        }
                      },
                      'user': {
                        'type': 'integer',
                        'format': 'double'
                      },
                      'team': {
                        'type': 'integer',
                        'format': 'double'
                      }
                    }
                  },
                  'next': {
                    'type': 'object',
                    'properties': {
                      'name': {
                        'type': 'string'
                      },
                      'levels': {
                        'type': 'array',
                        'items': {
                          'type': 'integer'
                        }
                      },
                      'user': {
                        'type': 'integer',
                        'format': 'double'
                      },
                      'team': {
                        'type': 'integer',
                        'format': 'double'
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  },
  '/referral/users': {
    'get': {
      'tags': [
        'Referral'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'parameters': [
        {
          'name': 'path',
          'in': 'query',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID родительского пользователя'
        }
      ],
      'description': 'Получение пользователей по уровню в реферальной системе',
      'responses': {
        '200': {
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/responses/Users'
              }
            }
          }
        }
      }
    }
  }
};
