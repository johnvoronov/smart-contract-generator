module.exports = {
  '/bounty': {
    'get': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение информации по вознаграждению и условиям баунти системы',
      'responses': {
        '200': {
          'description': 'Успешно',
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/responses/Bounties'
              }
            }
          }
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/bounty/accept': {
    'post': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Применение вознаграждения',
      'parameters': [
        {
          'name': 'name',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'description': 'Идентификатор события',
          'required': true
        }
      ],
      'responses': {
        '204': {
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'amount': {
                    'type': 'integer',
                    'format': 'double'
                  }
                }
              }
            }
          }
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        }
      }
    }
  },
  '/bounty/{id}': {
    'get': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение информации по конкретному вознаграждению и условиям',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'string'
          },
          'required': true,
          'example': 1,
          'description': 'ID объекта'
        }
      ],
      'responses': {
        '200': {
          'description': 'Успешно',
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/schemas/Bounty'
              }
            }
          }
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    },
    'post': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Изменение события',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID объекта'
        },
        {
          'name': 'amount',
          'in': 'body',
          'schema': {
            'type': 'integer',
            'format': 'double'
          },
          'required': true,
          'example': 1,
          'description': 'Сумма вознаграждения'
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    }
  },
  '/bounty/enable/{id}': {
    'post': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Включение события',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID объекта'
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    }
  },
  '/bounty/disable/{id}': {
    'post': {
      'tags': [
        'Bounty'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Выключение события',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID объекта'
        }
      ],
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    }
  }
};
