module.exports = {
  '/faq': {
    'post': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'question',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true,
          'description': 'Вопрос'
        },
        {
          'name': 'answer',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true,
          'description': 'Ответ'
        }
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Новый вопрос / ответ',
      'responses': {
        '201': {
          '$ref': '#/components/responses/Created'
        }
      }
    },
    'get': {
      'tags': [
        'Faq'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Получение списка вопросов и ответов',
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/responses/FaqsPager'
          }
        }
      }
    }
  },
  '/faq/{id}': {
    'post': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID статьи'
        },
        {
          'name': 'question',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true,
          'description': 'Вопрос'
        },
        {
          'name': 'answer',
          'in': 'body',
          'schema': {
            'type': 'string'
          },
          'required': true,
          'description': 'Ответ'
        },
        {
          'name': 'position',
          'in': 'body',
          'schema': {
            'type': 'integer'
          },
          'required': false,
          'example': 1,
          'description': 'Порядок вывода'
        }
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        }
      }
    },
    'delete': {
      'tags': [
        'Faq'
      ],
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'schema': {
            'type': 'integer'
          },
          'required': true,
          'example': 1,
          'description': 'ID'
        }
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          '$ref': '#/components/responses/NoContent'
        }
      }
    }
  }
};
