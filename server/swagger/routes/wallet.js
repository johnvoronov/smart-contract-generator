module.exports = {
  '/wallet': {
    'post': {
      'tags': [
        'Wallet'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Создание кошелька для крипто-валюты',
      'parameters': [
        {
          'name': 'currency_id',
          'in': 'body',
          'schema': {
            'type': 'integer'
          },
          'required': true
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'address': {
                    'type': 'string',
                    'example': '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d'
                  }
                }
              }
            }
          }
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    }
  },
  '/wallet/eth': {
    'post': {
      'tags': [
        'Wallet'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Создание кошелька для Ethereum крипто-валюты',
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'address': {
                    'type': 'string',
                    'example': '0xFB897C789DcD53030729FcE3E6fc71EAE8A8183d'
                  }
                }
              }
            }
          }
        },
        '400': {
          '$ref': '#/components/responses/ValidationErrors'
        },
        '404': {
          '$ref': '#/components/responses/NotFound'
        }
      }
    }
  }
};
