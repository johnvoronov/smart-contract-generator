module.exports = {
  '/withdraw': {
    'post': {
      'tags': [
        'Withdraw'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Withdraw',
      'responses': {
        '201': {
          '$ref': '#/components/responses/Created'
        }
      }
    }
  },
  '/withdraw/available': {
    'get': {
      'tags': [
        'Withdraw'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'description': 'Withdraw',
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                'type': 'object',
                'properties': {
                  'is_available': {
                    'type': 'boolean'
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
