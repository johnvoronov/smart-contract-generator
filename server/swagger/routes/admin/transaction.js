module.exports = {
  '/admin/transaction/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод операций по балансу',
      'parameters': [
        ...require('../../parameters/pagination'),
        {
          'name': 'q',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': 'hello',
          'description': 'Поиск'
        },
        {
          'name': 'order',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': '-created_at',
          'description': 'Сортировка по полям. Разрешенные значения: comment, -comment, amount, -amount, id, -id, created_at, -created_at, user_id, -user_id'
        },
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/TransactionsPager'
          }
        }
      }
    }
  },
  '/admin/transaction/{id}': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод операций по балансу',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID операции'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/Transaction'
          }
        }
      }
    }
  }
};
