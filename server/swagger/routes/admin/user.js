module.exports = {
  '/admin/user/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод списка пользователей',
      'parameters': [
        ...require('../../parameters/pagination'),
        {
          'name': 'q',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': 'hello',
          'description': 'Поиск'
        },
        {
          'name': 'order',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': '-created_at',
          'description': 'Сортировка по полям. Разрешенные значения: email, -email, is_active, -is_active, is_admin, -is_admin, id, -id, created_at, -created_at'
        },
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/UsersPager'
          }
        }
      }
    }
  },
  '/admin/user/{id}': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод информации о пользователе',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID пользователя'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/User'
          }
        }
      }
    }
  },
  '/admin/user/admin/{id}': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод информации о пользователе',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID пользователя'
        },
        {
          'name': 'id',
          'in': 'body',
          'type': 'boolean',
          'required': true,
          'example': true,
          'description': 'Включение / выключение флага "Администратор"'
        }
      ],
      'responses': {
        '204': {
          'description': 'Success',
        }
      }
    }
  }
};
