module.exports = {
  '/admin/referral_withdraw/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод операций по балансу',
      'parameters': require('../../parameters/pagination'),
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/ReferralWithdrawsPager'
          }
        }
      }
    }
  },
  '/admin/referral_withdraw/done/{id}': {
    'post': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Запросы на вывод баланса зачисленного по реферальной системе',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID запроса на вывод'
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
