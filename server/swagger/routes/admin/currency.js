module.exports = {
  '/admin/currency/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод списка крипто-валют',
      'parameters': require('../../parameters/pagination'),
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/CurrenciesPager'
          }
        }
      }
    }
  },
  '/admin/currency/exchange': {
    'post': {
      'tags': [
        'Admin'
      ],
      'description': 'Обновление курсов крипто-валют',
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/currency/toggle/{id}': {
    'post': {
      'tags': [
        'Admin'
      ],
      'description': 'Включение / выключение крипто-валюты',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID крипто-валюты'
        }
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
