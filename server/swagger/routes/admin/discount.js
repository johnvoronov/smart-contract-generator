module.exports = {
  '/admin/discount/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод интервалов бонусной системы',
      'parameters': require('../../parameters/pagination'),
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/DiscountsPager'
          }
        }
      }
    }
  },
  '/admin/discount': {
    'post': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение с перезаписью интервалов бонусной системы. Даты передаются в ISO 8601 ((new Date("10-10-2010")).toISOString())',
      'parameters': [
        {
          'name': 'intervals',
          'in': 'body',
          'schema': {
            'type': 'array',
            'items': {
              'type': 'object',
              'properties': {
                'value': {
                  'type': 'number',
                  'format': 'double'
                },
                'is_fixed': {
                  'type': 'boolean'
                },
                'from': {
                  'type': 'string',
                  'format': 'date-time'
                },
                'to': {
                  'type': 'string',
                  'format': 'date-time'
                }
              }
            }
          },
          'required': true,
          'example': {
            intervals: [
              {
                from: (new Date('10-10-2010')).toISOString(),
                to: (new Date('10-30-2010')).toISOString(),
                is_fixed: true,
                value: 10
              },
              {
                from: (new Date('1-11-2010')).toISOString(),
                to: (new Date('5-11-2010')).toISOString(),
                is_fixed: false,
                value: 10
              }
            ]
          }
        }
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
