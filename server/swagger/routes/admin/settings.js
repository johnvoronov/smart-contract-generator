module.exports = {
  '/admin/settings': {
    'get': {
      'tags': ['Admin'],
      'description': 'Вывод настроек (public & private)',
      'security': {
        'ApiKeyAuth': []
      },
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/Settings'
          }
        }
      }
    },
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек',
      'parameters': [
        ...require('../../components/schemas/settings/coinpayments'),
        ...require('../../components/schemas/settings/company'),
        ...require('../../components/schemas/settings/google'),
        ...require('../../components/schemas/settings/recaptcha'),
        ...require('../../components/schemas/settings/smtp'),
        ...require('../../components/schemas/settings/social'),
        ...require('../../components/schemas/settings/link'),
        ...require('../../components/schemas/settings/token')
      ],
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/token': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел token)',
      'parameters': require('../../parameters/settings/token'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/coinpayments': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел coinpayments)',
      'parameters': require('../../parameters/settings/coinpayments'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/recaptcha': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел recaptcha)',
      'parameters': require('../../parameters/settings/recaptcha'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/social': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел social auth)',
      'parameters': require('../../parameters/settings/social'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/smtp': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел smtp)',
      'parameters': require('../../parameters/settings/smtp'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/company': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел company)',
      'parameters': require('../../parameters/settings/company'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  },
  '/admin/settings/google': {
    'post': {
      'tags': ['Admin'],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Сохранение настроек (раздел google)',
      'parameters': require('../../parameters/settings/google'),
      'responses': {
        '204': {
          'description': 'Success'
        }
      }
    }
  }
};
