module.exports = {
  '/admin/payment/list': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод списка платежей',
      'parameters': [
        ...require('../../parameters/pagination'),
        {
          'name': 'q',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': 'hello',
          'description': 'Поиск'
        },
        {
          'name': 'order',
          'in': 'query',
          'type': 'string',
          'required': false,
          'example': '-created_at',
          'description': 'Сортировка по полям. Разрешенные значения: amount, -amount, tokens_amount, -tokens_amount, currency_id, -currency_id, user_id, -user_id, created_at, -created_at'
        },
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/PaymentsPager'
          }
        }
      }
    }
  },
  '/admin/payment/{id}': {
    'get': {
      'tags': [
        'Admin'
      ],
      'security': {
        'ApiKeyAuth': []
      },
      'description': 'Вывод информации о платеже',
      'parameters': [
        {
          'name': 'id',
          'in': 'path',
          'type': 'string',
          'required': true,
          'example': 1,
          'description': 'ID платежа'
        }
      ],
      'responses': {
        '200': {
          'description': 'Success',
          'schema': {
            '$ref': '#/components/schemas/Payment'
          }
        }
      }
    }
  }
};
