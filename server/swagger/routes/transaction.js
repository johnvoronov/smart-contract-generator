module.exports = {
  '/transaction/list': {
    'get': {
      'tags': [
        'Transaction'
      ],
      'security': {
        'BearerAuth': [],
        'ApiKeyAuth': []
      },
      'parameters': require('../parameters/pagination'),
      'description': 'Получение операций по балансу текущего пользователя',
      'responses': {
        '200': {
          'description': 'Success',
          'content': {
            'application/json': {
              'schema': {
                '$ref': '#/components/responses/TransactionsPager'
              }
            }
          },
        }
      }
    }
  }
};
