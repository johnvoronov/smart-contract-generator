module.exports = [
  {
    'name': 'page',
    'in': 'query',
    'schema': {
      'type': 'integer',
      'format': 'integer',
    },
    'required': false,
    'default': 1,
    'example': 1,
    'description': 'Номер страницы. По умолчанию: 1'
  },
  {
    'name': 'page_size',
    'in': 'query',
    'schema': {
      'type': 'integer',
      'format': 'integer',
    },
    'required': false,
    'default': 10,
    'example': 10,
    'description': 'Количество записей на страницу. По умолчанию: 10'
  }
];
