module.exports = {
  'description': 'Выполнено',
  'content': {
    'application/json': {
      'schema': {
        'type': 'object',
        'properties': {
          'id': {
            'type': 'integer',
            'example': 42
          }
        }
      }
    }
  }
};
