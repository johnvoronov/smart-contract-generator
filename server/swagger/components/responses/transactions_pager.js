module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Transactions'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
