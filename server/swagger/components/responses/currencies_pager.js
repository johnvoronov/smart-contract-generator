module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Currencies'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
