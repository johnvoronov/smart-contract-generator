module.exports = {
  'description': 'Не найдено',
  'content': {
    'application/json': {
      'schema': {
        'type': 'object',
        'properties': {
          'statusCode': {
            'type': 'integer',
            'example': 404
          },
          'message': {
            'type': 'string',
            'example': 'Not found'
          }
        }
      }
    }
  }
};
