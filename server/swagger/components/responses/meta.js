module.exports = {
  'properties': {
    'page': {
      'type': 'integer',
      'uniqueItems': true
    },
    'page_size': {
      'type': 'integer',
      'uniqueItems': true
    },
    'page_count': {
      'type': 'integer',
      'uniqueItems': true
    },
    'total': {
      'type': 'integer',
      'uniqueItems': true
    }
  }
};
