module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Faqs'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
