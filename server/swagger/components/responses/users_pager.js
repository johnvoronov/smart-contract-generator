module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Users'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
