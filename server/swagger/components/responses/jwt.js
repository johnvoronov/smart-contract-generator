module.exports = {
  'description': 'Успешная авторизация. В ответе от сервера помимо JWT возвращаются public поля модели User. В момент получения токена происходит дополнительная проверка.',
  'content': {
    'application/json': {
      'schema': {
        'properties': {
          'user': {
            '$ref': '#/components/schemas/User'
          },
          'jwt': {
            'type': 'string'
          }
        }
      }
    }
  }
};
