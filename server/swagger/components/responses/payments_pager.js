module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Payments'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
