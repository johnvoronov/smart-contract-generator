module.exports = {
  'properties': {
    'objects': {
      '$ref': '#/components/responses/Discounts'
    },
    'meta': {
      'type': 'object',
      '$ref': '#/components/responses/Meta'
    }
  }
};
