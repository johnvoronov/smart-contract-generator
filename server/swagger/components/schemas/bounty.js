module.exports = {
  'properties': {
    'name': {
      'type': 'string',
      'uniqueItems': true,
      'description': 'Уникальныое имя в системе'
    },
    'amount': {
      'type': 'number',
      'format': 'double',
      'description': 'Сумма вознаграждения'
    },
    'is_enabled': {
      'type': 'boolean',
      'description': 'Состояние вкл. / выкл.'
    }
  }
};
