module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'name': {
      'type': 'string',
      'uniqueItems': true
    },
    'levels': {
      'type': 'array',
      'items': {
        'type': 'integer',
        'format': 'double'
      }
    },
    'user': {
      'type': 'integer',
      'format': 'double'
    },
    'team': {
      'type': 'integer',
      'format': 'double'
    }
  }
};
