module.exports = {
  'required': [
    'email',
    'id'
  ],
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'email': {
      'type': 'string',
      'uniqueItems': true
    },
    'eth_address': {
      'type': 'string'
    },
    'refresh_token': {
      'type': 'string'
    },
    'is_active': {
      'type': 'boolean'
    },
    'is_admin': {
      'type': 'boolean'
    },
    'is_superuser': {
      'type': 'boolean'
    },
    'is_two_factor': {
      'type': 'boolean'
    },
    'parents': {
      'type': 'array',
      'items': {
        'type': 'integer'
      }
    },
    'is_accepted_bonus': {
      'type': 'integer'
    },
    'referral_id': {
      'type': 'integer'
    },
    'owner_id': {
      'type': 'integer'
    }
  }
};
