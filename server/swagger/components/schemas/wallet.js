module.exports = {
  'required': [
    'address',
    'user_id',
    'currency_id',
    'id'
  ],
  'properties': {
    'id': {
      'type': 'string',
      'uniqueItems': true
    },
    'address': {
      'type': 'string'
    },
    'user_id': {
      'type': 'integer'
    },
    'currency_id': {
      'type': 'integer'
    }
  }
};
