module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'code': {
      'type': 'string',
      'uniqueItems': true
    },
    'name': {
      'type': 'string'
    },
    'to_eth': {
      'type': 'number',
      'format': 'double'
    },
    'to_usd': {
      'type': 'number',
      'format': 'double'
    },
    'is_enabled': {
      'type': 'boolean'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
