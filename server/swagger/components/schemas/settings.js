module.exports = {
  'properties': {
    'minimal_withdraw': {
      'type': 'number',
      'format': 'double'
    },
    'minimal_amount': {
      'type': 'number',
      'format': 'double'
    },
    'token_bonus': {
      'type': 'number',
      'format': 'double'
    },
    'token_hardcap': {
      'type': 'number',
      'format': 'double'
    },
    'token_name': {
      'type': 'string'
    },
    'token_price': {
      'type': 'number',
      'format': 'double'
    },
    'token_decimals': {
      'type': 'number',
      'format': 'double'
    },
    'social_twitter_is_enabled': {
      'type': 'string'
    },
    'social_twitter_client_id': {
      'type': 'string'
    },
    'social_facebook_is_enabled': {
      'type': 'string'
    },
    'social_facebook_client_id': {
      'type': 'string'
    },
    'social_google_is_enabled': {
      'type': 'string'
    },
    'social_google_client_id': {
      'type': 'string'
    },
    'bitcoinstalk_link': {
      'type': 'string'
    },
    'facebook_link': {
      'type': 'string'
    },
    'twitter_link': {
      'type': 'string'
    },
    'youtube_link': {
      'type': 'string'
    },
    'telegram_link': {
      'type': 'string'
    },
    'google_analytics_id': {
      'type': 'string'
    },
    'google_tag_manager_id': {
      'type': 'string'
    },
    'support_email': {
      'type': 'string'
    },
    'url_privacy_and_policy': {
      'type': 'string'
    },
    'url_site': {
      'type': 'string'
    },
    'url_whitepaper': {
      'type': 'string'
    }
  }
};
