module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'value': {
      'type': 'number',
      'format': 'double'
    },
    'is_fixed': {
      'type': 'boolean'
    },
    'from': {
      'type': 'string',
      'format': 'date-time'
    },
    'to': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
