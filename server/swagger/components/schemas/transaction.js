module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'amount': {
      'type': 'integer',
      'format': 'double'
    },
    'comment': {
      'type': 'string',
    },
    'is_referrer': {
      'type': 'boolean',
    },
    'is_bonus': {
      'type': 'boolean',
    },
    'is_withdraw': {
      'type': 'boolean',
    },
    'in_sync': {
      'type': 'boolean',
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
