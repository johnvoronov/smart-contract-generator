module.exports = {
  'properties': {
    'id': {
      'type': 'integer',
      'uniqueItems': true
    },
    'amount': {
      'type': 'integer'
    },
    'transaction_id': {
      'type': 'string'
    },
    'fee': {
      'type': 'number',
      'format': 'double'
    },
    'conversion': {
      'type': 'string'
    },
    'token_price': {
      'type': 'number',
      'format': 'double'
    },
    'discount': {
      'type': 'integer'
    },
    'is_complete': {
      'type': 'boolean'
    },
    'is_cancelled': {
      'type': 'boolean'
    },
    'is_pending': {
      'type': 'boolean'
    },
    'tokens_amount': {
      'type': 'number',
      'format': 'double'
    },
    'to_usd': {
      'type': 'number',
      'format': 'double'
    },
    'to_eth': {
      'type': 'number',
      'format': 'double'
    },
    'user_id': {
      'type': 'integer'
    },
    'user': {
      '$ref': '#/components/schemas/User'
    },
    'currency_id': {
      'type': 'integer'
    },
    'currency': {
      '$ref': '#/components/schemas/Currency'
    },
    'wallet_id': {
      'type': 'integer'
    },
    'wallet': {
      '$ref': '#/components/schemas/Wallet'
    },
    'created_at': {
      'type': 'string',
      'format': 'date-time'
    },
    'updated_at': {
      'type': 'string',
      'format': 'date-time'
    }
  }
};
