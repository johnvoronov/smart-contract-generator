module.exports = [
  {
    'name': 'coinpayments_public_key',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': true,
    'description': 'Публичный ключ'
  },
  {
    'name': 'coinpayments_secret_key',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': true,
    'description': 'Секретный ключ'
  },
  {
    'name': 'coinpayments_ipn_secret',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': true,
    'description': 'IPN ключ'
  },
  {
    'name': 'coinpayments_merchant_id',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': true,
    'description': 'ID продавца'
  }
];
