module.exports = [
  {
    'name': 'support_email',
    'in': 'body',
    'schema': {
      'type': 'string',
      'format': 'email'
    },
    'required': false,
    'description': 'Электронная почта технической поддержки'
  },
  {
    'name': 'url_privacy_and_policy',
    'in': 'body',
    'schema': {
      'type': 'string',
      'format': 'uri'
    },
    'required': false,
    'description': 'Ссылка на политику конфиденциальности'
  },
  {
    'name': 'url_site',
    'in': 'body',
    'schema': {
      'type': 'string',
      'format': 'uri'
    },
    'required': false,
    'description': 'Ссылка на основной сайт'
  },
  {
    'name': 'url_whitepaper',
    'in': 'body',
    'schema': {
      'type': 'string',
      'format': 'uri'
    },
    'required': false,
    'description': 'Ссылка на Whitepaper'
  },
  {
    'name': 'user_registration_invite_only',
    'in': 'body',
    'schema': {
      'type': 'integer'
    },
    'required': false,
    'description': 'Регистрация только по приглашению по реф. ссылке'
  }
];

