module.exports = [
  {
    'name': 'social_twitter_is_enabled',
    'in': 'body',
    'schema': {
      'type': 'boolean',
    },
    'description': 'Включить / выключить авторизацию через Twitter'
  },
  {
    'name': 'social_twitter_client_id',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Публичный ключ'
  },
  {
    'name': 'social_twitter_client_secret',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Секретный ключ'
  },
  {
    'name': 'social_facebook_is_enabled',
    'in': 'body',
    'schema': {
      'type': 'boolean',
    },
    'description': 'Включить / выключить авторизацию через Facebook'
  },
  {
    'name': 'social_facebook_client_id',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Публичный ключ'
  },
  {
    'name': 'social_facebook_client_secret',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Секретный ключ'
  },
  {
    'name': 'social_google_is_enabled',
    'in': 'body',
    'schema': {
      'type': 'boolean',
    },
    'description': 'Включить / выключить авторизацию через Facebook'
  },
  {
    'name': 'social_google_client_id',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Публичный ключ'
  },
  {
    'name': 'social_google_client_secret',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Секретный ключ'
  }
];
