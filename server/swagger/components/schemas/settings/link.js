module.exports = [
  {
    'name': 'bitcoinstalk_link',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'description': 'Ссылка на bitcoinstalk'
  },
  {
    'name': 'facebook_link',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'description': 'Ссылка на группу в Facebook'
  },
  {
    'name': 'twitter_link',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'description': 'Ссылка на Twitter'
  },
  {
    'name': 'youtube_link',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'description': 'Ссылка на канал в YouTube'
  },
  {
    'name': 'telegram_link',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'description': 'Ссылка на канал в Telegram'
  }
];
