let parameters = [
  {
    'name': 'minimal_withdraw',
    'in': 'body',
    'schema': {
      'type': 'integer',
      'format': 'double'
    },
    'required': false,
    'description': 'Минимальная сумма на вывод в токенах'
  },
  {
    'name': 'minimal_amount',
    'in': 'body',
    'schema': {
      'type': 'integer',
      'format': 'double'
    },
    'required': false,
    'description': 'Минимальная сумма покупки в токенах'
  },
  {
    'name': 'token_bonus',
    'in': 'body',
    'schema': {
      'type': 'integer',
      'format': 'integer'
    },
    'required': false,
    'example': 10,
    'description': 'Сумма бонуса в процентах к каждой покупке пользователя'
  }
];

if (!process.env.BLOCKCHAIN_INTEGRATION) {
  parameters = [
    ...parameters,
    {
      'name': 'token_hardcap',
      'in': 'body',
      'schema': {
        'type': 'integer',
        'format': 'double'
      },
      'required': false,
      'description': 'Сумма HARDCAP'
    },
    {
      'name': 'token_name',
      'in': 'body',
      'schema': {
        'type': 'string'
      },
      'required': false,
      'description': 'Название токена'
    },
    {
      'name': 'token_price',
      'in': 'body',
      'schema': {
        'type': 'integer',
        'format': 'integer'
      },
      'required': true,
      'default': 1,
      'example': 1,
      'description': 'Стоимость токена в USD или ETH'
    },
    {
      'name': 'token_decimals',
      'in': 'body',
      'schema': {
        'type': 'integer',
        'format': 'integer'
      },
      'required': true,
      'default': 18,
      'example': 18,
      'description': 'Разрядность токена (кол-во знаков после запятой)'
    }
  ];
}

module.exports = parameters;
