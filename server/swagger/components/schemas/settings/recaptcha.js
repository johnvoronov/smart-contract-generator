module.exports = [
  {
    'name': 'recaptcha_is_enabled',
    'in': 'body',
    'schema': {
      'type': 'boolean'
    },
    'required': false,
    'description': 'Включить / Выключить'
  },
  {
    'name': 'recaptcha_public_key',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': false,
    'description': 'Публичный ключ'
  },
  {
    'name': 'recaptcha_secret_key',
    'in': 'body',
    'schema': {
      'type': 'string'
    },
    'required': false,
    'description': 'Секретный ключ'
  }
];
