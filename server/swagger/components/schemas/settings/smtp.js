module.exports = [
  {
    'name': 'smtp_from',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Адрес электронной почты с которой будет происходить отправка'
  },
  {
    'name': 'smtp_host',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Хост'
  },
  {
    'name': 'smtp_port',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Порт'
  },
  {
    'name': 'smtp_username',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Имя пользователя или электронная почта. Для AWS используется access key'
  },
  {
    'name': 'smtp_password',
    'in': 'body',
    'schema': {
      'type': 'string',
    },
    'description': 'Пароль (или secret key для AWS)'
  },
];
