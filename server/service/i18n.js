const path = require('path');
const i18n = require('i18n');
const config = require('../config/app');

i18n.configure({
  defaultLocale: config.i18n.default_locale,
  locales: config.i18n.locales,
  cookie: 'lang',
  queryParameter: 'lang',
  directory: path.resolve(__dirname, '../locales'),
  api: {
    '__': 't',
    '__n': 'tn'
  }
});

module.exports = i18n;
