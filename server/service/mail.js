const mail = require('../util/mail');

module.exports = {
  async send(to, template, data = {}) {
    // const params = await settings.fetch(false);

    const socials = [
      // { name: 'bitcoinstalk', link: params.bitcoinstalk_link },
      // { name: 'telegram', link: params.telegram_link },
      // { name: 'youtube', link: params.youtube_link },
      // { name: 'facebook', link: params.facebook_link },
      // { name: 'twitter', link: params.twitter_link }
    ];

    return mail.send(to, template, { ...data, socials },);
  }
};
