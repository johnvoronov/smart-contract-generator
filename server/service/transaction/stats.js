const models = require('../../models');
const { transactionStatus } = require('../../consts');
const sequelize = models.sequelize;

module.exports = {
  async getStats() {
    const model = await models.Payment.findOne({
      attributes: [
        [sequelize.fn('SUM', sequelize.literal('tokens_amount * token_price')), 'raised']
      ]
    });

    return {
      token: await models.Transaction.sum('amount', {
        where: {
          status: transactionStatus.TRANSACTION_PAYMENT
        }
      }) || 0,
      bonus: await models.Transaction.sum('amount', {
        where: {
          status: transactionStatus.TRANSACTION_BONUS
        }
      }) || 0,
      raised: model.getDataValue('raised') || 0
    };
  }
};
