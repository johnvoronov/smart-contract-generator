const path = require('path');
const fs = require('fs');
const solc = require('solc');

module.exports = {
  /**
   * Компиляция контрактов
   *
   * @returns {*|Function}
   */
  compile(contractsDir, nodeModulesDir) {
    const MyCrowdsale = this.findContract(contractsDir, nodeModulesDir, 'MyCrowdsale.sol');
    const Token = this.findContract(contractsDir, nodeModulesDir, 'Token.sol');

    const sources = {
      'MyCrowdsale.sol': MyCrowdsale,
      'Token.sol': Token
    };

    const compiledCode = solc.compile({ sources }, 1, file => ({
      'contents': this.findContract(contractsDir, nodeModulesDir, file)
    }));

    const crowdsaleKey = 'MyCrowdsale.sol:MyCrowdsale';
    const tokenKey = 'Token.sol:Token';

    if (crowdsaleKey in compiledCode.contracts && tokenKey && compiledCode.contracts) {
      const myCrowdsaleContract = compiledCode.contracts['MyCrowdsale.sol:MyCrowdsale'];
      const tokenContract = compiledCode.contracts['Token.sol:Token'];

      return {
        token: {
          bytecode: tokenContract.bytecode,
          abi: tokenContract.interface
        },
        ico: {
          bytecode: myCrowdsaleContract.bytecode,
          abi: myCrowdsaleContract.interface
        }
      };
    } else {
      throw new Error(compiledCode.errors.join(' '));
    }
  },

  /**
   * Поиск контракта
   *
   * @param contractsDir - путь до смарт контракта
   * @param nodeModulesDir - путь до node_modules
   * @param file - файл контракта
   * @returns {*}
   */
  findContract(contractsDir, nodeModulesDir, file) {
    const resolvedPath = file.indexOf('openzeppelin-solidity') !== -1
      ? path.resolve(nodeModulesDir, file)
      : path.resolve(contractsDir, file);

    if (false === fs.existsSync(resolvedPath)) {
      throw new Error(`File ${file} not found`);
    }

    return fs.readFileSync(resolvedPath).toString();
  }
};
