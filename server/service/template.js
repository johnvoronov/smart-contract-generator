const path = require('path');
const nunjucks = require('nunjucks');
const i18n = require('./i18n');

const env = nunjucks.configure(path.resolve(__dirname, '../../templates'), {
  autoescape: false,
  watch: process.env.NODE_ENV !== 'production'
});

env.addFilter('trans', function () {
  return i18n.__.apply(i18n, arguments);
});
env.addGlobal('locale', () => i18n.getLocale());

module.exports = env;
