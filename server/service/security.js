const crypto = require('crypto');

const IV_LENGTH = 16; // For AES, this is always 16
const CIPHER = 'aes-256-cbc';

function validatePrivateKey(privateKey) {
  if (privateKey.length !== 32) {
    throw new Error('Invalid private key length, it should be 32');
  }
}

function encrypt(text, privateKey) {
  validatePrivateKey(privateKey);

  const iv = crypto.randomBytes(IV_LENGTH);
  const cipher = crypto.createCipheriv(CIPHER, new Buffer(privateKey), iv);

  const encrypted = Buffer.concat([
    cipher.update(String(text)),
    cipher.final()
  ]).toString('hex');

  return iv.toString('hex') + ':' + encrypted;
}

function decrypt(text, privateKey) {
  validatePrivateKey(privateKey);

  const textParts = text.split(':');
  const iv = new Buffer(textParts.shift(), 'hex');
  const encryptedText = new Buffer(textParts.join(':'), 'hex');
  const decipher = crypto.createDecipheriv(CIPHER, new Buffer(privateKey), iv);

  return Buffer.concat([
    decipher.update(encryptedText),
    decipher.final()
  ]).toString();
}

module.exports = {
  decrypt,
  encrypt
};
