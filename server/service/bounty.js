const models = require('../models');

module.exports = {
  fire: async (name) => {
    const program = models.Bounty.findOne({
      where: { name }
    });

    if (null === program) {
      return;
    }

    return program.bonus;
  }
};
