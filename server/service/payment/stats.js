const models = require('../../models');
const sequelize = models.sequelize;

module.exports = {
  async getStats() {
    const model = await models.Payment.findOne({
      attributes: [
        [sequelize.fn('SUM', sequelize.literal('tokens_amount * token_price')), 'raised']
      ]
    });

    return {
      token: await models.Payment.sum('tokens_amount') || 0,
      raised: model.getDataValue('raised') || 0
    };
  }
};
