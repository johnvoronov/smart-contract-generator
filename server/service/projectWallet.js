const models = require('../models');
const coinpayments = require('./systemCoinpayments');

module.exports = {
  createNewWallet(currency) {
    return coinpayments.client().getCallbackAddress({
      currency: currency.code
    });
  },

  async getWallet(project, currency) {
    const wallet = await models.ProjectWallet.findOne({
      where: {
        currency_id: currency.id,
        project_id: project.id
      }
    });

    if (wallet) {
      return wallet;
    }

    const coinpaymentsWallet = await this.createNewWallet(currency);

    return await models.ProjectWallet.create({
      project_id: project.id,
      currency_id: currency.id,
      address: coinpaymentsWallet.address
    });
  }
};
