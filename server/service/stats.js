const config = require('../config/blockchain');
const models = require('../models');
const Op = require('sequelize').Op;
const {
  TRANSACTION_PAYMENT,
  TRANSACTION_BONUS
} = require('../consts');

module.exports = {
  /**
   * Используется интеграция
   * со смарт-контрактом?
   *
   * @returns {bool}
   */
  isBlockchain() {
    return config.is_enabled;
  },

  /**
   * Общая сумма сборов в токенах
   *
   * @returns {Promise<float>}
   */
  async getTotalRaised(project) {
    const where = {
      project_id: project.id,
      status: {
        [Op.in]: [
          TRANSACTION_PAYMENT,
          TRANSACTION_BONUS
        ]
      }
    };

    if (project.blockchain.code !== 'local') {
      return models.Transaction.sum('amount', {
        where,
        include: [
          {
            model: models.Eth,
            as: 'eth',
            where: { status: 1 }
          }
        ],
        group: [
          'eth.id'
        ]
      }) || 0;
    }

    return models.Transaction.sum('amount', {
      where
    }) || 0;
  }
};
