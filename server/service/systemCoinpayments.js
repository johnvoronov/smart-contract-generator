const Coinpayments = require('coinpayments');
const appConfig = require('../config/systemCoinpayments');

module.exports = {
  client() {
    return new Coinpayments({
      key: appConfig.publicKey,
      secret: appConfig.secretKey
    });
  }
};
