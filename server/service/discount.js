const discount = require('../util/discount');

module.exports = {
  /**
   * Возвращаем актуальное кол-во бонусных процентов (скидки)
   * на текущий момент времени
   *
   * @returns {int|null}
   */
  getDiscount(project) {
    return discount.getDiscount(new Date, project.discount || []);
  },

  /**
   * Возвращаем следующую существующую дату интервала для
   * обратного счетчика на frontend
   *
   * @returns {Date|null}
   */
  getNextDate(project) {
    return discount.getNextDate(new Date, project.discount || []);
  }
};
