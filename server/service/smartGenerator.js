const generator = require('./generator');
const AdmZip = require('adm-zip');
const fs = require('fs');
const path = require('path');
const appConfig = require('../config/app');

module.exports = {
  getPath(project) {
    const contractPath = path.join(appConfig.contract_path, project.id.toString());

    if (false === fs.existsSync(contractPath)) {
      fs.mkdirSync(contractPath, { recursive: true, mode: 755 });
    }

    return contractPath;
  },

  createContract(project) {
    return generator.generate(this.getPath(project), project.parameters);
  },

  /**
   * Архивирование смарт контракта
   *
   * @param project
   * @returns {*}
   */
  zip(project) {
    const zip = new AdmZip();
    zip.addLocalFolder(this.getPath(project));
    return zip.toBuffer();
  }
};
