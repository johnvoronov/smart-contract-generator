const ethers = require('ethers');
const utils = require('./utils');

class Contract {
  constructor(abi, bytecode, constructorArgs = []) {
    this.abi = typeof abi === 'string' ? JSON.parse(abi.toString()) : abi;
    this.bytecode = bytecode.toString();
    this.constructorArgs = constructorArgs;

    this.factory = new ethers.ContractFactory(abi, bytecode);
  }

  encodeABI() {
    return this.factory.interface.deployFunction.encode(
      utils.hexlifyString(this.bytecode),
      this.constructorArgs
    );
  }

  getAbi() {
    return this.abi;
  }

  getBytecode() {
    return this.bytecode;
  }

  getConstructorArgs() {
    return this.constructorArgs;
  }
}

module.exports = Contract;
