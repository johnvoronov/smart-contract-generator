const Ethereum = require('./providers/ethereum/ethereum');
const Quark = require('./providers/quark/quark');
const Fantom = require('./providers/fantom/fantom');
const EveriToken = require('./providers/everitoken/everitoken');

module.exports = {
  createProvider(name, wallet, network = 'rinkeby') {
    switch (name) {
      case 'quark':
        return new Quark(wallet, network);

      case 'fantom':
        return new Fantom(wallet, network);

      case 'ethereum':
        return new Ethereum(wallet, network);

      case 'everitoken':
        return new EveriToken(wallet, network);

      default:
        return new Error('Unsupported provider: ' + name);
    }
  }
};
