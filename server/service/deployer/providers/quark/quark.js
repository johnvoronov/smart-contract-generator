const axios = require('axios');
const ethers = require('ethers');
const Base = require('../base');
const Web3 = require('./web3-legacy/web3');
const Quarkchain = require('./quark/quark-web3');
const utils = require('./utils');
const poll = require('../../poll');

class QuarkEntity extends Base {
  constructor(wallet, network, web3provider = null) {
    super(wallet);

    web3provider = web3provider || new Web3.providers.HttpProvider(
      'https://rinkeby.infura.io/v3/ef9be6a96d844199877fb065c044c350'
    );
    this.instance = new Web3(web3provider);
    this.endpoint = 'http://jrpc.testnet.quarkchain.io:38391';
    this.provider = new ethers.providers.JsonRpcProvider(this.endpoint);

    Quarkchain.injectWeb3(this.instance, this.endpoint);
    this.instance.qkc.setPrivateKey(wallet.getPrivateKey());
  }

  async transferOwnership(abi, address, contractAddress) {
    const data = this.encodeContractMethod(abi, address, 'transferOwnership', [contractAddress]);
    return await this.sendTransaction({
      data: data,
      to: address,
      gasLimit: await this.getEstimateGas()
    });
  }

  /**
   * Получение баланса пользователя
   *
   * @param abi
   * @param address
   * @param identifier
   * @returns {Promise<float>}
   */
  async getBalance(abi, address, identifier) {
    const result = await this.callContractMethod(abi, address, 'balanceOf', [identifier]);

    return ethers.utils.formatEther(result.toString());
  }

  sendTransaction({ data, nonce, from, to, gasLimit }) {
    return new Promise((resolve, reject) => {
      const params = {
        data,
        nonce,
        to,
        // Convert BN, BigNumber from ethers and other value type to 16bit integer
        gas: `0x${gasLimit.toNumber().toString(16)}`
      };

      this.instance.qkc.sendTransaction(params, (err, transactionId) => {
        if (err) {
          reject(err);
        } else {
          resolve(transactionId);
        }
      });
    })
  }

  async getTransactionStatus(hash) {
    const receipt = await this.instance.qkc.getTransactionReceipt(hash);
    if (null === receipt) {
      return null;
    }

    return !!parseInt(receipt.status, 16);
  }

  _contractMethod(abi, address, method) {
    const abiJson = typeof abi === 'string' ? JSON.parse(abi.toString()) : abi;
    const MyContract = this.instance.qkc.contract(abiJson);
    const contractInstance = MyContract.at(address);
    return contractInstance[method];
  }

  encodeContractMethod(abi, address, method, args = []) {
    return this._contractMethod(abi, address, method).getData(...args);
  }

  callContractMethod(abi, address, method, args = []) {
    return this._contractMethod(abi, address, method).call(...args);
  }

  getEstimateGas(tx) {
    return Promise.resolve(ethers.utils.bigNumberify(1000000)); // TODO
  }

  getTransactionUrl(hash) {
    return `https://testnet.quarkchain.io/tx/${hash}`;
  }

  getNonce(address = null) {
    return this.instance.qkc.getTransactionCount(
      utils.toQuark(address || this.wallet.getAddress())
    );
  }

  _sendCreateContractTx(contractClass, args, tx) {
    return new Promise((resolve, reject) => {
      contractClass.new(...args, tx, (err, result) => {
        if (err) {
          reject(err);
        } else {
          // NOTE: The callback will fire twice!
          // Once the contract has the transactionId property set and once its deployed on an address.

          // e.g. check tx hash on the first call (transaction send)
          resolve(result.transactionId); // The id of the transaction, which deploys the contract

          // Note that the returned "contractInstance" === "contract",
          // so the returned "contractInstance" object will also get the address set.
        }
      })
    });
  }

  async getTransaction(hash) {
    return axios.post(this.endpoint, {
      jsonrpc: '2.0',
      method: 'getTransactionById',
      params: [hash],
      id: 1
    }).then(resp => resp.data);
  }

  async deployContract(contract) {
    const contractClass = this.instance.qkc.contract(contract.getAbi());
    const nonce = await this.getNonce();

    // const argumentsBytecode = contractClass.new.getData(...contract.getConstructorArgs()).replace('undefined', '');
    const txHash = await this._sendCreateContractTx(contractClass, contract.getConstructorArgs(), {
      data: `0x${contract.getBytecode()}`,
      nonce: `0x${nonce.toString(16)}`,
      gasPrice: '0x2540be400',
      gas: '0x989680',
      fromFullShardId: '0xfbcdfcea',
      toFullShardId: '0xfbcdfcea',
      value: '0x0',
      networkId: '0x3',
      version: '0x01'
    });

    return await poll.wait(async () => {
      const resultTx = await this.instance.qkc.getTransactionReceipt(txHash);
      if (resultTx) {
        if (0 === parseInt(resultTx.status, 16)) {
          throw new Error('Error while deploy contract');
        }

        if (resultTx.contractAddress) {
          return {
            address: resultTx.contractAddress,
            hash: txHash
          };
        }
      }
    }, { delay: 350 });
  }
}

module.exports = QuarkEntity;
