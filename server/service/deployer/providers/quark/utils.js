const ethUtil = require('ethereumjs-util');

module.exports = {
  getShardId(address) {
    if (address.length === 50) {
      return `0x${address.slice(-8)}`;
    } else {
      let fullShardId = '';
      for (let i = 2; i < 42; i += 10) {
        fullShardId += address.slice(i, i + 2);
      }
      const shard = `0x${fullShardId}`;
      return shard.toLowerCase();
    }
  },

  getAddressFromPrivateKey(value) {
    if (value.length !== 66) {
      throw new Error('Invalid key');
    }

    return `0x${ethUtil.privateToAddress(ethUtil.toBuffer(value)).toString('hex')}`;
  },

  toQuark(address) {
    if (address.length === 42) {
      const quarkAddress = `${address}${this.getShardId(address).slice(2)}`;
      return quarkAddress.toLowerCase();
    }

    return address.toLowerCase();
  },

  toEthereum(address) {
    if (address.length === 42) {
      return address.toLowerCase();
    }

    return address.slice(0, 42).toLowerCase();
  }
};
