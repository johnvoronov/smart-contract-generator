const ethers = require('ethers');
const axios = require('axios');
const Web3 = require('web3');
const Base = require('../base');
const poll = require('../../poll');
const EthereumTx = require('ethereumjs-tx');
const web3utils = require('web3-utils');

class FantomEntity extends Base {
  constructor(wallet, network) {
    super(wallet);
    this.endpoint = 'http://18.221.128.6:8080';
    this.web3provider = new Web3.providers.HttpProvider(this.endpoint);
    this.web3 = new Web3(this.web3provider);
  }

  async transferOwnership(abi, address, contractAddress) {
    const data = this.encodeContractMethod(abi, address, 'transferOwnership', [contractAddress]);
    return await this.sendTransaction({
      data: data,
      to: address,
      gasLimit: await this.getEstimateGas()
    });
  }

  _contractMethod(abi, address, method, args = []) {
    const contract = new this.web3.eth.Contract(JSON.parse(abi.toString()), address);
    return contract.methods[method](...args);
  }

  async sendTransaction({ data, nonce, from, to, gasLimit }) {
    const tx = new EthereumTx({
      nonce,
      gasPrice: '0x000000000001',
      gasLimit: gasLimit.toString(),
      to,
      value: '0x00',
      data,
      // EIP 155 chainId - mainnet: 1, ropsten: 3
      chainId: this.getNetwork() === 'rinkeby' ? 3 : 1
    });
    tx.sign(this.wallet.getPrivateKeyBuffer());

    const resp = await axios.post(
      `${this.endpoint}/sendRawTransaction`,
      '0x' + tx.serialize().toString('hex')
    );

    return resp.data.txHash;
  }

  async getTransactionStatus(hash) {
    const resp = await axios.get(`${this.endpoint}/tx/${hash}`);

    return !resp.data.failed;
  }

  getTransaction(hash) {
    return axios.get(`${this.endpoint}/tx/${hash}`);
  }

  encodeContractMethod(abi, address, method, args = []) {
    return this._contractMethod(abi, address, method, args).encodeABI();
  }

  async callContractMethod(abi, address, method, args = []) {
    const tx = {
      from: this.wallet.getAddress(),
      value: 0,
      gasPrice: 1,
      to: address,
      data: this.encodeContractMethod(abi, address, method, args)
    };

    const resp = await axios.post(`${this.endpoint}/call`, tx);
    return resp.data.data;
  }

  getTransactionUrl(hash) {
    return `${this.endpoint}/tx/${hash}`;
  }

  getEstimateGas(tx) {
    return Promise.resolve(ethers.utils.bigNumberify(1000000)); // TODO
  }

  async getNonce(address = null) {
    address = address || this.wallet.getAddress();

    const resp = await axios.get(`${this.endpoint}/account/${address}`);

    return parseInt(resp.data.nonce, 10);
  }

  /**
   * Получение баланса пользователя
   *
   * @param abi
   * @param address
   * @param identifier
   * @returns {Promise<float>}
   */
  async getBalance(abi, address, identifier) {
    const result = await this.callContractMethod(abi, address, 'balanceOf', [identifier]);
    return ethers.utils.formatEther(parseInt(result, 16).toString());
  }

  async deployContract(contract) {
    const contractClass = new this.web3.eth.Contract(contract.getAbi());
    const contractBytecodeWithArguments = contractClass.deploy({
      data: `0x${contract.getBytecode()}`,
      arguments: contract.getConstructorArgs()
    }).encodeABI();
    const txParams = {
      from: this.wallet.getAddress(),
      value: 0,
      nonce: await this.getNonce(this.wallet.getAddress()),
      chainId: 1,
      gas: 1000000,
      gasPrice: 0,
      data: contractBytecodeWithArguments
    };

    const tx = new EthereumTx(txParams);
    tx.sign(this.wallet.getPrivateKeyBuffer());

    const contractResp = await axios.post(
      `${this.endpoint}/sendRawTransaction`,
      '0x' + tx.serialize().toString('hex')
    );

    return await poll.wait(async () => {
      let resp;
      try {
        resp = await this.getTransaction(contractResp.data.txHash);
        console.log('wait', contractResp.data.txHash, resp.data.contractAddress);
        if (resp.status === 500 && resp.data === 'leveldb: not found\\n') {
          // У фантомов прикол такой: до завершения майнинга даже транзакции в бд
          // не создается и веб ui валит 500-ые `leveldb: not found`
        }
      } catch (e) {
      }

      if (resp) {
        if (resp.data.failed) {
          throw new Error('Error while deploy contract');
        }

        if (resp.data.contractAddress) {
          return {
            address: resp.data.contractAddress,
            hash: contractResp.data.txHash
          };
        }
      }
    }, { delay: 350 });
  }
}

module.exports = FantomEntity;
