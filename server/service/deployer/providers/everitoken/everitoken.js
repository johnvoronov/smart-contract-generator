const EVT = require('evtjs');

class EveriToken extends Base {
  constructor(credentials, network) {
    this.keyProvider = credentials.privateKey;
    this.wallet = credentials.wallet;
    this.network = network;
    this.apiCaller = EVT({
      endpoint: this.network,
      keyProvider: this.keyProvider
    });
    this.endpoint = 'https://evtscan.io';
  }


  /**
   * Отправить транзакцию
   *
   * @param action
   * @returns {Promise<*>}
   */
  async sendTransaction(action) {
    const { transactionId } = await this.apiCaller.pushTransaction(action);
    return transactionId;
  }

  /**
   * Получить данные о транзакции
   *
   * @param transactionId
   * @returns {Promise<*>}
   */
  async getTransaction(transactionId) {
    const { transaction } = await this.apiCaller.getTransactionDetailById(transactionId);

    return transaction;
  }

  /**
   * Получить url транзакции
   *
   * @param transactionId
   * @returns {string}
   */
  getTransactionUrl(transactionId) {
    return `${this.endpoint}/trx/${transactionId}`;
  }

  /**
   * Получить url токена
   *
   * @param symbol_id
   * @returns {string}
   */
  getTokenUrl(symbol_id) {
    return `${this.endpoint}/fungible/${symbol_id}`;
  }

  /**
   * Получить баланс
   *
   * @param address
   * @returns {Promise<*>}
   */
  async getBalance(address = null) {
    return this.apiCaller.getFungibleBalance(address || this.wallet);
  }

  /**
   * Получить оценочную сумму транзакционной комиссии за одну транзакцию.
   *
   * @param name <string>
   * @param abi <object>
   * @returns {Promise<void>}
   */
  async getEstimatedChargeForTransaction(name, abi) {
    const action = new EVT.EvtAction(name, abi);
    const { charge } = await this.apiCaller.getEstimatedChargeForTransaction(action);
    return charge;
  }

  /**
   * Получить адрес токена
   *
   * @param symbol_id <number>
   * @returns {Promise<*>}
   */
  async getTokenAddress(symbol_id) {
    const { address } = await this.apiCaller.getFungibleSymbolDetail(symbol_id);
    return address;
  }

  /**
   * Получить текущий supply
   *
   * @param symbol_id <number>
   * @returns {Promise<*>}
   */
  async getTokenCurrentSupply(symbol_id) {
    const { current_supply } = await this.apiCaller.getFungibleSymbolDetail(symbol_id);
    return current_supply;
  }

  /**
   * Получить приватный ключ
   *
   * @returns {*}
   */
  getPrivateKey() {
    return EVT.EvtKey.randomPrivateKey();
  }

  /**
   * Получить публичный ключ
   *
   * @param privateKey
   * @returns {*}
   */
  getPublicKey(privateKey) {
    return EVT.EvtKey.privateToPublic(privateKey)
  }

  /**
   * Валидация приватного ключа
   *
   * @param key
   * @returns {*}
   */
  validatePrivateKey(key) {
    return EVT.EvtKey.isValidPrivateKey(key)
  }

  /**
   * Валидация кошелька
   *
   * @param address
   * @returns {boolean|*}
   */
  validateWallet(address = null) {
    return EVT.EvtKey.isValidPublicKey(address || this.wallet)
  }

  /**
   * Отправить токены
   *
   * @param symbol_id <number>
   * @param from <string>
   * @param to <string>
   * @param amount <string>
   * @param memo <string>
   * @returns {Promise<*>}
   */
  transferTokens(symbol_id, from, to, amount, memo) {
    return this.sendTransaction(
      new EVT.EvtAction('transferft', {
        from,
        to,
        number: amount + ' S#' + symbol_id,
        memo
      })
    );
  }

  /**
   * Перечислить токены на другой адрес
   *
   * @param symbol_id <number>
   * @param address <string>
   * @param amount <string>
   * @param memo <string>
   * @returns {Promise<*>}
   */
  sendTokens(symbol_id, address, amount, memo) {
    return this.sendTransaction(
      new EVT.EvtAction('issuefungible', {
        address: address,
        number: amount + ' S#' + symbol_id,
        memo
      })
    );
  }

  /**
   * Создание и деплой токена
   *
   * @param type
   * @param supply
   * @param name
   * @param symbol
   * @param symbol_id
   * @param decimals
   * @returns {Promise<*>}
   */
   createToken({ type, supply, name, symbol, symbol_id, decimals }) {
    let precision = '.';
    for (let i = 0; i < decimals; i++) precision += 0;

    let total_supply;
    if (type === 'mintable') {
      total_supply = '0' + precision + ' S#' + symbol_id
    } else {
      total_supply = supply + precision + ' S#' + symbol_id
    }

    return this.sendTransaction(
      new EVT.EvtAction('newfungible', {
        name,
        sym_name: symbol,
        sym: decimals + ',S#' + symbol_id,
        creator: this.wallet,
        manage: { name: "manage", threshold: 1, authorizers: [ { ref: "[A] " + this.wallet, weight: 1  } ] },
        issue: { name: "issue", threshold: 1, authorizers: [ { ref: "[A] " + this.wallet, weight: 1  } ] },
        total_supply
      })
    );
  }
}

module.exports = EveriToken;
