const ethers = require('ethers');
const Web3 = require('web3');
const Base = require('../base');

class Ethereum extends Base {
  constructor(wallet, network, endpoint = 'https://rinkeby.infura.io/v3/ef9be6a96d844199877fb065c044c350') {
    super(wallet, network);

    this.endpoint = endpoint;
    this.web3provider = new Web3.providers.HttpProvider(this.endpoint);
    // this.provider = new ethers.providers.Web3Provider(this.web3provider);
    this.provider = new ethers.getDefaultProvider(network);
    this.etherWallet = new ethers.Wallet(
      this.wallet.getPrivateKey(),
      this.provider
    );
  }

  async transferOwnership(abi, address, contractAddress) {
    const data = this.encodeContractMethod(abi, address, 'transferOwnership', [contractAddress]);
    return await this.sendTransaction({
      data: data,
      to: address,
      gasLimit: await this.getEstimateGas()
    });
  }

  async sendTransaction(tx) {
    // tx = { data, nonce, from, to, gasLimit }
    const resp = await this.etherWallet.sendTransaction(tx);

    return resp.hash;
  }

  async getTransactionStatus(hash) {
    const receipt = await this.provider.getTransactionReceipt(hash);
    if (receipt === null) {
      return null;
    }

    return !!receipt.status;
  }

  getTransaction(hash) {
    return this.provider.getTransaction(hash);
  }

  _contractMethod(abi, address, method, args = []) {
    const web3 = new Web3(this.web3provider);
    const contract = new web3.eth.Contract(JSON.parse(abi.toString()), address);
    return contract.methods[method](...args);
  }

  encodeContractMethod(abi, address, method, args = []) {
    return this._contractMethod(abi, address, method, args).encodeABI();
  }

  callContractMethod(abi, address, method, args = []) {
    return this._contractMethod(abi, address, method, args).call();
  }

  getTransactionUrl(hash) {
    return `https://${this.network}.etherscan.io/tx/${hash}`;
  }

  getEstimateGas(tx) {
    return Promise.resolve(ethers.utils.bigNumberify(5000000)); // TODO
    // return this.provider.estimateGas(tx);
  }

  getNonce(address = null) {
    return this.provider.getTransactionCount(
      address || this.wallet.getAddress()
    );
  }

  /**
   * Получение баланса пользователя
   *
   * @param abi
   * @param address
   * @param identifier
   * @returns {Promise<float>}
   */
  async getBalance(abi, address, identifier) {
    const result = await this.callContractMethod(abi, address, 'balanceOf', [identifier]);
    return ethers.utils.formatEther(result);
  }

  async deployContract(contract) {
    const factory = new ethers.ContractFactory(
      contract.getAbi(),
      contract.getBytecode(),
      this.etherWallet
    );

    const contractTx = await factory.deploy(...contract.getConstructorArgs());

    const hash = contractTx.deployTransaction.hash;

    await contractTx.deployed();

    return {
      address: contractTx.address,
      hash
    };
  }
}

module.exports = Ethereum;
