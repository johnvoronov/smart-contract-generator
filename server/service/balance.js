const models = require('../models');
const { Op } = require('sequelize');
const {
  ethStatus,
  transactionStatus
} = require('../consts');

module.exports = {
  async getResponse(where, project) {
    return {
      balance: await this.getBalance(where, project),
      bonus: await this.getBonus(where, project),
      referral: await this.getReferralBonus(where, project),
      withdraw: await this.getWithdraw(where, project),
      bounty: await this.getBounty(where, project)
    };
  },

  /**
   * Получение реферального бонусного
   * по пользователю
   *
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  getReferralBonus(where, project) {
    return this._fetch({
      ...where,
      status: transactionStatus.TRANSACTION_REFERRAL
    }, project);
  },

  /**
   * Получение реферального бонусного
   * по пользователю
   *
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  getWithdraw(where, project) {
    return this._fetch({
      ...where,
      status: transactionStatus.TRANSACTION_WITHDRAW
    }, project);
  },

  /**
   * Получение бонуснов за выполнение программы баунти
   *
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  getBounty(where, project) {
    return this._fetch({
      ...where,
      status: transactionStatus.TRANSACTION_BOUNTY
    }, project);
  },

  /**
   * Получение бонусного баланса
   * по пользователю
   *
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  getBonus(where, project) {
    return this._fetch({
      ...where,
      status: transactionStatus.TRANSACTION_BONUS
    }, project);
  },

  /**
   * Получение актуального баланса
   * по пользователю
   *
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  getBalance(where, project) {
    return this._fetch(where, project);
  },

  /**
   * @param where
   * @param project
   * @returns {Promise<float>}
   */
  async _fetch(where, project) {
    if (project.blockchain.code === 'local') {
      return await models.Transaction.sum('amount', {
        where: { ...where, project_id: project.id }
      }) || 0;
    } else {
      const records = await models.Eth.findAll({
        where: {
          project_id: project.id,
          status: ethStatus.ETH_COMPLETE
        }
      });
      const ids = records.map(r => r.id);
      if (ids.length === 0) {
        return 0;
      }

      return await models.Transaction.sum('amount', {
        where: {
          ...where,
          eth_id: {
            [Op.in]: ids
          },
          project_id: project.id
        }
      }) || 0;
    }
  }
};
