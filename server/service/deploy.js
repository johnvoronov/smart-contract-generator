const path = require('path');
const { Op } = require('sequelize');
const models = require('../models');
const security = require('./security');
const appConfig = require('../config/app');
const { projectStatus } = require('../consts');
const smartGeneratorService = require('./smartGenerator');
const compilerService = require('./compiler');
const Wallet = require('./deployer/wallet');
const Contract = require('./deployer/contract');
const quarkUtils = require('./deployer/providers/quark/utils');
const factoryDeploy = require('./deployer');

module.exports = {
  async deployProjects() {
    const projects = await models.Project.findAll({
      where: {
        status: projectStatus.PROJECT_DONE
      },
      include: [
        { model: models.Blockchain, as: 'blockchain' }
      ]
    });

    for (let i = 0; i < projects.length; i++) {
      const project = projects[i];
      // Фиксируем факт деплоя проекта
      // чтобы крон не шел в нахлест
      // TODO заменить на atomic "select with update"
      if (project.blockchain.code === 'local') {
        await project.update({
          status: projectStatus.PROJECT_READY
        });
      } else {

        await project.update({
          status: projectStatus.PROJECT_PROGRESS
        });
        try {
          const data = await this.generateAndCompileContract(project);
          await this.deploy(project, data);
        } catch (e) {
          console.log(e);
          await project.update({
            status: projectStatus.PROJECT_DONE
          });
        }
      }
    }
  },

  /**
   * Генерируем контракт
   *
   * @param project
   * @returns {Promise<*|*|Function>}
   */
  async generateAndCompileContract(project) {
    const contractDir = await smartGeneratorService.createContract(project);

    return compilerService.compile(
      path.join(contractDir, 'contracts'),
      appConfig.node_modules
    );
  },

  /**
   * Деплой контракта
   *
   * @param project
   * @param data
   * @returns {Promise<boolean>}
   */
  async deploy(project, data) {
    const {
      address,
      privateKey
    } = project.credentials;

    const wallet = new Wallet(address, privateKey);

    const provider = factoryDeploy.createProvider(project.blockchain.code, wallet, 'rinkeby');

    console.log('start deploy token');
    const tokenData = await provider.deployContract(
      new Contract(data.token.abi, data.token.bytecode)
    );
    console.log(tokenData);

    // let initConstructorParams = ['uint256 rate', 'address wallet', 'ERC20 token'];
    const constructorArgs = [
      project.parameters.rate,
      project.blockchain.code === 'quark' ? quarkUtils.toEthereum(address) : address,
      project.blockchain.code === 'quark' ? quarkUtils.toEthereum(tokenData.address) : tokenData.address
    ];
    console.log('start deploy ico');
    const icoData = await provider.deployContract(
      new Contract(data.ico.abi, data.ico.bytecode, constructorArgs)
    );
    console.log(icoData);

    console.log('start transferOwnership');
    const transferTx = await provider.transferOwnership(
      data.token.abi,
      tokenData.address,
      icoData.address
    );
    console.log({
      transferTx,
      token: tokenData.address,
      ico: icoData.address
    });

    await project.update({
      status: projectStatus.PROJECT_READY,
      contract: {
        token: {
          ...data.token,
          address: tokenData.address,
          hash: tokenData.hash
        },
        ico: {
          ...data.ico,
          address: icoData.address,
          hash: icoData.hash
        }
      }
    });
  }
};
