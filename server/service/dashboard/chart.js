const models = require('../../models');
const momentBase = require('moment');
const momentRange = require('moment-range');
const intervalToDate = require('./intervalToDate');
const sequelize = models.sequelize;
const {
  TRANSACTION_WITHDRAW,
  TRANSACTION_PAYMENT,
  TRANSACTION_BONUS
} = require('../../consts');

module.exports = {
  rawQuery(sql, replacements = {}) {
    return sequelize.query(sql, {
      type: sequelize.QueryTypes.SELECT,
      replacements
    });
  },

  async getStats(interval) {
    /*
WITH subquery AS
(
    SELECT
      sum(amount) AS amount,
      date_trunc('day', created_at) AS date
    FROM transaction
    GROUP BY date
)
SELECT date, COALESCE(amount, 0) AS amount
FROM
  (
    SELECT generate_series('2017-07-29'::date, '2018-08-29'::date, interval '1 day') AS date
    FROM subquery
  ) x
  LEFT JOIN subquery
  USING (date)
ORDER BY date;
     */
    const genericChartsql = `
select
  to_char(d, :format) as date,
  COALESCE(sum("transaction"."amount"), 0) as amount
from generate_series(:from::date, :to::date, interval '1 day') d
  left join "transaction" on date("transaction"."created_at") = d
    and "transaction"."status" = :status
group by date
order by date;
`;

    const {
      format,
      from,
      to
    } = intervalToDate.getDatesFromInterval(interval);

    const replacements = {
      format,
      from: from.format(),
      to: to.format(),
    };

    return {
      bonus: await this.rawQuery(genericChartsql, {
        ...replacements,
        status: TRANSACTION_BONUS
      }),
      token: await this.rawQuery(genericChartsql, {
        ...replacements,
        status: TRANSACTION_PAYMENT
      }),
      withdraw: await this.rawQuery(genericChartsql, {
        ...replacements,
        status: TRANSACTION_WITHDRAW
      })
    };
  }
};
