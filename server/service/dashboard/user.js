const models = require('../../models');
const intervalToDate = require('./intervalToDate');
const Op = require('sequelize').Op;

module.exports = {
  async getStats(interval) {
    const {
      from,
      to
    } = intervalToDate.getDatesFromInterval(interval);

    const where = {
      created_at: {
        [Op.between]: [
          from.toDate(),
          to.toDate()
        ]
      }
    };

    const total = await models.User.count({ where }) || 0;

    const active = await models.User.count({
      where: {
        ...where,
        is_active: true
      }
    }) || 0;

    return {
      total: parseInt(total, 10),
      active: parseInt(active),
      percent: parseFloat((total / active) || 0).toFixed(2)
    };
  }
};
