const momentBase = require('moment');
const momentRange = require('moment-range');
const moment = momentRange.extendMoment(momentBase);

module.exports = {
  getToday() {
    return moment();
  },

  getDaysDiff(from, to) {
    const duration = moment.duration(moment(to).diff(moment(from)));

    return Math.ceil(duration.asDays());
  },

  getDatesFromInterval(interval) {
    let from, to, format;

    switch (interval) {
      case 'year':
        from = this.getToday().subtract(1, 'year');
        to = this.getToday();
        format = 'YYYY';
        break;

      case 'quarter':
        from = this.getToday().subtract(3, 'month');
        to = this.getToday();
        format = 'YYYY-MM';
        break;

      case 'month':
        from = this.getToday().subtract(1, 'month');
        to = this.getToday();
        format = 'YYYY-MM';
        break;

      case 'week':
        from = this.getToday().subtract(7, 'days');
        to = this.getToday();
        format = 'YYYY-MM-DD';
        break;

      case 'yesterday':
        from = this.getToday().subtract(1, 'days');
        to = this.getToday().subtract(1, 'days');
        format = 'YYYY-MM-DD';
        break;

      case 'today':
      default:
        from = this.getToday();
        to = this.getToday();
        format = 'YYYY-MM-DD';
        break;
    }

    return {
      format,
      from: from.startOf('day'),
      to: to.endOf('day')
    };
  }
};
