const models = require('../../models');
const intervalToDate = require('./intervalToDate');
const Op = require('sequelize').Op;

module.exports = {
  async getStats(interval) {
    const {
      from,
      to
    } = intervalToDate.getDatesFromInterval(interval);

    const where = {
      created_at: {
        [Op.between]: [
          from.toDate(),
          to.toDate()
        ]
      }
    };

    const total = await models.User.count({ where }) || 0;

    const withBalance = await models.Transaction.findAll({
      group: [
        'user_id',
        'id'
      ],
      where
    }) || 0;

    return {
      total: parseInt(withBalance.length, 10),
      percent: (total > 0 && withBalance.length > 0)
        ? parseFloat((total / withBalance.length) || 0).toFixed(2)
        : 0
    };
  }
};
