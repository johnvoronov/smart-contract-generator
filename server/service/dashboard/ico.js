const models = require('../../models');
// const settings = require('../../util/settings');
const sequelize = models.sequelize;

module.exports = {
  async getStats() {
    const topCurrencies = await models.Payment.findAll({
      group: ['currency.id'],
      order: [
        [sequelize.literal('raised'), 'DESC']
      ],
      attributes: [
        'currency.id',
        [sequelize.fn('SUM', sequelize.literal('amount')), 'raised']
      ],
      include: [
        {
          model: models.Currency,
          as: 'currency'
        }
      ],
      limit: 5,
      offset: 0
    });

    const model = await models.Payment.findOne({
      attributes: [
        [sequelize.fn('SUM', sequelize.literal('tokens_amount * token_price')), 'raised']
      ]
    });
    const raised = model.getDataValue('raised') || 0;

    return {
      raised: {
        top: topCurrencies,
        token: await models.Payment.sum('tokens_amount'),
        usd: parseFloat(raised),
      },
      cap: {
        hardcap: 0, // TODO await settings.getValue('token_hardcap') || 0,
        softcap: 0, // TODO await settings.getValue('token_softcap') || 0
      }
    };
  }
};
