const models = require('../../models');
const intervalToDate = require('./intervalToDate');
const Op = require('sequelize').Op;

module.exports = {
  async getStats(interval) {
    const {
      from,
      to
    } = intervalToDate.getDatesFromInterval(interval);

    const where = {
      created_at: {
        [Op.between]: [
          from.toDate(),
          to.toDate()
        ]
      }
    };

    const total = await models.Transaction.sum('amount', {
      where: { ...where, is_withdraw: false }
    }) || 0;

    const withdraw = await models.Transaction.sum('amount', {
      where: { ...where, is_withdraw: true }
    }) || 0;

    return {
      total: parseFloat(total),
      withdraw: parseFloat(withdraw),
      percent: total > 0 && withdraw > 0 ? parseFloat((total / withdraw) || 0).toFixed(2) : 0
    };
  }
};
