const userStats = require('./user');
const tokenStats = require('./token');
const withdrawStats = require('./withdraw');
const icoStats = require('./ico');
const chartStats = require('./chart');

module.exports = {
  async getStats(interval) {
    return {
      chart: await chartStats.getStats(interval),
      ico: await icoStats.getStats(),
      user: await userStats.getStats(interval),
      token: await tokenStats.getStats(interval),
      withdraw: await withdrawStats.getStats(interval),
    };
  }
};
