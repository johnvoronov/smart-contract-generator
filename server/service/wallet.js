const Coinpayments = require('coinpayments');
const models = require('../models');

module.exports = {
  /**
   * Создание клиента coinpayments для конкретного
   * проекта
   *
   * @param key
   * @param secret
   * @returns {Coinpayments}
   */
  createClient(key, secret) {
    return new Coinpayments({ key, secret });
  },

  /**
   * Создание нового кошелька для пользователя
   * под конкретный проект и выбранную пользователем
   * криптовалюту
   *
   * @param user
   * @param project
   * @param currency
   * @returns {Promise<*>}
   */
  async getWallet(user, project, currency) {
    const {
      public_key,
      secret_key,
      merchant_id
    } = project.coinpayments;

    const wallet = await models.Wallet.findOne({
      where: {
        merchant_id,
        project_id: project.id,
        user_id: user.id,
        currency_id: currency.id
      }
    });

    if (null === wallet) {
      const client = this.createClient(public_key, secret_key);
      const coinpaymentsWallet = await client.getCallbackAddress({
        currency: currency.code
      });

      return await models.Wallet.create({
        merchant_id,
        user_id: user.id,
        project_id: project.id,
        address: coinpaymentsWallet.address,
        currency_id: currency.id,
      });
    }

    return wallet;
  }
};
