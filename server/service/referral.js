const models = require('../models');
const {
  PAYMENT_COMPLETE,
  TRANSACTION_REFERRAL
} = require('../consts');

module.exports = {
  /**
   * Сетка с уровнями реферальной системы
   *
   * @returns {*[]}
   */
  async getGrid() {
    return await models.Referral.findAll({
      order: [
        ['team', 'DESC'],
        ['user', 'DESC']
      ]
    });
  },

  validate(item, a, b) {
    return this.validateUser(item, a);
  },

  validateTeam(item, b) {
    return b >= item.team;
  },

  validateUser(item, a) {
    return a >= item.user;
  },

  /**
   * Поиск процента реферала
   *
   * @param user
   * @returns {Promise<*>}
   */
  async getReferralLevel(user) {
    const levels = await this.getGrid();
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);

    for (let i = 0; i < levels.length; i++) {
      const level = levels[i];

      if (
        this.validateUser(level, userAmount)
        && this.validateTeam(level, teamAmount)
      ) {
        return level;
      }
    }

    return null;
  },

  async getReferralCount(user_id) {
    return await models.User.count({
      where: { owner_id: user_id }
    }) || 0;
  },

  /**
   * Получение процента
   *
   * @param level
   * @param index
   * @returns {float}
   */
  getPercent(level, index = 0) {
    if (!level) {
      return 0;
    }

    const levels = level.levels || [];
    return levels[index] || 0;
  },

  /**
   * Поиск текущего уровня реферала
   *
   * @param user
   * @returns {Promise<*>}
   */
  async getCurrentLevel(user) {
    const levels = await this.getGrid();
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);

    for (let i = 0; i < levels.length; i++) {
      const level = levels[i];

      if (this.validate(level, userAmount, teamAmount)) {
        return level;
      }
    }

    return null;
  },

  /**
   * Поиск следующего уровня реферала
   *
   * @param user
   * @returns {Promise<*>}
   */
  async getNextLevel(user) {
    const levels = await this.getGrid();
    const userAmount = await this.getUserAmount(user);
    const teamAmount = await this.getTeamAmount(user);

    for (let i = 0; i < levels.length; i++) {
      const level = levels[i];

      if (this.validate(level, userAmount, teamAmount)) {
        if (typeof levels[i - 1] === 'undefined') {
          return level;
        } else {
          return levels[i - 1];
        }
      }
    }

    return null;
  },

  async getStats(user) {
    const current = await this.getCurrentLevel(user);
    const next = await this.getNextLevel(user);

    const user_raised = await this.getUserAmount(user);
    const team_raised = await this.getTeamAmount(user);

    return {
      raised: {
        user: user_raised,
        team: team_raised
      },
      current: {
        name: current.name,
        team: current.team,
        user: current.user,
        levels: current.levels
      },
      next: {
        name: next.name,
        team: next.team,
        user: next.user,
        levels: next.levels
      }
    };
  },

  /**
   * Выборка актуальной текущей суммы сборов по текущему пользователю
   * за вычетом ранее выведенных средств.
   *
   * @param user
   * @returns {Promise<Number|*|number>}
   */
  async getRealUserAmount(user) {
    const amount = await this.getUserAmount(user); // TODO сумма инвестиций в $

    const withdraw = await models.ReferralWithdraw.sum('amount', {
      where: { user_id: user.id }
    }) || 0; // TODO сумма инвестиций в ETH

    return amount - withdraw;  // TODO а тут баг, $ - ETH = ?
  },

  /**
   * Выборка общей суммы сборов по текущему пользователю.
   * Операции is_withdraw не учитываются.
   *
   * @param user
   * @returns {Promise<Number|*|number>}
   */
  async getUserAmount(user) {
    return await models.Payment.sum('tokens_amount', {
      where: {
        user_id: user.id,
        status: PAYMENT_COMPLETE
      }
    }) || 0;
  },

  /**
   * Выборка общей суммы сборов по всем реферальным пользователям
   * текущего пользователя.
   *
   * @param user
   * @returns {Promise<Promise<Number>|*|number>}
   */
  async getTeamAmount(user) {
    return await models.Payment.sum('tokens_amount', {
      where: {
        status: PAYMENT_COMPLETE
      },
      include: [
        {
          model: models.User,
          as: 'user',
          where: {
            owner_id: user.id
          }
        }
      ],
      group: [
        'user.id'
      ]
    }) || 0;
  },

  /**
   * Вспомогательный метод для упрощения тестов
   *
   * @param id
   * @returns {Promise<Model>}
   */
  async findUser(id) {
    return await models.User.findById(id)
  },

  /**
   * Поиск главного владельца реф. цепочки и выше стоящего
   * реф. пользователя.
   *
   * nested set использовать не стал, так как является избыточным
   * для данного решения
   *
   * adjacency list выборку по дереву вверх штатными средствами
   * субд (sqlite, mysql, pgsql) не реализовать, а вложенность может
   * быть бесконечная
   *
   * Данная реализация с примитивным materialized path для выборки вверх
   * и хранения главного владельца реф. цепочки
   *
   * @param id
   * @returns {Promise<*>}
   */
  async findReferral(id) {
    const owner = await this.findUser(id);
    if (null === owner) {
      return {};
    }

    const parents = [
      ...(owner.parents || [])
    ];
    parents.push(owner.id);

    return {
      owner_id: parents[0],
      referral_id: id,
      parents
    }
  },

  /**
   * @param payment
   * @returns {Promise<Array>}
   */
  async createTransaction(payment) {
    // Возвращаем список созданных транзакций
    // для тестов и отладки
    const transactions = [];

    // Выбираем пользователя совершившего платеж.
    // Если пользователь не является чьим то рефералом,
    // то пропускаем дальнейшие действия
    const user = await payment.getUser();
    if (!user.owner_id) {
      return transactions;
    }

    // Получение всей иерархии рефералов вниз по идентификаторам пользователей
    // в соответствии индексами уровней реф. системы, где ключ соответствует
    // ключам в уровнях.
    const ids = user.parents || [];
    for (let index = 0; index < ids.length; index++) {

      // Выбираем конкретного пользователя в итерации
      const owner = await models.User.findById(ids[index]);

      // Если пользователь по каким то причинам удален или не существует
      // то пропускаем зачисление
      if (!owner) {
        continue;
      }

      // Получаем текущий уровень из реф. сетки
      // Выбор уровней происходит по выполнению условий по личным инвестициям
      // конкретного пользователя и по инвестициям команды. Нам нужно из данного
      // запроса только знать level, там статус по персональным инвестициям нам
      // не важен. Процент берется только если сумма пользователя И сумма команды
      // соответствуют условиям. Когда на морде ЛК мы отображаем его статус без проверки
      // по инвестициям команды, так как серым и зеленым нужно отображать выполнение условий
      const level = await this.getReferralLevel(owner, index);

      const percent = this.getPercent(level, index);

      // Пропускаем бонусы где нечего зачислять:
      // к примеру это 8 колено реф системы где всегда бонусы по нулям
      // или пользователи не выполнившие первоначальные условия
      // реферальной системы
      if (percent === 0) {
        continue;
      }

      const tx = await models.Transaction.create({
        // Сохраняем какому пользователю за платеж
        // реферала зачислять бонус
        user_id: owner.id,
        // Сохраняем идентификатор платежа
        payment_id: payment.id,
        // Расчитываем количество ETH на момент покупки
        amount: payment.tokens_amount + (payment.tokens_amount / 100 * percent),
        // Сохраняем процент по которому
        // зачисляем бонус
        comment: `Referral percent: ${percent}, level: ${level.name}`,
        status: TRANSACTION_REFERRAL
      });
      transactions.push(tx);
    }

    return transactions;
  }
};
