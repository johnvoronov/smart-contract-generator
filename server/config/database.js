const defaultOptions = {
  username: process.env.RDS_USERNAME,
  password: process.env.RDS_PASSWORD,
  database: process.env.RDS_DB_NAME,
  host: process.env.RDS_HOSTNAME,
  port: process.env.RDS_PORT,
  dialect: 'postgres',

  logging: console.log,
  migrationStorageTableName: 'migrations',
  freezeTableName: true,
  operatorsAliases: false,
  dialectOptions: {
    /**
     * Обязательный параметр для корректного сохранения данных в
     * decimal(27, 18) (миллионы и 18 чисел после запятой)
     * и корректного прокидывания из бд вместо строк - float
     */
    decimalNumbers: true
  },
  define: {
    underscored: true,
    freezeTableName: true,
    charset: 'utf8',
    paranoid: false,
    version: false
  }
};

module.exports = {
  production: { ...defaultOptions, logging: false },
  development: defaultOptions,
  test: { ...defaultOptions, logging: false }
};
