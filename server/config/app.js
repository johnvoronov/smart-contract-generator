const path = require('path');

module.exports = {
  secret_key: process.env.MASTER_KEY,
  node_modules: path.resolve(__dirname, '../../node_modules'),
  contract_path: path.resolve(__dirname, '../../contracts'),
  frontend_url: process.env.FRONTEND_URL || 'https://example.com',
  jwt_secret: process.env.JWT_SECRET,
  i18n: {
    default_locale: 'en',
    locales: [
      'en',
      'ru',
      'ja',
      'cn',
      'ko'
    ]
  },
  sentry: {
    dsn: process.env.SENTRY_DSN || '',
    release: process.env.APP_RELEASE || 'dev',
    environment: process.env.APP_ENVIRONMENT || 'localhost'
  },
  settings: {
    encrypted: [
      'coinpayments_public_key',
      'coinpayments_secret_key',
      'coinpayments_ipn_secret',
      'coinpayments_merchant_id',

      'recaptcha_public_key',
      'recaptcha_secret_key',

      'smtp_from',
      'smtp_host',
      'smtp_port',
      'smtp_password',
      'smtp_username'
    ],
    secret: [
      'coinpayments_public_key',
      'coinpayments_secret_key',
      'coinpayments_ipn_secret',
      'coinpayments_merchant_id',

      'recaptcha_public_key',
      'recaptcha_secret_key',

      'smtp_from',
      'smtp_host',
      'smtp_port',
      'smtp_password',
      'smtp_username',

      'social_facebook_client_secret',
      'social_google_client_secret',
      'social_twitter_client_secret'
    ]
  }
};
