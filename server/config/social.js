module.exports = {
  twitter: {
    id: process.env.SOCIAL_TWITTER_ID,
    secret: process.env.SOCIAL_TWITTER_SECRET
  },
  facebook: {
    id: process.env.SOCIAL_FACEBOOK_ID,
    secret: process.env.SOCIAL_FACEBOOK_SECRET
  },
  google: {
    id: process.env.SOCIAL_GOOGLE_ID,
    secret: process.env.SOCIAL_GOOGLE_SECRET
  }
};
