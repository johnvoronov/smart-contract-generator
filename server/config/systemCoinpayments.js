module.exports = {
  publicKey: process.env.SYSTEM_COINPAYMENTS_PUBLIC_KEY,
  secretKey: process.env.SYSTEM_COINPAYMENTS_SECRET_KEY,
  ipnSecret: process.env.SYSTEM_COINPAYMENTS_IPN_SECRET,
  merchantId: process.env.SYSTEM_COINPAYMENTS_MERCHANT_ID
};
