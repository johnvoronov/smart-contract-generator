module.exports = {
  blockchain_developer: process.env.BLOCKCHAIN_DEVELOPER || 'system@team.devchain.tech',
  gas_limit: 100000
};
