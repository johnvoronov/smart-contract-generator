const disableSending = (process.env.NODE_ENV === 'test')
  || (process.env.DISABLE_MAILER || false);

module.exports = {
  from: process.env.SMTP_FROM,
  transport: disableSending ? {
    jsonTransport: true
  } : {
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: true,
    auth: {
      user: process.env.SMTP_USERNAME,
      pass: process.env.SMTP_PASSWORD
    }
  }
};
