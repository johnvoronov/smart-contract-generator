const express = require('express');
const router = express.Router();
const Joi = require('joi');
const models = require('../../models');
const walletService = require('../../service/wallet');
const validation = require('../../middleware/validation');

router.post('', validation({
  body: {
    currency_id: Joi.number().positive().required()
  }
}), async (req, res) => {
  const {
    currency_id
  } = req.body;

  const currency = await models.Currency.findOne({
    where: {
      id: currency_id,
      is_enabled: true
    }
  });
  if (null === currency) {
    return res.boom.notFound();
  }

  const {
    address
  } = await walletService.getWallet(req.user, req.project, currency);

  res.json({ address });
});

// @DEPRECATED
router.post('/eth', async (req, res) => {
  const currency = await models.Currency.findOne({
    where: {
      code: 'ETH',
      is_enabled: true
    }
  });

  if (!currency) {
    return res.boom.notFound('currency not found');
  }

  const userWallet = await walletService.getWallet(req.user, currency);

  res.json({
    address: userWallet.address
  });
});

module.exports = router;
