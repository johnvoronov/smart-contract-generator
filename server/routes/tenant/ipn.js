const express = require('express');
const router = express.Router();
const models = require('../../models');
const referral = require('../../service/referral');
const discount = require('../../service/discount');
const mail = require('../../service/mail');
const calc = require('../../util/calculator');
const ipnProject = require('../../middleware/ipnProject');
const ipn = require('../../middleware/ipn');
const { callback } = require('../../util/coinpayment_events');
const { paymentStatus, transactionStatus } = require('../../consts');

router.post('', ipnProject, ipn, async (req, res) => {
  const status = parseInt(req.body.status, 10);

  let state = null;
  if (status < 0) {
    state = paymentStatus.PAYMENT_ERROR;
  }
  if (status < 100) {
    state = paymentStatus.PAYMENT_PENDING;
  }
  if (status === 100) {
    state = paymentStatus.PAYMENT_COMPLETE;
  }

  const {
    payment,
    wallet
  } = await callback(state, req.body);

  const user = await wallet.getUser();
  const project = await wallet.getProject();

  if (!user) {
    return res.boom.notFound(null, {
      error: 'User not found'
    });
  }

  const emailData = {
    payment,
    wallet,
    user
  };

  // Только для успешно проведенного платежа создаем транзакцию и
  // отправляем пользователю уведомление на почту
  if (payment.status === paymentStatus.PAYMENT_COMPLETE) {
    const tx = await models.Transaction.create({
      user_id: user.id,
      payment_id: payment.id,
      project_id: project.id,
      amount: payment.tokens_amount,
      status: transactionStatus.TRANSACTION_PAYMENT
    });

    mail.send(user.email, 'payment_success', {
      ...emailData,
      transaction: tx
    });

    const bonus = calc.getBonusFor(payment.tokens_amount, await discount.getDiscount(project));
    if (bonus > 0) {
      const txBonus = await models.Transaction.create({
        user_id: user.id,
        payment_id: payment.id,
        project_id: project.id,
        status: transactionStatus.TRANSACTION_BONUS,
        amount: bonus
      });

      mail.send(user.email, 'payment_bonus', {
        ...emailData,
        transaction: txBonus
      });
    }

    const referralTransactions = await referral.createTransaction(payment);
    if (referralTransactions.length > 0) {
      for (let t = 0; t < referralTransactions.length; t++) {
        const txReferral = referralTransactions[t];

        mail.send(user.email, 'payment_referral', {
          referral: txReferral
        });
      }
    }

    res.status(201).json({ id: tx.id });
  }
});

module.exports = router;
