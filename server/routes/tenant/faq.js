const express = require('express');
const router = express.Router();
const guard = require('express-jwt-permissions')();
const validation = require('../../middleware/validation');
const models = require('../../models');
const Joi = require('joi');
const filterValues = require('../../util/filterValues');

router.get('', async (req, res) => {
  const rows = await models.Faq.findAll({
    order: [
      ['id', 'DESC']
    ]
  });

  res.json(rows);
});

router.post('', guard.check(['admin']), validation({
  body: {
    question: Joi.string().required(),
    answer: Joi.string().required(),
    position: Joi.number().integer().default(0)
  }
}), async (req, res) => {
  const faq = await models.Faq.create(filterValues(req.body));

  res.status(201).json({
    id: faq.id
  });
});

router.post('/:id', guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().required()
  },
  body: {
    title: Joi.string(),
    sub_title: Joi.string(),
    content: Joi.string(),
    content_short: Joi.string()
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);

  if (!faq) {
    return res.boom.notFound();
  }

  await faq.update(filterValues(req.body));

  res.status(204).end();
});

router.delete('/:id', guard.check(['admin']), validation({
  params: {
    id: Joi.number().integer().required()
  }
}), async (req, res) => {
  const faq = await models.Faq.findById(req.params.id);

  if (!faq) {
    return res.boom.notFound();
  }

  await faq.destroy();

  res.status(204).end();
});

module.exports = router;
