const express = require('express');
const router = express.Router();
const discount = require('../../service/discount');

router.get('', async (req, res) => {
  res.json({
    discount: await discount.getDiscount(req.project)
  });
});

module.exports = router;
