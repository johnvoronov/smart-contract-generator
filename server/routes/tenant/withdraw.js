const express = require('express');
const router = express.Router();
const withdrawService = require('../../service/withdraw');
const models = require('../../models');
// const settings = require('../util/settings');
const { TRANSACTION_WITHDRAW } = require('../../consts');

/**
 * Вывод средств на кошелек пользователя.
 *
 * Нам нужен точный eth адрес который хранится
 * у юзверя в профиле в eth_address и его sha256 хеш на основе ID для
 * передачи данных на вывод
 *
 * @param req
 * @param res
 */
router.post('', async (req, res) => {
  const user = await models.User.findById(req.user.id);

  if (!user.eth_address) {
    return res.boom.badRequest(null, {
      errors: {
        eth_address: [
          req.t('controller.withdraw.withdraw.invalid_eth_address')
        ]
      }
    });
  }

  const isAvailable = await withdrawService.isAvailable();
  if (false === isAvailable) {
    // withdraw недоступен
    return res.boom.badRequest(null, {
      errors: {
        eth_address: [
          req.t('controller.withdraw.withdraw.withdraw_unavailable')
        ]
      }
    });
  }

  const amount = await models.Transaction.sum('amount', {
    where: { user_id: user.id }
  });

  const params = {}; // TODO await settings.fetch(false);

  // Пользователю нечего выводить или сумма на балансе пользователя
  // запрашиваемая на вывод меньше, чем минимально допустимая
  // сумма для вывода.
  //
  // Значение к выводу эквивалентно значению к минимальной
  // сумме покупки
  if (
    ((amount || 0) <= 0) ||
    ((amount || 0) < parseFloat(params.minimal_withdraw || 0))
  ) {
    return res.boom.badRequest(null, {
      errors: {
        eth_address: [
          req.t('controller.withdraw.withdraw.insufficient_funds')
        ]
      }
    });
  }

  const tx = await models.Transaction.create({
    user_id: req.user.id,
    project_id: req.project.id,
    status: TRANSACTION_WITHDRAW,
    amount: -amount
  });

  res.status(201).json({ id: tx.id });
});

/**
 * Проверка доступности withdraw в
 * смарт контракте.
 *
 * @param req
 * @param res
 */
router.get('/available', async (req, res) => {
  res.json({
    is_available: await withdrawService.isAvailable()
  })
});

module.exports = router;
