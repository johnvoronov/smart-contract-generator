const express = require('express');
const router = express.Router();
const balance = require('../../service/balance');
const referral = require('../../service/referral');

router.get('', async (req, res) => {
  const balanceResp = await balance.getResponse({
    user_id: req.user.id
  }, req.project);

  res.json({
    ...balanceResp,
    referral_count: await referral.getReferralCount(req.user.id) // TODO
  });
});

module.exports = router;
