const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const models = require('../../models');
const { paymentStatus } = require('../../consts');

router.get('/user', auth, async (req, res) => {
  const tokens = await models.Transaction.sum('amount', {
    where: {
      user_id: req.user.id,
      status: paymentStatus.TRANSACTION_PAYMENT
    }
  }) || 0;

  const bonusTokens = await models.Transaction.sum('amount', {
    where: {
      user_id: req.user.id,
      status: paymentStatus.TRANSACTION_BONUS
    }
  }) || 0;

  const referralTokens = await models.Transaction.sum('amount', {
    where: {
      user_id: req.user.id,
      status: paymentStatus.TRANSACTION_REFERRAL
    }
  }) || 0;

  const referralsCount = await models.User.count({
    where: { referral_id: req.user.id }
  }) || 0;

  res.json({
    tokens,
    bonus: bonusTokens,
    referral: {
      amount: referralTokens,
      count: referralsCount
    }
  });
});

module.exports = router;
