const router = require('express').Router();
const models = require('../../../models');
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const jwt = require('../../../util/jwt');
const Op = models.sequelize.Op;
const {
  TRANSACTION_PAYMENT,
  TRANSACTION_BONUS
} = require('../../../consts');

const getTokensAmount = async where => {
  return await models.Transaction.sum('amount', {
    where,
    status: TRANSACTION_PAYMENT
  }) || 0;
};

const getStats = async (user_id, project_id) => ({
  tokens_total: await getTokensAmount({
    user_id,
    project_id
  }),
  tokens_amount: await getTokensAmount({
    user_id,
    project_id,
    status: TRANSACTION_PAYMENT
  }),
  tokens_bonus: await getTokensAmount({
    user_id,
    project_id,
    status: TRANSACTION_BONUS
  })
});

router.get(
  '/options',
  async (req, res) => {
    const rows = await models.User.findAll({
      where: {
        project_id: req.project.id
      },
      attributes: ['id', 'email'],
      order: [
        ['email', 'ASC']
      ]
    });

    res.json(rows);
  });

router.get(
  '/list',
  validation({
    query: {
      q: Joi.string().allow('', null),
      order: Joi.string().allow('', null),
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    let where = {
      project_id: req.project.id,
      is_superuser: false
    };
    if (req.query.q) {
      where = {
        email: { [Op.like]: `%${req.query.q}%` }
      };
    }

    let order = [];
    switch (req.query.order) {
      case 'email':
        order.push(['email', 'ASC']);
        break;

      case '-email':
        order.push(['email', 'DESC']);
        break;

      case 'is_admin':
        order.push(['is_admin', 'ASC']);
        break;

      case '-is_admin':
        order.push(['is_admin', 'DESC']);
        break;

      case 'is_active':
        order.push(['is_active', 'ASC']);
        break;

      case '-is_active':
        order.push(['is_active', 'DESC']);
        break;

      case 'id':
        order.push(['id', 'ASC']);
        break;

      case '-id':
        order.push(['id', 'DESC']);
        break;

      case 'created_at':
        order.push(['created_at', 'ASC']);
        break;

      case '-created_at':
        order.push(['created_at', 'DESC']);
        break;

      default:
        break;
    }

    const result = await models.User.findAndCountAll({
      where,
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order,
      attributes: jwt.getPublicAttributes()
    });

    const objects = [];
    for (let i = 0; i < result.rows.length; i++) {
      const user = result.rows[i];

      objects.push({
        ...jwt.getPublicFields(user),
        stats: await getStats(user.id, req.project.id)
      });
    }

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects
    });
  });

router.get(
  '/:id',
  validation({
    params: {
      id: Joi.number().required()
    }
  }),
  async (req, res) => {
    const user = await models.User.findOne({
      where: {
        id: req.params.id,
        project_id: req.project.id
      }
    });

    if (!user) {
      return res.boom.notFound();
    }

    const publicUser = jwt.getPublicFields(user);

    res.json({
      ...publicUser,
      stats: await getStats(user.id)
    });
  });

router.post(
  '/admin/:id',
  validation({
    params: {
      id: Joi.number().required()
    },
    body: {
      is_admin: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const user = await models.User.findOne({
      where: {
        id: req.params.id,
        project_id: req.project.id
      }
    });

    if (!user) {
      return res.boom.notFound();
    }

    await user.update({
      is_admin: req.body.is_admin
    });

    res.status(204).end();
  });

module.exports = router;
