const router = require('express').Router();
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const dashboardStats = require('../../../service/dashboard/dashboard');

router.get(
  '',
  validation({
    query: {
      interval: Joi.string().default('today').allow([
        'today',
        'yesterday',
        'month',
        'year',
        'quarter',
        'week'
      ])
    }
  }),
  async (req, res) => {
    res.json(await dashboardStats.getStats(req.query.interval));
  });

module.exports = router;
