const router = require('express').Router();
const models = require('../../../models');
const paymentStats = require('../../../service/payment/stats');
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const jwt = require('../../../util/jwt');
const mail = require('../../../service/mail');
const wallet = require('../../../service/wallet');
const referral = require('../../../service/referral');
const calculator = require('../../../util/calculator');
const Op = models.sequelize.Op;
const { transactionStatus, paymentStatus } = require('../../../consts');

const include = [
  {
    model: models.Currency,
    as: 'currency'
  },
  {
    model: models.Wallet,
    as: 'wallet'
  },
  {
    model: models.User,
    as: 'user',
    attributes: jwt.getPublicAttributes()
  }
];

router.get(
  '/list',
  validation({
    query: {
      q: Joi.string().allow('', null),
      user_id: Joi.number().default(null),
      order: Joi.string().allow('', null),
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    let where = {
      project_id: req.project.id,
    };

    if (req.query.q) {
      where = {
        [Op.or]: [
          {
            amount: { [Op.like]: `%${req.query.q}%` }
          },
          {
            tokens_amount: { [Op.like]: `%${req.query.q}%` }
          }
        ]
      };
    }

    if (req.query.user_id) {
      where = {
        ...where,
        user_id: req.query.user_id
      };
    }

    let order = [];
    switch (req.query.order) {
      case 'amount':
        order.push(['tokens_amount', 'ASC']);
        break;
      case '-amount':
        order.push(['tokens_amount', 'DESC']);
        break;

      case 'tokens_amount':
        order.push(['tokens_amount', 'ASC']);
        break;
      case '-tokens_amount':
        order.push(['tokens_amount', 'DESC']);
        break;

      case 'currency_id':
        order.push(['currency_id', 'ASC']);
        break;
      case '-currency_id':
        order.push(['currency_id', 'DESC']);
        break;

      case 'user_id':
        order.push(['user_id', 'ASC']);
        break;
      case '-user_id':
        order.push(['user_id', 'DESC']);
        break;

      case 'created_at':
        order.push(['created_at', 'ASC']);
        break;

      case '-created_at':
      default:
        order.push(['created_at', 'DESC']);
        break;
    }

    const result = await models.Payment.findAndCountAll({
      where,
      include,
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order
    });

    res.json({
      stats: await paymentStats.getStats(),
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  });

router.get(
  '/:id',
  validation({
    params: {
      id: Joi.number().required()
    }
  }),
  async (req, res) => {
    const payment = await models.Payment.findOne({
      id: req.params.id,
      project_id: req.project.id
    }, {
      include
    });

    if (!payment) {
      return res.boom.notFound();
    }

    res.json(payment);
  });

router.post(
  '/create',
  validation({
    body: {
      user_id: Joi.number().integer().required(),
      currency_id: Joi.number().integer().required(),
      token_price: Joi.number().required(),
      currency_amount: Joi.number().required(),
      tokens_amount: Joi.number().required(),
      discount: Joi.number().required(),
      calculate_referral_bonus: Joi.number().integer().default(0)
    }
  }),
  async (req, res) => {

    // Находим пользователя
    const user = await models.User.findOne({
      id: req.body.user_id,
      project_id: req.project.id
    });
    if (!user) {
      return res.boom.badRequest();
    }

    // Находим валюту
    const currency = await models.Currency.findById(req.body.currency_id);
    if (!currency) {
      return res.boom.badRequest();
    }

    // Находим или создаем кошелек пользователя
    // так как он является обязательной связью у платежа
    const userWallet = await wallet.getWallet(user, currency);

    const payment = await models.Payment.create({
      project_id: req.project.id,
      // Сохраняем текущий курс
      to_eth: currency.to_eth,
      to_usd: currency.to_usd,
      currency_id: currency.id,
      user_id: user.id,
      // Сохраняем процент скидки по которому было зачисление
      discount: req.body.discount,
      wallet_id: userWallet.id,
      // Сохраняем количество криптовалюты полученное с frontend
      amount: req.body.currency_amount,
      tokens_amount: req.body.tokens_amount,
      token_price: req.body.token_price,
      // Отмечаем что это прямой платеж в обход ЛК и coinpayments
      is_direct: true,
      // Сразу отмечаем платеж как успешно завершенный
      status: paymentStatus.PAYMENT_COMPLETE
    });

    const tx = await models.Transaction.create({
      project_id: req.project.id,
      user_id: user.id,
      payment_id: payment.id,
      amount: payment.tokens_amount
    });

    mail.send(user.email, 'payment_success', {
      payment,
      wallet,
      user,
      transaction: tx
    });

    // Если сумма скидки выбранная пользователем при
    // прямом платеже больше нуля, то в данном кейсе мы
    // используем именно эту сумму скидки в обход discount.getDiscount()
    if (req.body.discount > 0) {
      const bonus = calculator.getBonusFor(
        payment.tokens_amount,
        req.body.discount
      );

      // Пропускаем случаи когда кол-во бонусов = 0
      if (bonus > 0) {
        const txBonus = await models.Transaction.create({
          project_id: req.project.id,
          user_id: user.id,
          payment_id: payment.id,
          status: transactionStatus.TRANSACTION_PAYMENT,
          amount: bonus
        });

        // Отправляем пользователю уведомление о зачислении
        // платежа
        mail.send(user.email, 'payment_bonus', {
          payment,
          wallet,
          user,
          transaction: txBonus
        });
      }
    }

    // Если пользователь выбрал зачислять реферальные бонусы
    // при прямом платеже
    if (req.body.calculate_referral_bonus) {

      // Зачисляем реферальные бонусы
      const referralTransactions = await referral.createTransaction(payment);

      // Если бонусные транзакции были созданы, то
      // отправляем пользователям уведомления о
      // поступившем платеже
      if (referralTransactions.length > 0) {
        for (let t = 0; t < referralTransactions.length; t++) {
          const txReferral = referralTransactions[t];

          mail.send(user.email, 'payment_referral', {
            referral: txReferral
          });
        }
      }
    }

    res.status(201).json({
      id: payment.id
    });
  });

module.exports = router;
