const router = require('express').Router();
const nodemailer = require('nodemailer');
const models = require('../../../models');
const validation = require('../../../middleware/validation');
const filterValues = require('../../../util/filterValues');
const mail = require('../../../util/mail');
const Joi = require('joi');

router.get('', async (req, res) => {
  res.json({
    coinpayments: req.project.coinpayments || {},
    settings: req.project.settings || {}
  });
});

router.post('/coinpayments', validation({
  body: {
    // Coinpayments
    public_key: Joi.string().default(null).allow([null, '']),
    secret_key: Joi.string().default(null).allow([null, '']),
    ipn_secret: Joi.string().default(null).allow([null, '']),
    merchant_id: Joi.string().default(null).allow([null, ''])
  }
}), async (req, res) => {
  await req.project.update({
    coinpayments: filterValues({ ...req.project.coinpayments, ...req.body })
  });

  res.status(204).end();
});

router.post('/smtp', validation({
  body: {
    // SMTP settings
    from: Joi.string().email().required(),
    host: Joi.string().required(),
    port: Joi.number().integer().required(),
    username: Joi.string().required(),
    password: Joi.string().required()
  }
}), async (req, res) => {
  const settings = req.project.settings || {};
  const smtp = {
    ...(settings.smtp || {}),
    ...filterValues(req.body)
  };

  const transport = nodemailer.createTransport({
    host: smtp.host,
    port: smtp.port,
    secure: true,
    auth: {
      user: smtp.username,
      pass: smtp.password
    }
  });

  try {
    await transport.verify();

    await req.project.update({
      settings: { ...settings, smtp }
    });

    res.status(204).end();
  } catch (e) {
    return res.boom.badRequest(null, {
      errors: {
        host: [
          req.t('smtp.incorrect_settings') + ' ' + e.toString()
        ]
      }
    })
  }
});

module.exports = router;
