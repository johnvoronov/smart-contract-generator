const router = require('express').Router();
const models = require('../../../models');
const Joi = require('joi');
const validation = require('../../../middleware/validation');

const fluidBoolean = Joi.boolean() // Жидкий бульон
  .truthy('yes')
  .truthy('y')
  .truthy('1')
  .truthy(1)
  .falsy('no')
  .falsy('n')
  .falsy('0')
  .falsy(0)
  .insensitive(true);

router.get(
  '/list',
  validation({
    query: {
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    const result = await models.Discount.findAndCountAll({
      where: {
        project_id: req.project.id
      },
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order
    });

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  });

router.post(
  '',
  validation({
    body: {
      intervals: Joi.array().items(Joi.object({
        from: Joi.date().iso().required(),
        to: Joi.date().iso().min(Joi.ref('from')).required(),
        value: Joi.number().required(),
        is_fixed: fluidBoolean.default(false)
      }))
    }
  }),
  async (req, res) => {
    for (let i = 0; i < req.body.intervals.length; i++) {
      const interval = req.body.intervals[i];

      await models.Discount.create({
        ...interval,
        project_id: req.project.id
      });
    }

    res.status(204).end();
  });

module.exports = router;
