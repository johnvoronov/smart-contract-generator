const router = require('express').Router();
const models = require('../../../models');
const transactionStats = require('../../../service/transaction/stats');
const jwt = require('../../../util/jwt');
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const Op = models.sequelize.Op;

const include = [
  {
    model: models.Payment,
    as: 'payment',
    include: [
      {
        model: models.Currency,
        as: 'currency'
      }
    ]
  },
  {
    model: models.Eth,
    as: 'eth'
  },
  {
    model: models.User,
    as: 'user',
    attributes: jwt.getPublicAttributes()
  }
];

router.get(
  '/list',
  validation({
    query: {
      q: Joi.string().allow('', null),
      user_id: Joi.number().default(null),
      order: Joi.string().allow('', null),
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    let where = {
      project_id: req.project.id
    };

    if (req.query.q) {
      where = {
        amount: { [Op.like]: `%${req.query.q}` }
      };
    }

    if (req.query.user_id) {
      where = {
        ...where,
        user_id: req.query.user_id
      };
    }

    let order = [];
    switch (req.query.order) {
      case 'comment':
        order.push(['comment', 'ASC']);
        break;

      case '-comment':
        order.push(['comment', 'DESC']);
        break;

      case 'amount':
        order.push(['amount', 'ASC']);
        break;

      case '-amount':
        order.push(['amount', 'DESC']);
        break;

      case 'user_id':
        order.push(['user_id', 'ASC']);
        break;

      case '-user_id':
        order.push(['user_id', 'DESC']);
        break;

      case 'id':
        order.push(['id', 'ASC']);
        break;

      case '-id':
        order.push(['id', 'DESC']);
        break;

      case 'created_at':
        order.push(['created_at', 'ASC']);
        break;

      case '-created_at':
      default:
        order.push(['created_at', 'DESC']);
        break;
    }

    const result = await models.Transaction.findAndCountAll({
      where,
      include,
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order
    });

    res.json({
      stats: await transactionStats.getStats(),
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  });

router.get(
  '/:id',
  validation({
    params: {
      id: Joi.number().required()
    }
  }),
  async (req, res) => {
    const transaction = await models.Transaction.findById(req.params.id, {
      include
    });

    if (!transaction) {
      return res.boom.notFound();
    }

    res.json(transaction);
  });

module.exports = router;
