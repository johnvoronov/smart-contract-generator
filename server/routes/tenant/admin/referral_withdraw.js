const router = require('express').Router();
const models = require('../../../models');
const jwt = require('../../../util/jwt');
const mail = require('../../../service/mail');
const Joi = require('joi');
const validation = require('../../../middleware/validation');

const include = [
  {
    model: models.User,
    as: 'user',
    attributes: jwt.getPublicAttributes()
  }
];

router.get(
  '/list',
  validation({
    query: {
      page_size: Joi.number().integer().min(1).max(300).default(100),
      page: Joi.number().integer().min(1).default(1)
    }
  }),
  async (req, res) => {
    const result = await models.ReferralWithdraw.findAndCountAll({
      include,
      limit: req.query.page_size,
      offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
      order: [
        ['is_done', 'ASC']
      ]
    });

    res.json({
      meta: {
        page: req.query.page,
        page_size: req.query.page_size,
        page_count: Math.ceil(result.count / req.query.page_size),
        total: result.count
      },
      objects: result.rows
    });
  });

router.post(
  '/done/:id',
  validation({
    params: {
      id: Joi.number().required()
    }
  }),
  async (req, res) => {
    const referral = await models.ReferralWithdraw.findById(req.params.id);

    if (!referral) {
      return res.boom.notFound();
    }

    await referral.update({ is_done: true });

    mail.send(referral.user.email, 'referral_withdraw', {
      referral
    });

    res.json(referral);
  });

module.exports = router;
