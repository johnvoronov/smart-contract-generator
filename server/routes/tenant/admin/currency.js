const router = require('express').Router();
const models = require('../../../models');
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const Op = models.sequelize.Op;

router.get('', validation({
  query: {
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  let where = {};
  if (req.query.q) {
    where = {
      [Op.or]: [
        {
          code: { [Op.like]: `%${req.query.q}%` }
        },
        {
          name: { [Op.like]: `%${req.query.q}%` }
        }
      ]
    };
  }

  let order = [];
  switch (req.query.order) {
    case 'code':
      order.push(['code', 'ASC']);
      break;

    case '-code':
      order.push(['code', 'DESC']);
      break;

    case 'name':
      order.push(['name', 'ASC']);
      break;

    case '-name':
      order.push(['name', 'DESC']);
      break;

    case 'to_usd':
      order.push(['to_usd', 'ASC']);
      break;

    case '-to_usd':
      order.push(['to_usd', 'DESC']);
      break;

    case 'to_eth':
      order.push(['to_eth', 'ASC']);
      break;

    case '-to_eth':
      order.push(['to_eth', 'DESC']);
      break;

    case 'updated_at':
      order.push(['updated_at', 'ASC']);
      break;

    case '-updated_at':
      order.push(['updated_at', 'DESC']);
      break;

    case 'created_at':
      order.push(['created_at', 'ASC']);
      break;

    case '-created_at':
      order.push(['created_at', 'DESC']);
      break;

    case 'is_enabled':
      order.push(['is_enabled', 'ASC']);
      break;

    case '-is_enabled':
      order.push(['is_enabled', 'DESC']);
      break;

    default:
      order.push(['is_enabled', 'DESC']);
      order.push(['code', 'ASC']);
      break;
  }

  const result = await models.Currency.findAndCountAll({
    where,
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

router.get('/options', async (req, res) => {
  const rows = await models.Currency.findAll({
    attributes: ['id', 'code', 'name', 'to_eth', 'to_usd'],
    order: [
      ['name', 'ASC']
    ]
  });

  res.json(rows);
});

router.post('/toggle', validation({
  body: {
    id: Joi.number().required()
  }
}), async (req, res) => {
  const currency = await models.Currency.findById(req.body.id);

  if (!currency) {
    return res.boom.notFound();
  }

  await currency.update({
    is_enabled: !currency.is_enabled
  });
  res.status(204).end();
});

module.exports = router;
