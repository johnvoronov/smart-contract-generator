const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');
const filterValues = require('../../../util/filterValues');
const models = require('../../../models');
const validation = require('../../../middleware/validation');
const Joi = require('joi');

router.get(
  '',
  auth,
  async (req, res) => {
    res.json({
      objects: await models.Referral.findAll()
    });
  }
);

router.post(
  '',
  auth,
  validation({
    body: {
      name: Joi.string().required(),
      user: Joi.number().positive().required(),
      team: Joi.number().positive().required(),
      levels: Joi.array().items(
        Joi.number().positive()
      ).required()
    }
  }),
  async (req, res) => {
    const referral = await models.Referral.create(filterValues(req.body));

    res.status(201).json({
      id: referral.id
    });
  }
);

router.post(
  '/:id',
  auth,
  validation({
    params: {
      id: Joi.number().required()
    },
    body: {
      name: Joi.string().allow('', null),
      user: Joi.number().positive().allow('', null),
      team: Joi.number().positive().allow('', null),
      levels: Joi.array().items(
        Joi.number().positive()
      ).allow('', null, []),
    }
  }),
  async (req, res) => {
    const referral = await models.Referral.findById(req.params.id);

    if (null === referral) {
      return res.boom.notFound();
    }

    await referral.update(filterValues(req.body));

    res.status(204).end();
  }
);

router.delete(
  '/:id',
  auth,
  validation({
    params: {
      id: Joi.number().required()
    }
  }),
  async (req, res) => {
    const referral = await models.Referral.findById(req.params.id);

    if (null === referral) {
      return res.boom.notFound();
    }

    await referral.destroy();

    res.status(204).end();
  }
);

module.exports = router;
