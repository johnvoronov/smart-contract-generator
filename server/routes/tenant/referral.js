const express = require('express');
const router = express.Router();
const referral = require('../../service/referral');
const auth = require('../../middleware/auth');
const models = require('../../models');
const validation = require('../../middleware/validation');
const Joi = require('joi');

/**
 * Вывод баланса по реферальной системе в ETH
 *
 * @param req
 * @param res
 */
router.get('/status', auth, async (req, res) => {
  res.json(await referral.getStats(req.user));
});

/**
 * Вывод пользователей по уровню реферальной системы
 *
 * @param req
 * @param res
 */
router.get('/users', auth, validation({
  query: {
    id: Joi.number().integer().default(null)
  }
}), async (req, res) => {
  const {
    id
  } = req.query;

  const rows = await models.User.findAll({
    where: { referral_id: id || req.user.id },
    attributes: [
      'id',
      'email',
      'is_active',
      'parents'
    ]
  });

  const result = [];
  for (let i = 0; i < rows.length; i++) {
    const item = rows[i];
    const stats = await referral.getStats(rows[i]);

    result.push({
      ...item.toJSON(),
      ...stats
    });
  }

  res.json(result);
});

router.get('/balance', auth, async (req, res) => {
  const balance = await models.Referral.sum('amount', {
    where: { user_id: req.user.id }
  }) || 0;

  res.json({ balance });
});

module.exports = router;
