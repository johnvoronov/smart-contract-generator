const express = require('express');
const router = express.Router();
const customerAuth = require('../../middleware/customerAuth');

router.use('/settings', customerAuth, require('./admin/settings'));
router.use('/dashboard', customerAuth, require('./admin/dashboard'));
router.use('/currency', customerAuth, require('./admin/currency'));
router.use('/user', customerAuth, require('./admin/user'));
router.use('/referral', customerAuth, require('./admin/referral'));
router.use('/transaction', customerAuth, require('./admin/transaction'));
router.use('/discount', customerAuth, require('./admin/discount'));
router.use('/referral_withdraw', customerAuth, require('./admin/referral_withdraw'));
router.use('/payment', customerAuth, require('./admin/payment'));

module.exports = router;
