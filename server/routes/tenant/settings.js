const express = require('express');
const router = express.Router();

router.get('', async (req, res) => {
  res.json({
    ...req.project.settings,
    blockchain: req.project.blockchain.code,
    token_decimals: req.project.parameters.decimals,
    token_name: req.project.parameters.name,
    token_symbol: req.project.parameters.symbol,
    token_price: 1 / req.project.parameters.rate,
  });
});

module.exports = router;
