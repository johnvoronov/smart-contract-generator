const express = require('express');
const router = express.Router();
const models = require('../../models');
const { Op } = require('sequelize');

router.get('', async (req, res) => {
  const throughCurrencies = await models.ProjectCurrency.findAll({
    where: { project_id: req.project.id }
  });
  const ids = [];
  for (let i = 0; i < throughCurrencies.length; i++) {
    ids.push(throughCurrencies[i].currency_id);
  }

  const rows = await models.Currency.findAll({
    where: {
      id: {
        [Op.in]: ids
      }
    }
  });

  res.json(rows);
});

module.exports = router;
