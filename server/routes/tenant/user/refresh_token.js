const express = require('express');
const Joi = require('joi');
const validation = require('../../../middleware/validation');
const models = require('../../../models');
const jwt = require('../../../util/jwt');
const router = express.Router();

router.post(
  '/refresh_token',
  validation({
    body: {
      refresh_token: Joi.string().required().label('validation.refresh_token')
    }
  }),
  async (req, res) => {
    const user = await models.User.findOne({
      where: {
        project_id: req.project.id,
        refresh_token: req.body.refresh_token
      }
    });

    if (!user) {
      return res.boom.notFound(null, {
        error: {
          refresh_token: [
            'invalid refresh token or user not found'
          ]
        }
      });
    }

    return res.json(jwt.response(user));
  });

module.exports = router;
