const express = require('express');
const QRCode = require('qrcode');
const Joi = require('joi');
const speakeasy = require('speakeasy');
const validation = require('../../../middleware/validation');
const models = require('../../../models');
const router = express.Router();

router.get(
  '',
  /**
   * Отображение пользователю изображения с QR кодом
   * для активации 2fa авторизации
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const user = await models.User.findOne({
      where: {
        id: req.user.id,
        project_id: req.project.id
      }
    });
    const url = `otpauth://totp/SecretKey?secret=${user.two_factor_secret}`;

    res.json({
      qr: await QRCode.toDataURL(url)
    });
  });

router.post(
  '',
  validation({
    body: {
      token: Joi.string().required().label('validation.token')
    }
  }),
  /**
   * Включение пользователю 2fa авторизации с подтверждением
   *
   * @param req
   * @param res
   * @returns {*}
   */
  async (req, res) => {
    const { token } = req.body;

    const user = await models.User.findOne({
      where: {
        id: req.user.id,
        project_id: req.project.id
      }
    });

    const verified = process.env.NODE_ENV === 'test' ? true : speakeasy.totp.verify({
      secret: user.two_factor_secret,
      encoding: 'base32',
      token: token
    });

    if (!verified) {
      return res.boom.badRequest('invalid query', {
        errors: {
          token: ['controller.invalid_token']
        }
      });
    }

    await user.update({ is_two_factor: true });

    req.app.emit('user.two_factor.enable', req, res, user);

    res.status(204).end();
  }
);

router.delete(
  '',
  /**
   * Отключение 2fa авторизации без подтверждения
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const user = await models.User.findOne({
      where: {
        id: req.user.id,
        project_id: req.project.id
      }
    });

    await user.update({
      is_two_factor: false
    });

    req.app.emit('user.two_factor.disable', req, res, user);

    res.status(204).end();
  }
);

module.exports = router;
