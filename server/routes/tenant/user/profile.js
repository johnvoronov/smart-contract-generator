const express = require('express');
const Joi = require('joi');
const router = express.Router();
const validation = require('../../../middleware/validation');
const models = require('../../../models');
const passwordUtil = require('../../../util/password');

router.post(
  '/eth_address',
  validation({
    body: {
      eth_address: Joi.string().required().label('validation.eth_address')
    }
  }),
  /**
   * Сохранение кошелька eth для текущего пользователя
   * @param req
   * @param res
   */
  async (req, res) => {
    const where = {
      id: req.user.id,
      project_id: req.project.id
    };

    await models.User.update({
      eth_address: req.body.eth_address
    }, { where });

    res.status(204).end();
  });

router.post(
  '/change_password',
  validation({
    body: {
      password_current: Joi.string().required().min(6).max(50).label('validation.password_current'),
      password: Joi.string().required().min(6).max(50).label('validation.password'),
      password_confirm: Joi.string().required().min(6).max(50).valid(Joi.ref('password')).options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      }).label('validation.password_confirm')
    }
  }),
  /**
   * Изменение пароля текущего пользователя
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      password_current,
      password,
      password_confirm
    } = req.body;

    // Не обязательная проверка, так как факт существования пользователя
    // с данным ID проверяется в auth middleware
    const user = await models.User.findOne({
      where: {
        id: req.user.id,
        project_id: req.project.id
      }
    });

    const valid = await passwordUtil.compare(password_current, user.password);
    if (!valid) {
      return res.boom.badRequest(null, {
        errors: {
          password_current: [
            req.t('controller.user.change_password.invalid_password')
          ]
        }
      });
    }

    if (password !== password_confirm) {
      return res.boom.badRequest(null, {
        errors: {
          password_confirm: [
            req.t('controller.user.password_not_match')
          ]
        }
      });
    }

    await user.update({
      password: await passwordUtil.hash(password)
    });

    res.status(204).send();
  });

module.exports = router;
