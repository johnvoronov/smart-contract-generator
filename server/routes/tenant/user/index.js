const express = require('express');
const router = express.Router();
const auth = require('../../../middleware/auth');

router.use('/login', require('./login'));
router.use('/registration', require('./registration'));
router.use('/restore', require('./restore'));

router.use('/profile', auth, require('./profile'));
router.use('/2fa', auth, require('./two_factor'));

module.exports = router;
