const express = require('express');
const router = express.Router();
const validation = require('../../middleware/validation');
const Joi = require('joi');
const request = require('request');
const jwt = require('../../util/jwt');
const socialConfig = require('../../config/social');
const models = require('../../models');
const passport = require('../../util/passport');

const defaultOptions = {
  session: false
};

const done = async (req, res) => {
  if (!req.user) {
    return res.boom.unauthorized('User Not Authenticated');
  }

  const user = await models.User.findById(req.user.id);
  if (null === user) {
    return res.boom.badImplementation(null, {
      error: new Error('User not found')
    });
  }

  return res.json(jwt.response(user));
};

router.post('/twitter/reverse', async (req, res) => {
  request.post({
    url: 'https://api.twitter.com/oauth/request_token',
    oauth: {
      consumer_key: socialConfig.twitter.id,
      consumer_secret: socialConfig.twitter.secret
    }
  }, (error, r, body) => {
    if (error) {
      return res.boom.badImplementation(null, { error });
    }

    const jsonStr = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';

    return res.json(JSON.parse(jsonStr));
  });
}, async (req, res) => {
  return passport.getPassport().authenticate('twitter-token', defaultOptions)(req, res);
}, done);

router.post('/twitter', validation({
  body: {
    oauth_token: Joi.string().required(),
    oauth_verifier: Joi.string().required()
  }
}), async (req, res, next) => {
  request.post({
    url: 'https://api.twitter.com/oauth/access_token?oauth_verifier',
    oauth: {
      consumer_key: socialConfig.twitter.id,
      consumer_secret: socialConfig.twitter.secret,
      token: req.body.oauth_token
    },
    form: {
      oauth_verifier: req.body.oauth_verifier
    }
  }, (error, r, body) => {
    if (error) {
      return res.boom.badImplementation(null, { error });
    }

    try {
      const bodyString = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
      const parsedBody = JSON.parse(bodyString);

      req.body = {
        ...req.body,
        oauth_token: parsedBody.oauth_token,
        oauth_token_secret: parsedBody.oauth_token_secret,
        user_id: parsedBody.user_id
      };

      next();
    } catch (error) {
      return res.boom.badImplementation(null, { error });
    }
  });
}, async (req, res) => {
  return passport.getPassport().authenticate('twitter-token', defaultOptions)(req, res);
}, done);

router.post('/facebook', async (req, res) => {
  return passport.getPassport().authenticate('facebook-token', defaultOptions)(req, res);
}, done);

router.post('/google', async (req, res) => {
  return passport.getPassport().authenticate('google-token', defaultOptions)(req, res);
}, done);

module.exports = router;
