const express = require('express');
const router = express.Router();
const models = require('../../models');
const withdraw = require('../../service/withdraw');
// const settings = require('../util/settings');
const discount = require('../../service/discount');
const balance = require('../../service/balance');
const stats = require('../../service/stats');

router.get(
  '',
  /**
   * Вывод всех данных ЛК в один запрос
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    return res.json({
      settings: {}, // TODO await settings.fetch(),
      balance: await balance.getResponse({
        user_id: req.user.id
      }, req.project),
      blockchain: {
        raised: await stats.getTotalRaised(req.project),
        discount: await discount.getDiscount(req.project),
        intervals: [], // TODO discount.getIntervals(req.project),
        next_bonus_date: discount.getNextDate(req.project)
      },
      withdraw: {
        is_available: await withdraw.isAvailable()
      },
      currencies: await models.Currency.findAll({
        where: { is_enabled: true }
      })
    });
  }
);

module.exports = router;
