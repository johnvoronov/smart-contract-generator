const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validation = require('../../middleware/validation');
const models = require('../../models');

const include = [
  {
    model: models.Payment, as: 'payment',
    include: [
      {
        model: models.Currency,
        as: 'currency'
      }
    ]
  },
  {
    model: models.Eth,
    as: 'eth'
  }
];

router.get('/list', validation({
  query: {
    page_size: Joi.number().integer().min(1).max(300).default(100),
    page: Joi.number().integer().min(1).default(1)
  }
}), async (req, res) => {
  const result = await models.Transaction.findAndCountAll({
    where: {
      project_id: req.project.id,
      user_id: req.user.id
    },
    include,
    limit: req.query.page_size,
    offset: req.query.page > 1 ? (req.query.page - 1) * req.query.page_size : 0,
    order: [
      ['id', 'DESC']
    ]
  });

  res.json({
    meta: {
      page: req.query.page,
      page_size: req.query.page_size,
      page_count: Math.ceil(result.count / req.query.page_size),
      total: result.count
    },
    objects: result.rows
  });
});

module.exports = router;
