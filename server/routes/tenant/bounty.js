const express = require('express');
const Joi = require('joi');
const router = express.Router();
const models = require('../../models');
const validation = require('../../middleware/validation');
const guard = require('express-jwt-permissions')();
const { TRANSACTION_BOUNTY } = require('../../consts');

router.get(
  '',
  async (req, res) => {
    const rows = await models.Bounty.findAll();

    res.json(rows);
  });

router.post(
  '/accept',
  validation({
    body: {
      name: Joi.string().required()
    }
  }),
  async (req, res) => {
    const {
      name
    } = req.body;

    const bounty = await models.Bounty.findOne({
      where: { name }
    });

    if (null === bounty) {
      return res.boom.notFound();
    }

    const count = await models.Transaction.count({
      where: { bounty_id: bounty.id, user_id: req.user.id }
    });
    if (count > 0) {
      return res.boom.badRequest(null, {
        errors: {
          name: [
            req.t('bounty.multiple_event_declaration')
          ]
        }
      });
    }

    const transaction = await models.Transaction.create({
      amount: bounty.amount,
      user_id: req.user.id,
      bounty_id: bounty.id,
      status: TRANSACTION_BOUNTY
    }); // TODO send email notification?

    res.json({
      amount: bounty.amount
    });
  });

router.get(
  '/:id',
  validation({
    params: {
      id: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const bounty = await models.Bounty.findById(req.params.id);

    if (null === bounty) {
      return res.boom.notFound();
    }

    res.json(bounty);
  });

router.post(
  '/:id',
  guard.check(['admin']),
  validation({
    params: {
      id: Joi.number().integer().required()
    },
    body: {
      amount: Joi.number().required()
    }
  }),
  async (req, res) => {
    const bounty = await models.Bounty.findById(req.params.id);

    if (null === bounty) {
      return res.boom.notFound();
    }

    await bounty.update({
      amount: req.body.amount
    });

    res.status(204).end();
  });

router.post(
  '/enable/:id',
  guard.check(['admin']),
  validation({
    params: {
      id: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const bounty = await models.Bounty.findById(req.params.id);

    if (null === bounty) {
      return res.boom.notFound();
    }

    await bounty.update({
      is_enabled: true
    });

    res.status(204).end();
  });

router.post(
  '/disable/:id',
  guard.check(['admin']),
  validation({
    params: {
      id: Joi.number().integer().required()
    }
  }),
  async (req, res) => {
    const bounty = await models.Bounty.findById(req.params.id);

    if (null === bounty) {
      return res.boom.notFound();
    }

    await bounty.update({
      is_enabled: false
    });

    res.status(204).end();
  });

module.exports = router;
