const express = require('express');
const router = express.Router();
const validation = require('../middleware/validation');
const ipnProject = require('../middleware/ipnProject');
const ipn = require('../middleware/ipn');
const customerAuth = require('../middleware/customerAuth');
const models = require('../models');
const filterValues = require('../util/filterValues');
const blockchain = require('../util/blockchain');
const mail = require('../service/mail');
const discount = require('../service/discount');
const systemWallet = require('../config/systemWallet');
const {
  projectStatus,
  ethStatus,
  paymentStatus,
  transactionStatus
} = require('../consts');
const Op = require('sequelize');
const Joi = require('joi');

router.post('', customerAuth, validation({
  body: {
    // Поддомен для проекта
    subdomain: Joi.string().required().label('Поддомен'),
    // Blockchain сеть
    blockchain: Joi.string().required().label('Блокчейн'),
    // Название токена (Bitcoin)
    name: Joi.string().max(255).required().label('Название'),
    // Код токена (BTC)
    symbol: Joi.string().max(255).required().label('Символ'),
    // Стоимость токена.
    rate: Joi.number().required().label('Стоимость токена'),
    // Тип токена ERC20 (mintable или burnable)
    type: Joi.string().allow('mintable', 'burnable').default('mintable').label('Тип токена'),
    // Кол-во токенов для burnable
    supply: Joi.alternatives().when('type', {
      is: 'burnable',
      then: Joi.number().integer().positive().required(),
      otherwise: Joi.number().integer().positive()
    }).default(0).label('Стартовое кол-во'),
    wallet: Joi.alternatives().when('type', {
      is: 'local',
      then: Joi.string(),
      otherwise: Joi.string().required()
    }).default(0).label('Кошелек'),
    // Дата начала ICO
    opening_time: Joi.date().iso().label('Дата начала'),
    // Дата завершения ICO
    closing_time: Joi.date().iso().label('Дата завершения'),
    // Разрядность токена
    decimals: Joi.number().integer().positive().default(18).max(18).label('Разрядность'),
    discount_token: Joi.array().items(Joi.object({
      discount: Joi.number().integer().required().label('Процент скидки'),
      tokens: Joi.number().integer().required().label('Кол-во токенов')
    })).label('Скидки по кол-ву токенов'),
    discount_time: Joi.array().items(Joi.object({
      discount: Joi.number().integer().required().label('Процент скидки'),
      time: Joi.date().iso().required().label('Время')
    })),
    // hardcap
    softcap: Joi.number().integer().positive().default(null).label('Softcap'),
    // hardcap
    hardcap: Joi.number().integer().positive().default(null).label('Hardcap'),
    // Настройки coinpayments
    coinpayments_public_key: Joi.string().required().label('Coinpayments public key'),
    coinpayments_secret_key: Joi.string().required().label('Coinpayments secret key'),
    coinpayments_ipn_secret: Joi.string().required().label('Coinpayments ipn secret'),
    coinpayments_merchant_id: Joi.string().required().label('Coinpayments merchant id')
  }
}), async (req, res) => {
  if (await models.Project.count({ where: { subdomain: req.body.subdomain } }) > 0) {
    return res.boom.badRequest(null, {
      errors: {
        subdomain: ['Поддомен уже занят']
      }
    });
  }

  const blockchain = await models.Blockchain.findOne({
    where: { code: req.body.blockchain }
  });
  if (null === blockchain) {
    return res.boom.notFound();
  }

  const parameters = {
    type: req.body.type,
    name: req.body.name,
    symbol: req.body.symbol,
    decimals: req.body.decimals,
    rate: req.body.rate,
    supply: req.body.supply,
    wallet: req.body.wallet
  };

  // Формируем parents

  const project = await models.Project.create(filterValues({
    ...req.body,
    parameters,
    credentials: systemWallet[blockchain.code] || {},
    blockchain_id: blockchain.id,
    status: blockchain.code === 'local' ? projectStatus.PROJECT_READY : projectStatus.PROJECT_DONE, // TODO
    customer_id: req.user.id,
    coinpayments: {
      public_key: req.body.coinpayments_public_key,
      secret_key: req.body.coinpayments_secret_key,
      ipn_secret: req.body.coinpayments_ipn_secret,
      merchant_id: req.body.coinpayments_merchant_id
    }
  }));

  const currencies = await models.Currency.findAll({
    where: { is_enabled: true }
  });
  for (let i = 0; i < currencies.length; i++) {
    await models.ProjectCurrency.create({
      project_id: project.id,
      currency_id: currencies[i].id
    });
  }

  res.status(201).json({
    id: project.id
  });
});

router.get('', customerAuth, async (req, res) => {
  const rows = await models.Project.findAll({
    where: {
      customer_id: req.user.id
    },
    include: [
      {
        model: models.Blockchain,
        as: 'blockchain'
      }
    ],
    order: [
      ['id', 'DESC']
    ]
  });

  res.json(rows);
});

router.get('/:id', customerAuth, validation({
  params: {
    id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const project = await models.Project.findOne({
    where: {
      customer_id: req.user.id,
      id: req.params.id
    },
    include: [
      {
        model: models.Blockchain,
        as: 'blockchain'
      }
    ]
  });
  if (null === project) {
    return res.boom.notFound();
  }

  res.json(project);
});

/**
 * Прием оплаты для проекта
 */
router.post('/:project_id/ipn', validation({
  params: {
    project_id: Joi.number().integer().positive().required(),
    currency_id: Joi.number().integer().positive().required()
  }
}), ipnProject, ipn, async (req, res) => {
  const status = parseInt(req.body.status, 10);

  const wallet = await models.ProjectWallet.findOne({
    where: { address: data.address }
  });
  if (null === wallet) {
    throw new Error('Wallet not found');
  }

  const user = await wallet.getUser();

  const payment = await models.Payment.findOne({
    where: {
      transaction_id: req.body.txn_id,
      user_id: wallet.user_id
    }
  });

  const emailData = { payment, wallet, user };

  if (status < 0) {
    await payment.update({
      status: paymentStatus.PAYMENT_ERROR
    });

    mail.send(user.email, 'system_payment_error', emailData);
  } else if (status < 100) {
    if (null === payment) {
      const currency = await wallet.getCurrency();

      const project = await wallet.getProject();
      const blockchain = await project.getBlockchain();
      const projectCurrency = blockchain.getCurrency();

      const rate = projectCurrency.id === currency.id ? 1 : 1 / currency.to_eth;

      const payment = await models.ProjectPayment.create({
        amount: data.amount * rate,
        to_eth: currency.to_eth,
        to_usd: currency.to_usd,
        currency_id: currency.id,
        wallet_id: wallet.id,
        user_id: wallet.user_id,
        transaction_id: data.txn_id,
        fee: data.fee,
        status: paymentStatus.PAYMENT_PENDING
      });

      mail.send(user.email, 'system_payment_pending', { ...emailData, payment });
    }
  } else if (status === 100) {
    await payment.update({
      status: paymentStatus.PAYMENT_COMPLETE
    });

    const tx = await models.Transaction.create({
      user_id: user.id,
      payment_id: payment.id,
      amount: payment.amount,
      status: transactionStatus.TRANSACTION_PAYMENT
    });

    mail.send(user.email, 'system_payment_success', { ...emailData, tx });
  }

  return res.boom.badRequest();
});

router.get('/:id/statistics', customerAuth, validation({
  params: {
    id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const project = await models.Project.findById(req.params.id);
  if (null === project) {
    return res.boom.notFound();
  }

  const raised = await models.Transaction.sum('amount', {
    where: {
      amount: {
        [Op.gte]: 0
      }
    },
    include: [
      {
        model: models.Eth,
        as: 'eth',
        where: {
          status: ethStatus.ETH_COMPLETE
        }
      }
    ],
    group: [
      'eth.id'
    ]
  }) || 0;

  res.json({
    raised,
    token: project.token,
    discount: {
      discount: await discount.getDiscount(project),
      next_date: await discount.getNextDate(project),
      intervals: project.discount
    },
    withdraw: await blockchain.withdrawAvailable(/* TODO project */)
  });
});

module.exports = router;
