const express = require('express');
const router = express.Router();
const validation = require('../middleware/validation');
const Joi = require('joi');
const mail = require('../util/mail');

router.get(
  '/mail',
  async (req, res) => {
    const {
      template
    } = req.query;

    const templates = [
      'user_login',
      'user_restore',
      'user_registration',
      'transaction_success',
      'transaction_error',
      'payment_success'
    ];

    if (templates.indexOf(template) === -1) {
      return res.boom.notFound();
    }

    let data = {};
    if (template === 'user_login') {
      data = {
        email: 'user@user.com',
        user_agent: 'Browser yo niga common chrome',
        ip: '127.0.0.1',
        date: (new Date()).toLocaleString()
      };
    } else if (template === 'user_registration') {
      data = {
        token: '123456'
      };
    } else if (template === 'user_restore') {
      data = {
        email: 'user@email.com',
        token: '123456'
      };
    } else if (template === 'transaction_success') {
      data = {
        hash: '0x123456'
      };
    } else if (template === 'transaction_error') {
      data = {
        hash: '0x123456'
      };
    } else if (template === 'payment_success') {
      data = {
        hash: '0x123456',
        amount: 0.0011
      };
    }

    const result = mail.render(template, {
      ...data,
      socials: [
        { name: 'bitcoinstalk', link: 'https://facebook.com' },
        { name: 'facebook', link: 'https://facebook.com' },
        { name: 'twitter', link: 'https://facebook.com' },
        { name: 'telegram', link: 'https://facebook.com' },
        { name: 'youtube', link: 'https://facebook.com' }
      ]
    });

    res.send(result.html);
  }
);

router.get(
  '/locale',
  async (req, res) => {
    res.json({
      locale: req.getLocale(),
      text: res.t('mail.login.subject')
    });
  }
);

router.get(
  '/async-error',
  async (req, res) => {
    throw new Error('test');
  }
);

router.get(
  '/joi-locale',
  validation({
    query: {
      name: Joi.string().required()
    }
  }),
  async (req, res) => {
    res.json({ foo: 'bar' });
  });

module.exports = router;
