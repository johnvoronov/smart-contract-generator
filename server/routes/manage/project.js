const express = require('express');
const Joi = require('joi');
const validation = require('../../middleware/validation');
const auth = require('../../middleware/auth');
const projectWalletService = require('../../service/projectWallet');
const filterValues = require('../../util/filterValues');
const smartGeneratorService = require('../../service/smartGeneratorService');
const models = require('../../models');
const { projectStatus } = require('../../consts');
const router = express.Router();

router.post('/:project_id/settings', auth, validation({
  params: {
    project_id: Joi.number().integer().positive().required()
  },
  body: {
    // Минимальная сумма к выводу в токенах
    minimal_withdraw: Joi.number().integer().default(0).min(0),
    // Минимальная сумма покупки в токенах
    minimal_amount: Joi.number().integer().default(0).min(0),
    // Включение / выключение обменника Indacoin
    exchange_indacoin: Joi.number().integer().default(0),
    // Включение / выключение обменника Changelly
    exchange_changelly: Joi.number().integer().default(0),
    // Включение / выключение обменника ShapeShift
    exchange_shapeshift: Joi.number().integer().default(0),
    // Social media links
    bitcoinstalk: Joi.string().default(null).allow([null, '']),
    facebook: Joi.string().default(null).allow([null, '']),
    twitter: Joi.string().default(null).allow([null, '']),
    youtube: Joi.string().default(null).allow([null, '']),
    telegram: Joi.string().default(null).allow([null, ''])
  }
}), async (req, res) => {
  const project = await models.Project.findById(req.params.project_id);

  if (null === project) {
    return res.boom.notFound();
  }

  await project.update({
    settings: filterValues({
      ...project.settings,
      ...req.body
    })
  });

  res.status(204).end();
});

router.post('/create/discount', auth, validation({
  body: {
    project_id: Joi.number().integer().positive().required()
    // TODO валидация скидок из админки
  }
}), async (req, res) => {
  const project = await models.Project.findByOne({
    where: {
      user_id: req.user.id,
      project_id: req.body.project_id,
    }
  });
  if (null === project) {
    return res.boom.notFound();
  }

  // TODO сохранение скидок

  res.status(204).end();
});

/**
 * Получение или создание кошелька
 * для оплаты проекта
 */
router.post('/wallet', auth, validation({
  body: {
    project_id: Joi.number().integer().positive().required(),
    currency_id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const project = await models.Project.findOne({
    where: {
      id: req.body.project_id,
      user_id: req.user.id
    }
  });
  if (null === project) {
    return res.boom.notFound();
  }

  if (project.is_paid) {
    return res.boom.badRequest(req.t('project.already_paided'));
  }

  const currency = await models.Currency.findById(req.body.currency_id);
  if (null === currency) {
    return res.boom.notFound();
  }

  const wallet = await projectWalletService.getWallet(project, currency);

  res.json(wallet);
});

router.post('/deploy', auth, validation({
  body: {
    project_id: Joi.number().integer().positive().required()
  }
}), async (req, res) => {
  const project = await models.Project.findOne({
    where: {
      id: req.body.project_id,
      user_id: req.user.id,
      status: projectStatus.PROJECT_DRAFT,
    }
  });
  if (null === project) {
    return res.boom.notFound();
  }

  if (!project.is_paid) {
    return res.boom.paymentRequired('insufficient funds');
  }

  await project.update({
    status: projectStatus.PROJECT_DONE
  });
  const result = await smartGeneratorService.createContract(project);

  res.status(201).json({
    // TODO возвращать ID транзакции контракта
    // TODO сохранять contract_address, owner_private_key, owner_address
    address: result
  });
});

module.exports = router;
