const fs = require('fs');
const path = require('path');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerSchema = require('../swagger/schema');
const router = express.Router();

router.use(swaggerUi.serve, swaggerUi.setup(swaggerSchema, {
  customCss: fs.readFileSync(path.join(__dirname, '../swagger/theme.css'), 'UTF-8')
}));

module.exports = router;
