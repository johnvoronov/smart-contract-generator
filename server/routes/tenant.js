const path = require('path');
const express = require('express');
const fallback = require('express-history-api-fallback');
const router = express.Router();
const subdomain = require('../middleware/subdomain');
const auth = require('../middleware/auth');
const tenantMiddleware = require('../middleware/tenant');

router.use('/api/ipn', tenantMiddleware, require('./tenant/ipn'));
router.use('/api/dashboard', auth, tenantMiddleware, require('./tenant/dashboard'));
router.use('/api/discount', auth, tenantMiddleware, require('./tenant/discount'));
router.use('/api/faq', auth, tenantMiddleware, require('./tenant/faq'));
router.use('/api/currency', auth, tenantMiddleware, require('./tenant/currency'));
router.use('/api/transaction', auth, tenantMiddleware, require('./tenant/transaction'));
router.use('/api/wallet', auth, tenantMiddleware, require('./tenant/wallet'));
router.use('/api/balance', auth, tenantMiddleware, require('./tenant/balance'));
router.use('/api/withdraw', auth, tenantMiddleware, require('./tenant/withdraw'));
router.use('/api/payment', auth, tenantMiddleware, require('./tenant/payment'));
router.use('/api/stats', auth, tenantMiddleware, require('./tenant/stats'));
router.use('/api/referral', auth, tenantMiddleware, require('./tenant/referral'));
router.use('/api/social', auth, tenantMiddleware, require('./tenant/social'));
router.use('/api/settings', tenantMiddleware, require('./tenant/settings'));
router.use('/api/user', tenantMiddleware, require('./tenant/user'));
router.use('/api/bounty', tenantMiddleware, require('./tenant/bounty'));
router.use('/api/admin', tenantMiddleware, require('./tenant/admin'));
router.use('/api/auth', tenantMiddleware, require('./auth'));

const adminRoot = path.join(__dirname, '../../admin');
router.use('/admin', tenantMiddleware, express.static(adminRoot));
router.use('/admin', fallback('index.html', { root: adminRoot }));

const frontendRoot = path.join(__dirname, '../../frontend');
router.use('/', tenantMiddleware, express.static(frontendRoot));
router.use('/', fallback('index.html', { root: frontendRoot }));

module.exports = subdomain('*.tokensale', router);
