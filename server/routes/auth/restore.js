const express = require('express');
const Joi = require('joi');
const models = require('../../models');
const validation = require('../../middleware/validation');
const passwordUtil = require('../../util/password');
const mail = require('../../service/mail');
const jwt = require('../../util/jwt');
const router = express.Router();

router.post(
  '',
  validation({
    body: {
      email: Joi.string().email().required().label('validation.email')
    }
  }),
  /**
   * Восстановление доступа к учетной записи пользователя
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const { email } = req.body;
    console.log({ email });

    const customer = await models.Customer.findOne({
      where: { email: email.toLowerCase() }
    });

    if (!customer) {
      return res.boom.badRequest(null, {
        errors: {
          email: [
            req.t('controller.user.invalid_email_or_user_not_found')
          ]
        }
      });
    }

    if (!customer.token) {
      await customer.update({ token: passwordUtil.token() });
    }

    mail.send(customer.email, 'user_restore', {
      user: customer
    });

    req.app.emit('customer.restore', req, res, customer);

    res.status(204).end();
  }
);

router.post(
  '/confirm',
  validation({
    body: {
      token: Joi.string().required().label('validation.token'),
      password: Joi.string().required().min(6).max(50).label('validation.password'),
      password_confirm: Joi.string().required().min(6).max(50).valid(Joi.ref('password')).options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      }).label('validation.password_confirm')
    }
  }),
  /**
   * Подтверждение пользователем восстановления пароля
   * Метод почти полностью дублирует изменение пароля пользователем, но
   * с дополнительной проверкой по корректности полученного токена из письма
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      token,
      password,
      password_confirm
    } = req.body;

    const user = await models.Customer.findOne({
      where: { token }
    });

    if (!user) {
      return res.boom.badRequest(null, {
        errors: {
          password: [
            req.t('controller.invalid_token')
          ]
        }
      });
    }

    if (password !== password_confirm) {
      return res.boom.badRequest(null, {
        errors: {
          password: [
            req.t('controller.user.password_not_match')
          ]
        }
      });
    }

    await user.update({
      // Активируем учетную запись пользователя, так как
      // восстановление учетки подразумевает отправку письма на
      // ящик пользователя, что само сабой подразумевает подтверждение
      // ящика электронной почты пользователя
      is_active: true,
      // Обнуляем токен воизбежание переиспользования
      token: null,
      password: await passwordUtil.hash(password)
    });

    req.app.emit('customer.restore.confirm', req, res, user);

    res.json(jwt.response(user));
  }
);

router.post(
  '/check',
  validation({
    body: {
      token: Joi.string().required().label('validation.token')
    }
  }),
  /**
   * Проверка токена полученного пользователем по эл. почте перед установкой пароля.
   * Пользователь заказывает восстановление пароля ->
   * Вводит токен с эл. почты ->
   * Проверяем токен на корректность ->
   * Если верный, позволяем задать новый пароль с тем же токеном. Иначе валим ошибку.
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      token
    } = req.body;

    const user = await models.Customer.findOne({
      where: { token }
    });

    if (!user) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.invalid_token')
          ]
        }
      });
    }

    res.status(204).end();
  }
);

router.post(
  '/resend',
  validation({
    body: {
      email: Joi.string().email().required().label('validation.email')
    }
  }),
  /**
   * Повторная отправка письма с активацией учетной записи.
   * Если пользователь был ранее уже активирован, то возвращается
   * 400 ответ от сервера
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const { email } = req.body;

    const customer = await models.Customer.findOne({
      where: { email: email.toLowerCase() }
    });

    if (!customer) {
      return res.boom.badRequest(null, {
        errors: {
          email: [
            req.t('controller.user.invalid_email_or_user_not_found')
          ]
        }
      });
    }

    if (!customer.token) {
      await customer.update({ token: passwordUtil.token() });
    }

    mail.send(customer.email, 'user_restore', {
      user: customer
    });

    req.app.emit('customer.restore.resend', req, res, customer);

    return res.status(204).end();
  }
);

module.exports = router;
