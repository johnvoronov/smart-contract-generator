const express = require('express');
const Joi = require('joi');
const models = require('../../models');
const validation = require('../../middleware/validation');
const passwordUtil = require('../../util/password');
const jwt = require('../../util/jwt');
const mail = require('../../service/mail');
const speakeasy = require('speakeasy');
const router = express.Router();

router.post(
  '',
  validation({
    body: {
      email: Joi.string().email().required().label('validation.email'),
      password: Joi.string().min(6).max(50).required().label('validation.password'),
      token: Joi.number().label('validation.token')
    }
  }),
  /**
   * Авторизация пользователя с JWT токеном
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      email,
      password,
      token
    } = req.body;

    const user = await models.Customer.findOne({
      where: { email }
    });

    if (!user) {
      return res.boom.badRequest(null, {
        errors: {
          password: [
            req.t('controller.user.login.invalid_username_or_password')
          ]
        }
      });
    }

    if (!user.is_active) {
      return res.boom.badRequest(null, {
        errors: {
          email: [
            req.t('controller.user.login.inactive_account')
          ]
        }
      });
    }

    const valid = await passwordUtil.compare(password, user.password);

    if (!valid) {
      return res.boom.badRequest(null, {
        errors: {
          password: [
            req.t('controller.user.login.invalid_username_or_password')
          ]
        }
      });
    }

    if (user.is_two_factor) {
      if (!token) {
        return res.boom.notAcceptable();
      }

      const verified = speakeasy.totp.verify({
        secret: user.two_factor_secret,
        encoding: 'base32',
        token
      });

      if (!verified) {
        return res.boom.badRequest(null, {
          errors: {
            token: [
              req.t('controller.user.login.invalid_2fa_token')
            ]
          }
        });
      }
    }

    await user.update({
      refresh_token: passwordUtil.token()
    });

    // TODO добавить в настройки опцию для отключения отправки письма после авторизации
    const ip = (req.headers['x-forwarded-for'] || '').split(',').pop() || req.connection.remoteAddress;

    mail.send(user.email, 'user_login', {
      ip,
      email: user.email,
      user_agent: req.headers['user-agent'],
      login_at: (new Date()).toLocaleString()
    });

    req.app.emit('customer.login', req, res, user);

    res.json(jwt.response(await user.reload()));
  }
);

module.exports = router;
