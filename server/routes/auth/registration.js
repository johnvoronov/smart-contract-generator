const express = require('express');
const Joi = require('joi');
const zxcvbn = require('zxcvbn');
const models = require('../../models');
const validation = require('../../middleware/validation');
const passwordUtil = require('../../util/password');
const jwt = require('../../util/jwt');
const router = express.Router();
const mail = require('../../service/mail');
const { ethAddress } = require('../../util/joiUtils');

router.post(
  '',
  validation({
    body: {
      is_agree: Joi.boolean().required().label('validation.is_agree'),
      is_confirm: Joi.boolean().required().label('validation.is_confirm'),
      email: Joi.string().email().required().label('validation.email'),
      password: Joi.string().min(6).max(50).required().label('validation.password'),
      password_confirm: Joi.string().min(6).max(50).required().valid(Joi.ref('password')).options({
        language: {
          any: {
            allowOnly: '!!Passwords do not match'
          }
        }
      }).label('validation.password_confirm')
    }
  }),
  /**
   * Регистрация пользователя
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      email,
      password
    } = req.body;

    if (zxcvbn(password).score < 3) {
      return res.boom.badRequest(null, {
        errors: {
          password: [
            req.t('controller.user.registration.weak_password')
          ]
        }
      })
    }

    const count = await models.Customer.count({
      where: { email: email.toLowerCase() }
    });

    if (count > 0) {
      return res.boom.badRequest(null, {
        errors: {
          email: [
            req.t('controller.user.registration.email_already_taken')
          ]
        }
      });
    }

    const transaction = await models.sequelize.transaction();
    try {
      const user = await models.Customer.create({
        email: email.toLowerCase(),
        token: passwordUtil.token(),
        password: await passwordUtil.hash(password),
      }, { transaction });

      mail.send(user.email.toLowerCase(), 'user_registration', {
        user
      });

      req.app.emit('customer.registration', req, res, user);

      transaction.commit();

      res.status(201).json({
        id: user.id
      });
    } catch (e) {
      transaction.rollback();
      throw e;
    }
  }
);

router.post(
  '/confirm',
  validation({
    body: {
      token: Joi.string().required().label('validation.token')
    }
  }),
  /**
   * Подтверждение электронного адреса пользователя
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      token
    } = req.body;

    const user = await models.Customer.findOne({
      where: { token }
    });

    if (!user) {
      return res.boom.badRequest(null, {
        errors: {
          token: [
            req.t('controller.invalid_token')
          ]
        }
      });
    }

    await user.update({
      is_active: true,
      token: null
    });

    req.app.emit('customer.registration.confirm', req, res, user);

    res.json(jwt.response(user));
  }
);

router.post(
  '/resend',
  validation({
    body: {
      email: Joi.string().email().required().label('validation.email')
    }
  }),
  /**
   * Повторная отправка письма с активацией учетной записи.
   * Если пользователь был ранее уже активирован, то возвращается
   * 400 ответ от сервера
   *
   * @param req
   * @param res
   */
  async (req, res) => {
    const {
      email
    } = req.body;

    const user = await models.Customer.findOne({
      where: {
        email: email.toLowerCase(),
        is_active: false
      }
    });

    if (!user) {
      return res.boom.badRequest(null, {
        errors: {
          email: [
            req.t('controller.user.invalid_email_or_user_not_found')
          ]
        }
      });
    }

    await user.update({
      token: passwordUtil.token()
    });

    mail.send(user.email, 'user_registration', {
      user
    });

    req.app.emit('customer.registration.resend', req, res, user);

    res.status(204).end()
  }
);

module.exports = router;
