const express = require('express');
const router = express.Router();
const customerAuth = require('../middleware/customerAuth');

router.use('/project', customerAuth, require('./project'));
router.use('/blockchain', customerAuth, require('./blockchain'));

router.use('/auth', require('./auth'));
router.use('/ipn', require('./ipn'));

router.use('/doc', require('./doc'));

if (process.env.NODE_ENV !== 'production') {
  router.use('/system', require('./system'));
}

module.exports = router;
